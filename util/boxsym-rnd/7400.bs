refdes U??

pinalign right center
pinalign bottom center
pinalign top center
attr device 7400

begin slot power
	shape box
	begin pin Vcc
		num 14
		loc top
	end pin
	begin pin gnd
		num 7
		loc bottom
	end pin
	pinalign top center
	pinalign bottom center
end slot


begin slot logic
	shape box right=halfcirc
	label "4*NAND"
	attr device 7400
	attr_both -slot 1
#	attr_invis foo bar

	begin pin A
		num 1:4:9:12
		loc left
		dir in
	end pin

	begin pin B
		num 2:5:10:13
		loc left
		dir in
	end pin

	begin pin Z
		num 3:6:8:11
		loc right
		dir out
		invcirc
	end pin

	pinalign left center
	pinalign right center
end slot

