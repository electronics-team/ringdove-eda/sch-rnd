.title sch-rnd export using export_spice

*** sch-rnd/export_spice: model card: _spice_model_card_lm358 ***
* lm358 - low power opamp model (single slot)
*
* (C) 2023 Tibor 'Igor2' Palinkas
* License: CC0 (no rights reserved): https://creativecommons.org/publicdomain/zero/1.0/
* Source: from ST's datasheet: https://archive.org/details/st-ts321
* (st321 is reasonably close to lm358 for simple simulation cases; see
* warnings on page 7)
*
*
** CONNECTIONS:
* 1 inverting input
* 2 non-inverting INPUT
* 3 output
* 4 positive power supply
* 5 negative power supply
.SUBCKT schrnd__spice_model_card_lm358 1 2 3 4 5

.MODEL MDTH D IS=1E-8 KF=3.104131E-15 CJO=10F

* INPUT STAGE
CIP 2 5 1.000000E-12
CIN 1 5 1.000000E-12
EIP 10 5 2 5 1
EIN 16 5 1 5 1
RIP 10 11 2.600000E+01
RIN 15 16 2.600000E+01
RIS 11 15 2.003862E+02
DIP 11 12 MDTH 400E-12
DIN 15 14 MDTH 400E-12
VOFP 12 13 DC 0
VOFN 13 14 DC 0
IPOL 13 5 1.000000E-05
CPS 11 15 3.783376E-09
DINN 17 13 MDTH 400E-12
VIN 17 5 0.000000e+00
DINR 15 18 MDTH 400E-12
VIP 4 18 2.000000E+00
FCP 4 5 VOFP 3.400000E+01
FCN 5 4 VOFN 3.400000E+01
FIBP 2 5 VOFN 2.000000E-03
FIBN 5 1 VOFP 2.000000E-03

* AMPLIFYING STAGE
FIP 5 19 VOFP 3.600000E+02
FIN 5 19 VOFN 3.600000E+02
RG1 19 5 3.652997E+06
RG2 19 4 3.652997E+06
CC 19 5 6.000000E-09
DOPM 19 22 MDTH 400E-12
DONM 21 19 MDTH 400E-12
HOPM 22 28 VOUT 7.500000E+03
VIPM 28 4 1.500000E+02
HONM 21 27 VOUT 7.500000E+03
VINM 5 27 1.500000E+02
EOUT 26 23 19 5 1
VOUT 23 5 0
ROUT 26 3 20
COUT 3 5 1.000000E-12
DOP 19 25 MDTH 400E-12
VOP 4 25 2.242230E+00
DON 24 19 MDTH 400E-12
VON 24 5 7.922301E-01
.ENDS


*** circuit ***
R5 anon_net_9 0 10k
R4 out anon_net_9 250
V1 in 0 dc 0 ac 0.1
V2 Vcc 0 dc 5
X_U1__1 out anon_net_13 out Vcc Vneg schrnd__spice_model_card_lm358
V3 0 Vneg dc 5
X_U1__2 anon_net_12 anon_net_9 anon_net_12 Vcc Vneg schrnd__spice_model_card_lm358
C2 anon_net_10 anon_net_13 100n
C3 anon_net_11 anon_net_12 200n
C1 in anon_net_10 100n
R1 in anon_net_11 1600
R2 anon_net_11 anon_net_13 1600
R3 anon_net_10 anon_net_12 800

*** sch-rnd/export_spice: commands ***
.control

ac dec 10 1 1000k
settype decibel out
plot vdb(out) xlimit 1 1000k ylabel 'small signal gain'
settype phase out
plot cph(out) xlimit 1 1000k ylabel 'phase (in rad)'

.endc
.end
