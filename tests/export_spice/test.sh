#!/bin/sh
root=../..
tut=$root/doc/tutorial/simulation/raw
src=$root/src/sch-rnd
here=`pwd`
refs=`ls *.cir.ref`
opts="-c rc/quiet=1"

bad=0
all=0
cd $src
for n in $refs
do
	bn=${n%%.cir.ref}
	./sch-rnd $opts -x spice --view spice_raw --outfile $here/$bn.cir.out $tut/$bn.rs
	diff $here/$bn.cir.ref $here/$bn.cir.out
	if test $? = 0
	then
		rm $here/$bn.cir.out
	else
		echo "BROKEN spice: $n"
		bad=$(($bad+1))
	fi
	all=$(($all+1))
done

if test $bad = 0
then
	echo "*** QC PASS ***"
	true
else
	echo "Failed raw spice tests: $bad of $all"
	false
fi
