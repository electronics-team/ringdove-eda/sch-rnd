#include <stdlib.h>
#include <stdio.h>

char *csch_relative_path_files(const char *pth, const char *relto);

static void test(const char *p1, const char *p2)
{
	char *res = csch_relative_path_files(p1, p2);
	printf("(%s, %s) = %s\n", p1, p2, res);
	free(res);
}

int main()
{
	test("/home/foo/bar/baz.pcb", "/home/foo/heh.txt");
	test("/home/foo/heh.txt", "/home/foo/bar/baz.pcb");

	test("/home/foo/bar/baz.pcb", "/tmp/heh.txt");
	test("/tmp/heh.txt", "/home/foo/bar/baz.pcb");

	return 0;
}
