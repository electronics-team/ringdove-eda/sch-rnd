#include <stdio.h>
#include <librnd/config.h>
#ifndef RND_INLINE
#define RND_INLINE static
#endif
#include "cond.h"

const char *forgecond_var_resolve_cb(forgecond_ctx_t *ctx, const char *name1, const char *name2)
{
	if (strcmp(name1, "first") == 0) return "one";
	if (strcmp(name1, "second") == 0) return "two";
	if (strcmp(name1, "third") == 0) return "three";
	if (strcmp(name1, "fourth") == 0) return "one";
	if (strcmp(name1, "empty") == 0) return "";
	return NULL;
}

void forgecond_error_cb(forgecond_ctx_t *ctx, const char *s)
{
	fprintf(stderr, "%s\n", s);
}


int main(void)
{
	long res;
	forgecond_ctx_t ctx = {0};
	vtl0_t stack = {0};
	char line[256], *s;


	printf("-- compile --\n");
	s = fgets(line, sizeof(line), stdin);
	if (forgecond_parse_str(&ctx, s) != 0)
		return 1;

	printf("-- AST --\n");
	forgecond_dump(stderr, &ctx);

	printf("-- execute --\n");
	res = forgecond_exec(&ctx, &stack);
	printf("res=%ld\n", res);

	forgecond_uninit(&ctx);
	vtl0_uninit(&stack);

	return 0;
}

