ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=aym2tojMMW6UVaugMwQAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.2 {
				uuid=aym2tojMMW6UVaugMwQAAAAH; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAe;
				x=76000; y=136000;
				li:objects {
					ha:group.1 {
						uuid=aym2tojMMW6UVaugMwQAAAAI; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAf;
						x=0; y=4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.2 { x1=0; y1=4000; x2=-2000; y2=6000; stroke=sym-decor; }
					ha:line.3 { x1=-2000; y1=6000; x2=0; y2=8000; stroke=sym-decor; }
					ha:line.4 { x1=0; y1=8000; x2=2000; y2=6000; stroke=sym-decor; }
					ha:line.5 { x1=2000; y1=6000; x2=0; y2=4000; stroke=sym-decor; }
					ha:text.6 { x1=-4000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					loc_l2=1
					name=/foo1
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.4 {
				uuid=aym2tojMMW6UVaugMwQAAAAP; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAe;
				x=104000; y=136000;
				li:objects {
					ha:group.1 {
						uuid=aym2tojMMW6UVaugMwQAAAAQ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAf;
						x=0; y=4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.2 { x1=0; y1=4000; x2=-2000; y2=6000; stroke=sym-decor; }
					ha:line.3 { x1=-2000; y1=6000; x2=0; y2=8000; stroke=sym-decor; }
					ha:line.4 { x1=0; y1=8000; x2=2000; y2=6000; stroke=sym-decor; }
					ha:line.5 { x1=2000; y1=6000; x2=0; y2=4000; stroke=sym-decor; }
					ha:text.6 { x1=-4000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					loc_l2=1
					name=/foo2
					role=symbol
					spice/omit=yes
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
