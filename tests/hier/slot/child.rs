ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=gNCuJ00G+cWbegsDojwAAAAd;
				li:objects {
					ha:group.1 {
						uuid=gNCuJ00G+cWbegsDojwAAAAe; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							device=lm358
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=gNCuJ00G+cWbegsDojwAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=gNCuJ00G+cWbegsDojwAAAAF; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=76000; y=124000;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=inA
					role=terminal
				}
			}
			ha:group.3 {
				uuid=gNCuJ00G+cWbegsDojwAAAAG; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=76000; y=116000;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=inB
					role=terminal
				}
			}
			ha:group.4 {
				uuid=gNCuJ00G+cWbegsDojwAAAAJ; src_uuid=AHibvjaMiL5NH+9/wR0AAAAI;
				x=132000; y=124000;
				li:objects {
					ha:line.1 { x1=4000; y1=0; x2=0; y2=0; stroke=term-decor; }
					ha:text.2 { x1=4500; y1=-1500; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=17000; y1=0; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=17000; y1=0; x2=16000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=16000; y1=-2000; x2=4000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=4000; y1=2000; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=4000; y1=2000; x2=4000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet output net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=outA
					role=terminal
				}
			}
			ha:group.5 {
				uuid=gNCuJ00G+cWbegsDojwAAAAK; src_uuid=AHibvjaMiL5NH+9/wR0AAAAI;
				x=132000; y=116000;
				li:objects {
					ha:line.1 { x1=4000; y1=0; x2=0; y2=0; stroke=term-decor; }
					ha:text.2 { x1=4500; y1=-1500; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=17000; y1=0; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=17000; y1=0; x2=16000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=16000; y1=-2000; x2=4000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=4000; y1=2000; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=4000; y1=2000; x2=4000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet output net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=outB
					role=terminal
				}
			}
			ha:group.6 {
				uuid=gNCuJ00G+cWbegsDojwAAAAX; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=116000; y=120000;
				li:objects {
					ha:group.1 {
						uuid=gNCuJ00G+cWbegsDojwAAAAY; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=gNCuJ00G+cWbegsDojwAAAAZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=gNCuJ00G+cWbegsDojwAAAAa; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=gNCuJ00G+cWbegsDojwAAAAb; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=gNCuJ00G+cWbegsDojwAAAAc; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-slot=1
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=./U1
					role=symbol
				}
			}
			ha:group.7 {
				uuid=gNCuJ00G+cWbegsDojwAAAAf;
				li:objects {
					ha:line.1 { x1=76000; y1=124000; x2=92000; y2=124000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.8 {
				li:conn {
					/2/7/1
					/2/2/1
				}
			}
			ha:connection.9 {
				li:conn {
					/2/7/1
					/2/6/1/1
				}
			}
			ha:group.10 {
				uuid=gNCuJ00G+cWbegsDojwAAAAg;
				li:objects {
					ha:line.1 { x1=76000; y1=116000; x2=92000; y2=116000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/10/1
					/2/3/1
				}
			}
			ha:connection.12 {
				li:conn {
					/2/10/1
					/2/6/2/1
				}
			}
			ha:group.13 {
				uuid=gNCuJ00G+cWbegsDojwAAAAh;
				li:objects {
					ha:line.1 { x1=116000; y1=120000; x2=124000; y2=120000; stroke=wire; }
					ha:line.2 { x1=124000; y1=120000; x2=124000; y2=116000; stroke=wire; }
					ha:line.3 { x1=124000; y1=116000; x2=132000; y2=116000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.14 {
				li:conn {
					/2/13/1
					/2/6/3/1
				}
			}
			ha:connection.15 {
				li:conn {
					/2/13/3
					/2/5/1
				}
			}
			ha:group.16 {
				uuid=gNCuJ00G+cWbegsDojwAAAAi;
				li:objects {
					ha:line.1 { x1=104000; y1=128000; x2=128000; y2=128000; stroke=wire; }
					ha:line.2 { x1=128000; y1=128000; x2=128000; y2=124000; stroke=wire; }
					ha:line.3 { x1=128000; y1=124000; x2=132000; y2=124000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.17 {
				li:conn {
					/2/16/1
					/2/6/11/1
				}
			}
			ha:connection.18 {
				li:conn {
					/2/16/3
					/2/4/1
				}
			}
			ha:group.19 {
				uuid=gNCuJ00G+cWbegsDojwAAAAn; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=104000; y=112000;
				li:objects {
					ha:group.1 {
						uuid=gNCuJ00G+cWbegsDojwAAAAo; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.20 {
				li:conn {
					/2/19/1/1
					/2/6/10/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
    }
   }
  }
}
