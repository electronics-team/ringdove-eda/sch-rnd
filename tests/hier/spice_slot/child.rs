ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAV;
				li:objects {
					ha:group.1 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAW; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							device=lm358
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=TXsYkAVvycDUg/6VRmIAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.2 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAP; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=124000; y=128000;
				li:objects {
					ha:group.1 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAQ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAR; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAS; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAT; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAU; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.13 { x1=-15000; y1=9000; dyntext=1; stroke=sym-secondary; text=%../A.-slot%; floater=1; }
				}
				ha:attrib {
					-slot=1
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=./U1
					role=symbol
					spice/prefix=A
				}
			}
			ha:group.4 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAj; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=160000; y=128000;
				li:objects {
					ha:group.1 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAk; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAl; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAm; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAn; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=TXsYkAVvycDUg/6VRmIAAAAo; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.13 { x1=-15000; y1=9000; dyntext=1; stroke=sym-secondary; text=%../A.-slot%; floater=1; }
				}
				ha:attrib {
					-slot=2
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=./U1
					role=symbol
					spice/prefix=A
				}
			}
			ha:connection.6 {
				li:conn {
					/2/2/3/1
					/2/24/6
				}
			}
			ha:connection.7 {
				li:conn {
					/2/4/1/1
					/2/24/5
				}
			}
			ha:group.8 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAq;
				li:objects {
					ha:line.1 { x1=100000; y1=132000; x2=80000; y2=132000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.9 {
				li:conn {
					/2/8/1
					/2/2/1/1
				}
			}
			ha:connection.11 {
				li:conn {
					/2/4/3/1
					/2/22/4
				}
			}
			ha:group.12 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAu; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=80000; y=132000;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=in
					role=terminal
				}
			}
			ha:connection.13 {
				li:conn {
					/2/12/1
					/2/8/1
				}
			}
			ha:group.14 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAx; src_uuid=AHibvjaMiL5NH+9/wR0AAAAI;
				x=176000; y=128000;
				li:objects {
					ha:line.1 { x1=4000; y1=0; x2=0; y2=0; stroke=term-decor; }
					ha:text.2 { x1=4500; y1=-1500; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=17000; y1=0; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=17000; y1=0; x2=16000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=16000; y1=-2000; x2=4000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=4000; y1=2000; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=4000; y1=2000; x2=4000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet output net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=out
					role=terminal
				}
			}
			ha:connection.15 {
				li:conn {
					/2/14/1
					/2/22/4
				}
			}
			ha:group.16 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAy;
				li:objects {
					ha:line.1 { x1=112000; y1=136000; x2=112000; y2=152000; stroke=wire; }
					ha:text.4 { x1=108000; y1=153000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
					ha:line.5 { x1=112000; y1=148000; x2=148000; y2=148000; stroke=wire; }
					ha:line.6 { x1=112000; y1=148000; x2=112000; y2=148000; stroke=junction; }
					ha:line.7 { x1=148000; y1=148000; x2=148000; y2=136000; stroke=wire; }
				}
				ha:attrib {
					name=opa_plus
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.17 {
				li:conn {
					/2/16/1
					/2/2/11/1
				}
			}
			ha:group.19 {
				uuid=TXsYkAVvycDUg/6VRmIAAAAz;
				li:objects {
					ha:line.1 { x1=112000; y1=120000; x2=112000; y2=104000; stroke=wire; }
					ha:text.4 { x1=107000; y1=100000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
					ha:line.5 { x1=112000; y1=108000; x2=148000; y2=108000; stroke=wire; }
					ha:line.6 { x1=112000; y1=108000; x2=112000; y2=108000; stroke=junction; }
					ha:line.7 { x1=148000; y1=108000; x2=148000; y2=120000; stroke=wire; }
				}
				ha:attrib {
					name=opa_minus
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.20 {
				li:conn {
					/2/19/1
					/2/2/10/1
				}
			}
			ha:group.22 {
				uuid=TXsYkAVvycDUg/6VRmIAAAA0;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=132000; y2=124000; stroke=wire; }
					ha:line.2 { x1=132000; y1=124000; x2=132000; y2=116000; stroke=wire; }
					ha:line.3 { x1=132000; y1=116000; x2=164000; y2=116000; stroke=wire; }
					ha:line.4 { x1=160000; y1=128000; x2=176000; y2=128000; stroke=wire; }
					ha:line.5 { x1=164000; y1=116000; x2=164000; y2=128000; stroke=wire; }
					ha:line.6 { x1=164000; y1=128000; x2=164000; y2=128000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.23 {
				li:conn {
					/2/22/1
					/2/4/2/1
				}
			}
			ha:group.24 {
				uuid=TXsYkAVvycDUg/6VRmIAAAA1;
				li:objects {
					ha:line.1 { x1=100000; y1=124000; x2=92000; y2=124000; stroke=wire; }
					ha:line.2 { x1=92000; y1=124000; x2=92000; y2=112000; stroke=wire; }
					ha:line.3 { x1=92000; y1=112000; x2=124000; y2=112000; stroke=wire; }
					ha:line.5 { x1=124000; y1=132000; x2=136000; y2=132000; stroke=wire; }
					ha:line.6 { x1=124000; y1=112000; x2=124000; y2=132000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.25 {
				li:conn {
					/2/24/1
					/2/2/2/1
				}
			}
			ha:connection.26 {
				li:conn {
					/2/16/7
					/2/4/11/1
				}
			}
			ha:connection.27 {
				li:conn {
					/2/19/7
					/2/4/10/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
