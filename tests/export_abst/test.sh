#!/bin/sh
ROOT=../..
SCHDIR=`pwd`/$ROOT/doc/examples
HERE=`pwd`
REFS=`ls *.ref`
SRC=$ROOT/src/sch-rnd
bad=0


cd $SRC
for ref in $REFS
do
	bn="${ref%%.ref}"
	rs="$SCHDIR/$bn.rs"
	out="$HERE/$bn.out"
	log="$HERE/$bn.log"
	./sch-rnd -x abst --outfile "$out" "$rs" >"$log" 2>&1
	diff "$HERE/$ref" "$out"
	if test $? -eq 0
	then
		rm "$out"
	else
		bad=1
	fi
done

if test $bad -eq 0
then
	echo "*** QC PASS ***"
	exit 0
else
	exit 1
fi


