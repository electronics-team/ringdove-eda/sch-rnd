/* nanojson - very small JSON event parser with push API - semantic example

   This nanolib is written by Tibor 'Igor2' Palinkas in 2023 and 2024 and
   is licensed under the Creative Common CC0 (or is placed in the Public Domain
   where it is permitted by the law). */

#include <stdio.h>
#include "semantic.h"

int main()
{
	njson_sem_ctx_t ctx = {0};
	int chr;

	for(;;) {
		njson_sem_ev_t ev;

		chr = fgetc(stdin);

		ev = njson_sem_push(&ctx, chr);
		switch(ev) {
			case NJSON_SEM_EV_OBJECT_BEGIN: printf("object '%s' begin\n", ctx.name); break;
			case NJSON_SEM_EV_OBJECT_END:   printf("object end\n"); break;
			case NJSON_SEM_EV_ARRAY_BEGIN:  printf("array '%s' begin\n", ctx.name); break;
			case NJSON_SEM_EV_ARRAY_END:    printf("array end\n"); break;

			case NJSON_SEM_EV_ATOMIC:
				printf("atomic: ");
				switch(ctx.type) {
					case NJSON_SEM_TYPE_STRING: printf("'%s'='%s'\n", ctx.name, ctx.value.string); break;
					case NJSON_SEM_TYPE_NUMBER: printf("'%s'=%f\n", ctx.name, ctx.value.number); break;
					case NJSON_SEM_TYPE_TRUE:   printf("'%s'=TRUE\n", ctx.name); break;
					case NJSON_SEM_TYPE_FALSE:  printf("'%s'=FALSE\n", ctx.name); break;
					case NJSON_SEM_TYPE_NULL:   printf("'%s'=NULL\n", ctx.name); break;
					default:                    printf(" ???\n");
				}
				break;
				
			case NJSON_SEM_EV_error:  printf("error '%s' at %ld:%ld\n", ctx.njs.error, ctx.njs.lineno, ctx.njs.col); return 1;
			case NJSON_SEM_EV_eof:    printf("<eof>\n"); njson_sem_uninit(&ctx); return 0;
			case NJSON_SEM_EV_more:   break;
		}
	}

	njson_sem_uninit(&ctx);

	return 1;
}
