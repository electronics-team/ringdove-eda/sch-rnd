/* nanojson - very small JSON event parser with push API - token level example

   This nanolib is written by Tibor 'Igor2' Palinkas in 2023 and 2024 and
   is licensed under the Creative Common CC0 (or is placed in the Public Domain
   where it is permitted by the law). */
#include <stdio.h>
#include "nanojson.h"

int main()
{
	njson_ctx_t ctx = {0};
	int chr;

	for(;;) {
		njson_ev_t ev;

		chr = fgetc(stdin);

		ev = njson_push(&ctx, chr);
		switch(ev) {
			case NJSON_EV_OBJECT_BEGIN: printf("object begin\n"); break;
			case NJSON_EV_OBJECT_END:   printf("object end\n"); break;
			case NJSON_EV_ARRAY_BEGIN:  printf("array begin\n"); break;
			case NJSON_EV_ARRAY_END:    printf("array end\n"); break;

			case NJSON_EV_NAME:   printf("name:   '%s'\n", ctx.value.string); break;
			case NJSON_EV_STRING: printf("string: '%s'\n", ctx.value.string); break;
			case NJSON_EV_NUMBER: printf("number: %f\n", ctx.value.number); break;
			case NJSON_EV_TRUE:   printf("true\n"); break;
			case NJSON_EV_FALSE:  printf("false\n"); break;
			case NJSON_EV_NULL:   printf("null\n"); break;
			case NJSON_EV_error:  printf("error '%s' at %ld:%ld\n", ctx.error, ctx.lineno, ctx.col); return 1;
			case NJSON_EV_eof:    printf("<eof>\n"); njson_uninit(&ctx); return 0;
			case NJSON_EV_more:   break;
		}
	}

	njson_uninit(&ctx);
	return 1;
}
