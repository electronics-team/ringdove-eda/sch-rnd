extern const char csch_acts_AbstractDialog[];
extern const char csch_acth_AbstractDialog[];
fgw_error_t csch_act_AbstractDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

void csch_dlg_abst_init(void);
void csch_dlg_abst_uninit(void);
void csch_dlg_abst_compiled(csch_project_t *prj);
