#include <libcschem/concrete.h>
extern const char csch_acts_PenDialog[];
extern const char csch_acth_PenDialog[];
fgw_error_t csch_act_PenDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

const char *sch_rnd_pen_dlg(csch_sheet_t *sheet, csch_cgrp_t *grp, const char *pen_name, int modal, int recursive);

void csch_dlg_pen_preunload(csch_sheet_t *sheet);

