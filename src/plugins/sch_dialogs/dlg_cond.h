void csch_dlg_cond_init(void);
void csch_dlg_cond_uninit(void);


extern const char csch_acts_ConditionalDialog[];
extern const char csch_acth_ConditionalDialog[];
fgw_error_t csch_act_ConditionalDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_quick_attr_forge__if__dnp[];
extern const char csch_acth_quick_attr_forge__if__dnp[];
extern const char csch_acts_quick_attr_forge__if__omit[];
extern const char csch_acth_quick_attr_forge__if__omit[];
fgw_error_t csch_act_quick_attr_forge__if__dnp_omit(fgw_arg_t *res, int argc, fgw_arg_t *argv);
