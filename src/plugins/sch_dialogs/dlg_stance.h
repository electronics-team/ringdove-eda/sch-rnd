void csch_dlg_stance_init(void);
void csch_dlg_stance_uninit(void);
void csch_dlg_cond_preunload(csch_sheet_t *sheet);


extern const char csch_acts_StanceDialog[];
extern const char csch_acth_StanceDialog[];
fgw_error_t csch_act_StanceDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_ConditionalDialog[];
extern const char csch_acth_ConditionalDialog[];
fgw_error_t csch_act_ConditionalDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_quick_attr_forge__if__dnp[];
extern const char csch_acth_quick_attr_forge__if__dnp[];
extern const char csch_acts_quick_attr_forge__if__omit[];
extern const char csch_acth_quick_attr_forge__if__omit[];
fgw_error_t csch_act_quick_attr_forge__if__dnp_omit(fgw_arg_t *res, int argc, fgw_arg_t *argv);

/* bitfield */
typedef enum {
	STE_CANCEL = 0,            /* no-op */
	STE_SET = 1,               /* set the value of the stance */
	STE_REMEMBER = 2           /* append the value to possible values */
} sch_stance_edit_res_t;


/* Present a modal dialog to select a stance from all available stance values
   of that stance and/or adding a new value. Initval is the initial value the
   dialog box will be filled in with, or NULL to read the value from the config.
   Uses the current project (and config). val_out is dynamically allocated,
   the caller needs to free it. */
sch_stance_edit_res_t sch_stance_edit_dlg(const char *stance_name, const char *initval, char **val_out);
