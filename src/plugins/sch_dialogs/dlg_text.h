#include <libcschem/concrete.h>
#include <libcschem/cnc_text.h>

extern const char csch_acts_EditText[];
extern const char csch_acth_EditText[];
fgw_error_t csch_act_EditText(fgw_arg_t *res, int argc, fgw_arg_t *argv);

/* Prompt for editing text object and make an undoable modify. Returns 0
   on success, non-zero on error/cancel */
int sch_rnd_edit_text_dialog(csch_sheet_t *sheet, csch_text_t *text);
