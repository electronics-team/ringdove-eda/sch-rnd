#include <libcschem/concrete.h>

extern const char csch_acts_AttributeDialog[];
extern const char csch_acth_AttributeDialog[];
extern fgw_error_t csch_act_AttributeDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

extern const char csch_acts_AttributePick[];
extern const char csch_acth_AttributePick[];
extern fgw_error_t csch_act_AttributePick(fgw_arg_t *res, int argc, fgw_arg_t *argv);

void csch_dlg_attr_preunload(csch_sheet_t *sheet);
void csch_dlg_attr_edit(csch_sheet_t *sheet);
void csch_dlg_attr_obj_attr_edit(csch_sheet_t *sheet, csch_cgrp_t *obj);
void csch_dlg_attr_compiled(csch_project_t *prj);


csch_chdr_t *sch_dialog_resolve_obj(csch_sheet_t *sheet, const char *actname, const char *cmd, int *has_coords);

