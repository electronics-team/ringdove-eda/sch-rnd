extern const char csch_acts_ViewPortDialog[];
extern const char csch_acth_ViewPortDialog[];
fgw_error_t csch_act_ViewPortDialog(fgw_arg_t *res, int argc, fgw_arg_t *argv);

void csch_dlg_viewport_edit(csch_sheet_t *sheet);
void csch_dlg_viewport_preunload(csch_sheet_t *sheet);
void csch_dlg_viewport_uninit(void);

