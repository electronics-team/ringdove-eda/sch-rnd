#ifndef SCH_RND_TARGET_PCB_CONF_H
#define SCH_RND_TARGET_PCB_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			const struct {
				RND_CFT_STRING flow_prefix;  /* attributes starting with this prefix and a colon are exported (because of workflow considerations) */
				RND_CFT_STRING sw_prefix;    /* attributes starting with this prefix and a colon are exported (because of mathcing target software) */
				RND_CFT_LIST whitelist;      /* list attribute keys to be copied to export; when in form of key1->key2, rename key1 to key2 on export */
				RND_CFT_LIST blacklist;      /* do not export these attributes even if other rules would permit exporting them */
			} attr_export;
		} target_pcb;
	} plugins;
} conf_target_pcb_t;

extern conf_target_pcb_t target_pcb_conf;

#endif
