/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - attr. transf. for PCB workflow
 *  Copyright (C) 2022, 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022 and Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/config.h>

#include <stdlib.h>
#include <assert.h>

#include <libfungw/fungw.h>
#include <librnd/core/plugins.h>
#include <librnd/core/conf.h>
#include <librnd/core/conf_multi.h>

#include <libcschem/libcschem.h>
#include <libcschem/abstract.h>
#include <libcschem/engine.h>
#include <libcschem/actions_csch.h>
#include <libcschem/attrib.h>
#include <libcschem/util_compile.h>

#include <plugins/lib_target/lib_target.h>

#include "target_pcb_conf.h"
#include "conf_internal.c"

conf_target_pcb_t target_pcb_conf;

static const char cookie[] = "target_pcb";

typedef struct target_pcb_ctx_s {
	csch_view_eng_t *eng;
	sch_trgt_ctx_t trgt;
} target_pcb_ctx_t;

/*** hooks ***/

fgw_error_t target_pcb_compile_port(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
/*	csch_hook_call_ctx_t *cctx = argv[0].val.argv0.user_call_ctx;*/
	fgw_obj_t *obj = argv[0].val.argv0.func->obj;
	target_pcb_ctx_t *ctx = obj->script_data;
	csch_view_eng_t *eng = ctx->eng;
	csch_aport_t *port;
	const char *pinnum;
	csch_source_arg_t *src;


	CSCH_HOOK_CONVARG(1, FGW_AOBJ, std_cschem_comp_update, port = fgw_aobj(&argv[1]));
	assert(port->hdr.type == CSCH_ATYPE_PORT);

	pinnum = csch_attrib_get_str(&port->hdr.attr, "pcb/pinnum");
	if (pinnum != NULL)
		src = csch_attrib_src_pa(&port->hdr, "pcb/pinnum", "target_pcb", NULL);

	if (pinnum == NULL) {
		pinnum = csch_attrib_get_str(&port->hdr.attr, "pinnum");
		if (pinnum != NULL)
			src = csch_attrib_src_pa(&port->hdr, "pinnum", "target_pcb", NULL);
	}
	if (pinnum == NULL) {
		pinnum = port->name;
		if (pinnum != NULL)
			src = csch_attrib_src_p("target_pcb", "fallback on port name");
	}

	if (pinnum != NULL)
		csch_attrib_set(&port->hdr.attr, eng->eprio + CSCH_PRI_PLUGIN_NORMAL, "display/name", pinnum, src, NULL);

	return 0;
}

fgw_error_t target_pcb_compile_component0(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	fgw_obj_t *obj = argv[0].val.argv0.func->obj;
	target_pcb_ctx_t *ctx = obj->script_data;
	csch_view_eng_t *eng = ctx->eng;
	csch_acomp_t *comp;

	CSCH_HOOK_CONVARG(1, FGW_AOBJ, std_cschem_comp_update, comp = fgw_aobj(&argv[1]));
	assert(comp->hdr.type == CSCH_ATYPE_COMP);

	sch_trgt_copy_alt_attrib(&comp->hdr, eng->eprio + CSCH_PRI_PLUGIN_NORMAL, "target_pcb", "display/dnp",    "pcb/dnp",  "dnp", NULL);
	sch_trgt_copy_alt_attrib(&comp->hdr, eng->eprio + CSCH_PRI_PLUGIN_NORMAL, "target_pcb", "display/omit",   "pcb/omit", "omit", NULL);

	return 0;
}

fgw_error_t target_pcb_compile_component1(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	fgw_obj_t *obj = argv[0].val.argv0.func->obj;
	target_pcb_ctx_t *ctx = obj->script_data;
/*	csch_view_eng_t *eng = ctx->eng;*/
	csch_acomp_t *comp;

	CSCH_HOOK_CONVARG(1, FGW_AOBJ, std_cschem_comp_update, comp = fgw_aobj(&argv[1]));
	assert(comp->hdr.type == CSCH_ATYPE_COMP);

	sch_trgt_export_attrs(&ctx->trgt, &comp->hdr);

	return 0;
}

static void target_pcb_gen_nettags(target_pcb_ctx_t *ctx, csch_abstract_t *abst)
{
	htsp_entry_t *e;
	csch_view_eng_t *eng = ctx->eng;

	for(e = htsp_first(&abst->nets); e != NULL; e = htsp_next(&abst->nets, e)) {
		csch_anet_t *net = e->value;

		sch_trgt_copy_alt_attrib(&net->hdr, eng->eprio + CSCH_PRI_PLUGIN_NORMAL, "target_pcb", "display/omit",   "pcb/omit", "omit", NULL);
		sch_trgt_export_attrs(&ctx->trgt, &net->hdr);
	}
}


fgw_error_t target_pcb_compile_project_before(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
/*	csch_hook_call_ctx_t *cctx = argv[0].val.argv0.user_call_ctx;*/
	fgw_obj_t *obj = argv[0].val.argv0.func->obj;
	target_pcb_ctx_t *ctx = obj->script_data;

	sch_trgt_ctx_init(&ctx->trgt,
		&target_pcb_conf.plugins.target_pcb.attr_export.whitelist,
		&target_pcb_conf.plugins.target_pcb.attr_export.blacklist,
		target_pcb_conf.plugins.target_pcb.attr_export.flow_prefix,
		target_pcb_conf.plugins.target_pcb.attr_export.sw_prefix);


	return 0;
}

fgw_error_t target_pcb_compile_project_after(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
/*	csch_hook_call_ctx_t *cctx = argv[0].val.argv0.user_call_ctx;*/
	fgw_obj_t *obj = argv[0].val.argv0.func->obj;
	target_pcb_ctx_t *ctx = obj->script_data;
	csch_abstract_t *abst;
/*	csch_project_t *prj;*/

	CSCH_HOOK_CONVARG(1, FGW_STRUCT|FGW_PTR, std_cschem_comp_update, abst = argv[1].val.ptr_void);
/*	CSCH_HOOK_CONVARG(2, FGW_STRUCT|FGW_PTR, std_cschem_comp_update, prj = argv[2].val.ptr_void);*/

	target_pcb_gen_nettags(ctx, abst);

	sch_trgt_ctx_uninit(&ctx->trgt);
	return 0;
}


static int on_load(fgw_obj_t *obj, const char *filename, const char *opts)
{
	target_pcb_ctx_t *ctx = calloc(sizeof(target_pcb_ctx_t), 1);

	fgw_func_reg(obj, "compile_port", target_pcb_compile_port);
	fgw_func_reg(obj, "compile_component0", target_pcb_compile_component0);
	fgw_func_reg(obj, "compile_component1", target_pcb_compile_component1);
	fgw_func_reg(obj, "compile_project_before", target_pcb_compile_project_before);
	fgw_func_reg(obj, "compile_project_after", target_pcb_compile_project_after);

	obj->script_data = ctx;
	ctx->eng = obj->script_user_call_ctx; /* save eng ptr */
	return 0;
}

static int on_unload(fgw_obj_t *obj)
{
	target_pcb_ctx_t *ctx = obj->script_data;
	sch_trgt_ctx_uninit(&ctx->trgt);
	free(ctx);
	return 0;
}


static const fgw_eng_t fgw_target_pcb_eng = {
	"target_pcb",
	csch_c_call_script,
	NULL,
	on_load,
	on_unload
};

int pplg_check_ver_target_pcb(int ver_needed) { return 0; }

void pplg_uninit_target_pcb(void)
{
	rnd_conf_plug_unreg("plugins/target_pcb/", target_pcb_conf_internal, cookie);
}

int pplg_init_target_pcb(void)
{
	RND_API_CHK_VER;

	fgw_eng_reg(&fgw_target_pcb_eng);

	rnd_conf_plug_reg(target_pcb_conf, target_pcb_conf_internal, cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(target_pcb_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "target_pcb_conf_fields.h"

	return 0;
}

