#ifndef GENHT_HTCP_H
#define GENHT_HTCP_H

#include <libcschem/abstract.h>

typedef struct {
	const char *orig_comp_name; /* points into an attribute value */
	long pinnum;
} htcp_key_t;

typedef struct {
	vtp0_t ports;
} htcp_value_t;

#define HT(x) htcp_ ## x
#include <genht/ht.h>
#undef HT

int htcp_keyeq(htcp_key_t a, htcp_key_t b);
unsigned int htcp_keyhash(htcp_key_t a);

#endif
