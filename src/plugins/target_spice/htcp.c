#include "htcp.h"
#define HT(x) htcp_ ## x
static htcp_value_t invalid;
#define HT_INVALID_VALUE invalid
#include <genht/ht.c>
#include <genht/hash.h>

int htcp_keyeq(htcp_key_t a, htcp_key_t b)
{
	return (strcmp(a.orig_comp_name, b.orig_comp_name) == 0) && (a.pinnum == b.pinnum);
}

unsigned int htcp_keyhash(htcp_key_t a)
{
	return strhash(a.orig_comp_name) ^ longhash(a.pinnum);
}

#undef HT
