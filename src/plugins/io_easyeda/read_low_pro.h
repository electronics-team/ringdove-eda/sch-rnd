#include <stdio.h>
#include "easyeda_sphash.h"
#include <rnd_inclib/lib_easyeda/gendom.h>

/* Parse an open file into a gdom tree; call gdom_free() to discard it
   after use. Works on "easyeda pro" symbol files */
gdom_node_t *easypro_low_parse(FILE *f);



