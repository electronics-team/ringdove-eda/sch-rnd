#ifndef SCH_RND_IO_EASYEDA_CONF_H
#define SCH_RND_IO_EASYEDA_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_REAL coord_mult;               /* all easyeda coordinates are multiplied by this value to get sch-rnd coords */
			RND_CFT_LIST library_search_paths;     /* ordered list of paths that are each recursively searched for easyeda sym files */
			RND_CFT_BOOLEAN auto_normalize;        /* move all objects so that starting coords are near 0;0, without the high, usually 40000 offset of gschem */
			RND_CFT_LIST postproc_sheet_load;      /* pattern;action pairs for object transformations after a succesful load; mostly used for attribute editing */
			RND_CFT_REAL line_approx_seg_len;      /* when approximating curves with line segments, try to use this segment length; in input units; smaller number is finer approximation but more line objects */
			RND_CFT_BOOLEAN auto_lock_frame;       /* enables heuristics to find the sheet frame symbol and lock it so that it doesn't interfere with selection */
			RND_CFT_STRING zip_list_cmd;           /* shell command that lists the content of a zip file to stdout; %s is replaced by path to the file; noise (headers and file sizes) is accepted as long as file names are not cut by newlines */
			RND_CFT_STRING zip_extract_cmd;        /* shell command that extracts a zip file in current working directory; %s is replaced by path to the file */
			const struct {
				RND_CFT_BOOLEAN dump_dom;            /* print the Document Object Model to stdout after the low level parse step */
				RND_CFT_BOOLEAN unzip_static;        /* always unzip to /tmp/easypro and don't remove it - don't use in production (unsafe temp file creation, unzip blocking to ask for overwrite on console) */
			} debug;
		} io_easyeda;
	} plugins;
} conf_io_easyeda_t;

extern conf_io_easyeda_t io_easyeda_conf;

#endif
