#include <stdio.h>
#include "easyeda_sphash.h"
#include <rnd_inclib/lib_easyeda/gendom.h>

/* Parse an open file into a gdom tree; call gdom_free() to discard it
   after use. Works on "easyeda std" sch files */
gdom_node_t *easystd_low_parse(FILE *f, int is_sym);



