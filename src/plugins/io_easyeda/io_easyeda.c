/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - easyeda format support
 *  Copyright (C) 2024 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include <librnd/core/plugins.h>
#include <librnd/core/conf_multi.h>
#include "read.h"

#include "io_easyeda_conf.h"
#include "conf_internal.c"

conf_io_easyeda_t io_easyeda_conf;

static csch_plug_io_t easystd, easypro;
static char easyeda_cookie[] = "io_easyeda";

static int io_easystd_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "easyeda") && !strstr(fmt, "easyeda") && !strstr(fmt, "sch"))
			return 0;
	}
	if ((type == CSCH_IOTYP_SHEET) || (type == CSCH_IOTYP_GROUP))
		return 90;
	return 0;
}

static int io_easypro_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "easyeda") && !strstr(fmt, "easyeda") && !strstr(fmt, "sch"))
			return 0;
	}
	if ((type == CSCH_IOTYP_SHEET) || (type == CSCH_IOTYP_GROUP))
		return 90;
	return 0;
}

int pplg_check_ver_io_easyeda(int ver_needed) { return 0; }

void pplg_uninit_io_easyeda(void)
{
	csch_plug_io_unregister(&easystd);
	csch_plug_io_unregister(&easypro);
	rnd_conf_plug_unreg("plugins/io_easyeda/", io_easyeda_conf_internal, easyeda_cookie);
}

int pplg_init_io_easyeda(void)
{
	RND_API_CHK_VER;

	easystd.name = "EasyEDA std sheets and symbols";
	easystd.load_prio = io_easystd_load_prio;
	easystd.test_parse = io_easystd_test_parse; /* for syms */
	easystd.load_grp = io_easystd_load_grp;
	easystd.load_sheet_bundled = io_easystd_load_sheet_bundled;
	easystd.test_parse_bundled = io_easystd_test_parse_bundled;
	easystd.end_bundled = io_easystd_end_bundled;
	easystd.ext_save_sheet = "sch";
	easystd.ext_save_grp = "sym";
	csch_plug_io_register(&easystd);

	easypro.name = "EasyEDA pro sheets and symbols";
	easypro.load_prio = io_easypro_load_prio;
	easypro.load_grp = io_easypro_load_grp;
	easypro.test_parse = io_easypro_test_parse; /* for syms */
	easypro.load_sheet_bundled = io_easypro_load_sheet_bundled;
	easypro.test_parse_bundled = io_easypro_test_parse_bundled;
	easypro.end_bundled = io_easypro_end_bundled;
	easypro.ext_save_sheet = "sch";
	easypro.ext_save_grp = "sym";
	csch_plug_io_register(&easypro);


	rnd_conf_plug_reg(io_easyeda_conf, io_easyeda_conf_internal, easyeda_cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(io_easyeda_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "io_easyeda_conf_fields.h"

	return 0;
}

