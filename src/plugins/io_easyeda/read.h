#include <libcschem/plug_io.h>

/*** std ***/
int io_easystd_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);
csch_cgrp_t *io_easystd_load_grp(FILE *f, const char *fn, const char *fmt, csch_sheet_t *sheet);

void *io_easystd_test_parse_bundled(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);
int io_easystd_load_sheet_bundled(void *cookie, FILE *f, const char *fn, csch_sheet_t *dst);
void io_easystd_end_bundled(void *cookie, const char *fn);


/*** pro ***/
int io_easypro_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);

csch_cgrp_t *io_easypro_load_grp(FILE *f, const char *fn, const char *fmt, csch_sheet_t *sheet);

void *io_easypro_test_parse_bundled(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);
int io_easypro_load_sheet_bundled(void *cookie, FILE *f, const char *fn, csch_sheet_t *dst);
void io_easypro_end_bundled(void *cookie, const char *fn);






