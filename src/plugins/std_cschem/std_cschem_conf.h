#ifndef SCH_RND_STD_CSCHEM_CONF_H
#define SCH_RND_STD_CSCHEM_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_BOOLEAN fix_unnamed_syms;      /* If a symbol's name ends in ?, rename it to something unqiue in the abstract model */
		} std_cschem;
	} plugins;
} conf_std_cschem_t;

#endif
