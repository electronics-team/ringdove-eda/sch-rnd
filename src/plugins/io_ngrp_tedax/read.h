/* High level tEDAx parser and glue to core and comp.c */

#include <libcschem/plug_io.h>

int io_ngrp_tedax_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
int io_ngrp_tedax_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);

extern csch_non_graphical_impl_t io_ngrp_tedax_impl;
