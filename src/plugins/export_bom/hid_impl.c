/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - Bill of Materials export
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <librnd/hid/hid.h>
#include <librnd/hid/hid_attrib.h>
#include <librnd/hid/hid_nogui.h>
#include <librnd/hid/hid_init.h>
#include <libcschem/project.h>
#include <sch-rnd/export.h>

static const char bom_cookie[] = "bom export hid";

static rnd_export_opt_t bom_options[] = {
	{"bomfile", "Name of the BoM output file",
	 RND_HATT_STRING, 0, 0, {0, 0, 0}, 0},
#define HA_outfile 0

	{"format", "file format (template)",
	 RND_HATT_ENUM, 0, 0, {0, 0, 0}, NULL},
#define HA_format 1

	{"part-rnd", "export a part-rnd tEDAx that contains the selected template and all data",
	 RND_HATT_BOOL, 0, 0, {0, 0, 0}, NULL},
#define HA_part_rnd 2

	{"view", "Name of the view to export (use first view when not specified)",
	 RND_HATT_STRING, 0, 0, {0, 0, 0}, 0},
#define HA_view 3
};

#define NUM_OPTIONS (sizeof(bom_options)/sizeof(bom_options[0]))

static rnd_hid_attr_val_t bom_values[NUM_OPTIONS];

static const rnd_export_opt_t *bom_get_export_options(rnd_hid_t *hid, int *n, rnd_design_t *dsg, void *appspec)
{
	const char *val = bom_values[HA_outfile].str;

	/* load all formats from the config */
	bom_build_fmts(&conf_bom.plugins.export_bom.templates);

	if (bom_fmt_names.used == 0) {
		rnd_message(RND_MSG_ERROR, "export_bom: can not set up export options: no template available\n");
		return NULL;
	}

	bom_options[HA_format].enumerations = (const char **)bom_fmt_names.array;

	if ((dsg != NULL) && ((val == NULL) || (*val == '\0')))
		csch_derive_default_filename(dsg, 1, &bom_values[HA_outfile], ".txt");

	if (n)
		*n = NUM_OPTIONS;
	return bom_options;
}

static void bom_do_export(rnd_hid_t *hid, rnd_design_t *design, rnd_hid_attr_val_t *options, void *appspec)
{
	csch_sheet_t *sheet = (csch_sheet_t *)design;
	int viewid = CSCH_VIEW_DEFAULT;

	if (!options) {
		bom_get_export_options(hid, 0, design, appspec);
		options = bom_values;
	}

	if ((options[HA_view].str != NULL) && (options[HA_view].str[0] != '\0')) {
		viewid = csch_view_get_id((csch_project_t *)sheet->hidlib.project, options[HA_view].str);
		if (viewid < 0) {
			rnd_message(RND_MSG_ERROR, "No such view in the project: '%s'\n", options[HA_view].str);
			return;
		}
	}

	bom_part_rnd_mode = options[HA_part_rnd].lng ? part_rnd_print : NULL;

	sch_rnd_export_prj_abst((csch_project_t *)sheet->hidlib.project, sheet, viewid, "bom", options[HA_outfile].str, options);
}

static int bom_usage(rnd_hid_t *hid, const char *topic)
{
	fprintf(stderr, "\nBill of Materials exporter command line arguments:\n\n");
	rnd_hid_usage(bom_options, sizeof(bom_options) / sizeof(bom_options[0]));
	fprintf(stderr, "\nUsage: sch-rnd [generic_options] -x bom [options] foo.rs\n\n");
	return 0;
}


static int bom_parse_arguments(rnd_hid_t *hid, int *argc, char ***argv)
{
	rnd_export_register_opts2(hid, bom_options, sizeof(bom_options) / sizeof(bom_options[0]), bom_cookie, 0);
	return rnd_hid_parse_command_line(argc, argv);
}

rnd_hid_t bom_hid = {0};
