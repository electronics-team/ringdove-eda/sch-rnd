/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - object access for scripts
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* includeed from act_read.c */

#include <librnd/core/compat_misc.h>
#include <sch-rnd/search.h>
#include <sch-rnd/draw.h>
#include <sch-rnd/crosshair.h>

static const char csch_acts_RtreeList[] = "RtreeList[rtree_name, [x1, y1, x2, y2])\n";
static const char csch_acth_RtreeList[] = "Return a list of idpaths for objects overlapping with the box specified (empty list if no object is found). Error is indicated by returning nil. If coordinates are not specified the whole tree is searched.";
/* DOC: rtreelist.html */
static fgw_error_t csch_act_RtreeList(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	csch_sheet_t *sheet = CSCH_ACT_SHEET;
	csch_rtree_t *rtree;
	const char *rtree_name;
	csch_rtree_it_t it;
	csch_chdr_t *o;
	csch_oidpath_list_t *list;
	int has_bbox = 0;
	csch_rtree_box_t qbox;

	RND_ACT_CONVARG(1, FGW_STR, RtreeList, rtree_name = argv[1].val.str);
	rtree = csch_rtree_lookup_by_name(sheet, rtree_name);
	if (rtree == NULL) {
		res->type = FGW_VOID | FGW_PTR;
		res->val.ptr_void = 0;
		return 0;
	}

	if (argc == 6) {
		RND_ACT_CONVARG(2, FGW_COORD, RtreeList, qbox.x1 = fgw_coord(&argv[2]));
		RND_ACT_CONVARG(3, FGW_COORD, RtreeList, qbox.y1 = fgw_coord(&argv[3]));
		RND_ACT_CONVARG(4, FGW_COORD, RtreeList, qbox.x2 = fgw_coord(&argv[4]));
		RND_ACT_CONVARG(5, FGW_COORD, RtreeList, qbox.y2 = fgw_coord(&argv[5]));
		has_bbox = 1;
	}

	list = calloc(sizeof(csch_oidpath_list_t), 1);
	fgw_ptr_reg(&rnd_fgw, res, RND_PTR_DOMAIN_IDPATH_LIST, FGW_PTR | FGW_STRUCT, list);

	if (has_bbox) {
		for(o = csch_rtree_first(&it, rtree, &qbox); o != NULL; o = csch_rtree_next(&it)) {
			fgw_arg_t tmp;
			csch_oidpath_t *idp = calloc(sizeof(csch_oidpath_t), 1);
			csch_oidpath_from_obj(idp, o);
			fgw_ptr_reg(&rnd_fgw, &tmp, RND_PTR_DOMAIN_IDPATH, FGW_PTR | FGW_STRUCT, idp);
			csch_oidpath_list_append(list, idp);
		}
	}
	else {
		for(o = csch_rtree_all_first(&it, rtree); o != NULL; o = csch_rtree_all_next(&it)) {
			fgw_arg_t tmp;
			csch_oidpath_t *idp = calloc(sizeof(csch_oidpath_t), 1);
			csch_oidpath_from_obj(idp, o);
			fgw_ptr_reg(&rnd_fgw, &tmp, RND_PTR_DOMAIN_IDPATH, FGW_PTR | FGW_STRUCT, idp);
			csch_oidpath_list_append(list, idp);
		}
	}

	return 0;
}

