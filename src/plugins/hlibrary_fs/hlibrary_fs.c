/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - library of hierarchic child sheets (from the local file system)
 *  Copyright (C) 2024 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2024)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include <libcschem/plug_library.h>
#include <libcschem/util_lib_fs.h>
#include <libcschem/cnc_grp.h>
#include <libcschem/cnc_any_obj.h>

#include <librnd/core/error.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/compat_fs.h>
#include <librnd/core/safe_fs.h>
#include <librnd/core/safe_fs_dir.h>
#include <librnd/core/plugins.h>

static csch_lib_backend_t be_hlibrary_fs;

static char *hlibrary_fs_realpath(rnd_design_t *hl, const char *root)
{
	/* accept only non-prefixed paths */
	if (strchr(root, '@') != NULL)
		return NULL;

	return csch_lib_fs_realpath(hl, root);
}

csch_lib_type_t hlibrary_fs_file_type(rnd_design_t *hl, const char *fn)
{
	int res;

	res = csch_test_parse_fn(hl, fn, CSCH_IOTYP_SHEET);
	if (res == 1)
		return CSCH_SLIB_STATIC;

	return CSCH_SLIB_invalid;
}

static int hlibrary_fs_map(rnd_design_t *hl, csch_lib_t *root_dir)
{
	gds_t tmp = {0};
	gds_append_str(&tmp, root_dir->realpath);
	csch_lib_fs_map(hl, &be_hlibrary_fs, root_dir, &tmp, hlibrary_fs_file_type);
	gds_uninit(&tmp);
	return 0;
}

static int hlibrary_fs_load(csch_sheet_t *sheet, void *dst_, csch_lib_t *src, const char *params)
{
	csch_sheet_t *dst = dst_;
	csch_cgrp_t *grp = NULL;

	switch(src->type) {
		case CSCH_SLIB_PARAMETRIC:
		rnd_message(RND_MSG_ERROR, "hlibrary_fs_load(): can't handle parametric\n");
		return -1;

		case CSCH_SLIB_STATIC:
			TODO("call the loader?");
/*			grp = csch_load_grp(dst, src->realpath, NULL);*/
			return -1;
			break;

		case CSCH_SLIB_invalid:
		case CSCH_SLIB_DIR:
			return -1;
	}

	if (grp == NULL)
		return -1;

	grp->sym_prefer_loclib = 1;
	csch_cgrp_render_all(dst, &dst->direct);
	csch_cobj_update(dst, &dst->direct.hdr, 1);

	return 0;
}

static void hlibrary_fs_free(csch_lib_t *src)
{
}


int pplg_check_ver_hlibrary_fs(int ver_needed) { return 0; }

void pplg_uninit_hlibrary_fs(void)
{

}

int pplg_init_hlibrary_fs(void)
{
	RND_API_CHK_VER;

	be_hlibrary_fs.name = "hlibrary_fs";
	be_hlibrary_fs.realpath = hlibrary_fs_realpath;
	be_hlibrary_fs.map = hlibrary_fs_map;
	be_hlibrary_fs.load = hlibrary_fs_load; /* loads a group into dst sheet */
	be_hlibrary_fs.free = hlibrary_fs_free;

	csch_lib_backend_reg(csch_lib_get_master("hlibrary", 1), &be_hlibrary_fs);

	return 0;
}

