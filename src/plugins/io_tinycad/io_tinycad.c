/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - tinycad file format support
 *  Copyright (C) 2022, 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022 and Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <librnd/core/plugins.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include "read.h"

#include "io_tinycad_conf.h"
#include "conf_internal.c"

conf_io_tinycad_t io_tinycad_conf;
static csch_plug_io_t tinycad;
static char tinycad_cookie[] = "io_tinycad";

static int io_tinycad_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "tinycad") && !strstr(fmt, "tinycad") && !strstr(fmt, "sch"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 90;
	return 0;
}

int pplg_check_ver_io_tinycad(int ver_needed) { return 0; }

void pplg_uninit_io_tinycad(void)
{
	csch_plug_io_unregister(&tinycad);
	rnd_conf_plug_unreg("plugins/io_tinycad/", io_tinycad_conf_internal, tinycad_cookie);
}

int pplg_init_io_tinycad(void)
{
	RND_API_CHK_VER;

	tinycad.name = "tinycad schematics sheet v2 or symbol v1";
	tinycad.load_prio = io_tinycad_load_prio;

	tinycad.test_parse_bundled = io_tinycad_test_parse_bundled;
	tinycad.load_sheet_bundled = io_tinycad_load_sheet_bundled;
	tinycad.end_bundled = io_tinycad_end_bundled;


	tinycad.ext_save_sheet = "dsn";
	csch_plug_io_register(&tinycad);

	rnd_conf_plug_reg(io_tinycad_conf, io_tinycad_conf_internal, tinycad_cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(io_tinycad_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "io_tinycad_conf_fields.h"

	return 0;
}

