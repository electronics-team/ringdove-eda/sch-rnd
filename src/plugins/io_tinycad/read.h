#include <libcschem/plug_io.h>
void *io_tinycad_test_parse_bundled(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);
int io_tinycad_load_sheet_bundled(void *cookie, FILE *f, const char *fn, csch_sheet_t *dst);
void io_tinycad_end_bundled(void *cookie, const char *fn);

