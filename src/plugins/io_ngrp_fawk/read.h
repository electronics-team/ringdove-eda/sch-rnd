/* High level fawk parser and glue to core and comp.c */

#include <libcschem/plug_io.h>
#include <libfungw/fungw.h>
#include <puplug/puplug.h>

extern pup_context_t io_ngrp_fawk_pup;


int io_ngrp_fawk_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
int io_ngrp_fawk_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);

extern csch_non_graphical_impl_t io_ngrp_fawk_impl;
extern const fgw_eng_t fgw_ngrp_fawk_eng;
