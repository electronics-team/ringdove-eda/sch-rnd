/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - fawk non-graphical sheet support
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include <libcschem/non_graphical.h>
#include <librnd/core/plugins.h>
#include "read.h"

static csch_plug_io_t fawkv1;

static int io_ngrp_fawk_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "fawk"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 100;
	return 0;
}

static int io_ngrp_fawk_save_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "fawk"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 100;
	return 0;
}


int pplg_check_ver_io_ngrp_fawk(int ver_needed) { return 0; }

void pplg_uninit_io_ngrp_fawk(void)
{
	csch_non_graphical_impl_pre_unload(&io_ngrp_fawk_impl);
	csch_plug_io_unregister(&fawkv1);
	fgw_eng_unreg(fgw_ngrp_fawk_eng.name);
	pup_uninit(&io_ngrp_fawk_pup);
}

int pplg_init_io_ngrp_fawk(void)
{
	RND_API_CHK_VER;

	fawkv1.name = "fawk non-graphical schematic sheet";
	fawkv1.load_prio = io_ngrp_fawk_load_prio;
	fawkv1.save_prio = io_ngrp_fawk_save_prio;
	fawkv1.load_sheet = io_ngrp_fawk_load_sheet;
	fawkv1.test_parse = io_ngrp_fawk_test_parse;

	fawkv1.ext_save_sheet = "fawk";
	csch_plug_io_register(&fawkv1);

	pup_init(&io_ngrp_fawk_pup);
	fgw_eng_reg(&fgw_ngrp_fawk_eng);


	return 0;
}

