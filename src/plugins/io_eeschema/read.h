#include <libcschem/plug_io.h>
int io_eeschema_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
csch_cgrp_t *io_eeschema_load_grp(FILE *f, const char *fn, const char *fmt, csch_sheet_t *sheet);
int io_eeschema_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);



