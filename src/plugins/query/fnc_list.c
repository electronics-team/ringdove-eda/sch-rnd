/*
 *                            COPYRIGHT
 *
 *  pcb-rnd, interactive printed circuit board design
 *  Copyright (C) 2020 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

/* Query language - listing functions */

#define PCB dontuse

static int fnc_mklist(pcb_qry_exec_t *ectx, int argc, pcb_qry_val_t *argv, pcb_qry_val_t *res)
{
	int n;
	res->type = PCBQ_VT_LST;
	vtp0_init(&res->data.lst);
	for(n = 0; n < argc; n++) {
		if (argv[n].type == PCBQ_VT_OBJ)
			vtp0_append(&res->data.lst, argv[n].data.obj);
	}
	return 0;
}

static int fnc_violation(pcb_qry_exec_t *ectx, int argc, pcb_qry_val_t *argv, pcb_qry_val_t *res)
{
	int n;

	if ((argc < 2) || (argc % 2 != 0))
		return -1;

	/* argument sanity checks */
	for(n = 0; n < argc; n+=2) {
		pcb_qry_val_t *val = &argv[n+1];
		pcb_qry_drc_ctrl_t ctrl = pcb_qry_drc_ctrl_decode(argv[n].data.obj);
		switch(ctrl) {
			case PCB_QRY_DRC_GRP1:
			case PCB_QRY_DRC_GRP2:
				if (val->type != PCBQ_VT_OBJ)
					return -1;
				break;
			case PCB_QRY_DRC_EXPECT:
			case PCB_QRY_DRC_MEASURE:
				break; /* accept as is */
			case PCB_QRY_DRC_TEXT:
				break; /* accept anything */
			default:
				return -1;
		}
	}

	res->type = PCBQ_VT_LST;
	vtp0_init(&res->data.lst);
	for(n = 0; n < argc; n+=2) {
		pcb_qry_drc_ctrl_t ctrl = pcb_qry_drc_ctrl_decode(argv[n].data.obj);
		pcb_qry_val_t *val = &argv[n+1];
		pcb_obj_qry_const_t *tmp;

		if (ctrl == PCB_QRY_DRC_TEXT) {
			char *str = "<invalid node>", buff[128];
			switch(val->type) {
				case PCBQ_VT_VOID:   str = "<void>"; break;
				case PCBQ_VT_OBJ:    str = "<obj>"; break;
				case PCBQ_VT_LST:    str = "<list>"; break;
				case PCBQ_VT_COORD:  rnd_snprintf(buff, sizeof(buff), "%ld", argv[n+1].data.crd); str = buff; break;
				case PCBQ_VT_LONG:   rnd_snprintf(buff, sizeof(buff), "%ld", argv[n+1].data.lng); str = buff; break;
				case PCBQ_VT_DOUBLE: rnd_snprintf(buff, sizeof(buff), "%f", argv[n+1].data.dbl); str = buff; break;
				case PCBQ_VT_STRING: str = (char *)argv[n+1].data.str; break;
			}
			str = rnd_strdup(str == NULL ? "" : str);
			vtp0_append(&res->data.lst, argv[n].data.obj);
			vtp0_append(&res->data.lst, str);
			vtp0_append(&ectx->autofree, str);
		}
		else if ((ctrl == PCB_QRY_DRC_EXPECT) || (ctrl == PCB_QRY_DRC_MEASURE)) {
			tmp = malloc(sizeof(pcb_obj_qry_const_t));
			tmp->val = *val;
			tmp->hdr.type = PCB_OBJ_QRY_CONST;
			vtp0_append(&res->data.lst, argv[n].data.obj);
			vtp0_append(&res->data.lst, tmp);
			vtp0_append(&ectx->autofree, tmp);
		}
		else switch(val->type) {
			case PCBQ_VT_OBJ:
				vtp0_append(&res->data.lst, argv[n].data.obj);
				vtp0_append(&res->data.lst, argv[n+1].data.obj);
				break;
			case PCBQ_VT_COORD:
			case PCBQ_VT_LONG:
			case PCBQ_VT_DOUBLE:
				tmp = malloc(sizeof(pcb_obj_qry_const_t));
				memcpy(&tmp->val, val, sizeof(pcb_qry_val_t));
				vtp0_append(&res->data.lst, argv[n].data.obj);
				vtp0_append(&res->data.lst, tmp);
				vtp0_append(&ectx->autofree, tmp);
				break;
			case PCBQ_VT_VOID:
			case PCBQ_VT_LST:
			case PCBQ_VT_STRING:
				/* can't be on a violation list at the moment */
				break;
		}
	}
	return 0;
}

