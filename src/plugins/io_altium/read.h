#include <libcschem/plug_io.h>
int io_altium_bin_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
int io_altium_bin_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);

int io_altium_ascii_load_sheet(FILE *f, const char *fn, const char *fmt, csch_sheet_t *dst);
int io_altium_ascii_test_parse(FILE *f, const char *fn, const char *fmt, csch_plug_io_type_t type);
