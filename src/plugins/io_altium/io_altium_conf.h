#ifndef SCH_RND_IO_ALTIUM_CONF_H
#define SCH_RND_IO_ALTIUM_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_REAL coord_mult;               /* all altium coordinates are multiplied by this value to get sch-rnd coords */
			RND_CFT_BOOLEAN emulate_text_ang_180;  /* altium displays text objects with angle==180 with an extra 180 degree rotation; it's a display hack sch-rnd doesn't have; when this emulation is enabled, the loader adds a +180 degree rotation in such text (changing data!) to match the behavior */
			RND_CFT_BOOLEAN auto_normalize;        /* move all objects so that starting coords are near 0;0 */
			RND_CFT_LIST postproc_sheet_load;      /* pattern;action pairs for object transformations after a succesful load; mostly used for attribute editing */
			RND_CFT_BOOLEAN rename_redundant_pins; /* if pin names are not unique within a symbol, rename all instances */
		} io_altium;
	} plugins;
} conf_io_altium_t;

extern conf_io_altium_t io_altium_conf;

#endif
