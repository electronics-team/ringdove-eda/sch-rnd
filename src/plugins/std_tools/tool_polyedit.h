#ifndef STD_TOOLS_POLYEDIT_H
#define STD_TOOLS_POLYEDIT_H

/* returns 0 if accepted */
static int tool_polyedit_press_at(rnd_design_t *hl, csch_coord_t x, csch_coord_t y, csch_cpoly_t *poly, long obj_idx[2], int ret_arrow);

#endif