#ifndef STD_TOOLS_MOVECOPY_H
#define STD_TOOLS_MOVECOPY_H

static void tool_movecopy_press_at(rnd_design_t *hl, csch_coord_t x, csch_coord_t y, csch_chdr_t *obj, int ret_arrow);

#endif