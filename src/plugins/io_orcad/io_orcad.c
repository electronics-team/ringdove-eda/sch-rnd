/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - OrCAD format support
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust Fund in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include <stdio.h>
#include <string.h>
#include <librnd/core/plugins.h>
#include <libcschem/config.h>
#include <libcschem/plug_io.h>
#include "read.h"

#include "io_orcad_conf.h"
#include "conf_internal.c"

conf_io_orcad_t io_orcad_conf;
static char orcad_cookie[] = "io_orcad";

static csch_plug_io_t orcad;

static int io_orcad_common_load_prio(const char *fn, const char *fmt, csch_plug_io_type_t type)
{
	if (fmt != NULL) {
		if (!strstr(fmt, "orcad") && !strstr(fmt, "dsn"))
			return 0;
	}
	if (type == CSCH_IOTYP_SHEET)
		return 90;
	return 0;
}

int pplg_check_ver_io_orcad(int ver_needed) { return 0; }

void pplg_uninit_io_orcad(void)
{
	csch_plug_io_unregister(&orcad);
	rnd_conf_plug_unreg("plugins/io_orcad/", io_orcad_conf_internal, orcad_cookie);
}

int pplg_init_io_orcad(void)
{
	RND_API_CHK_VER;

	orcad.name = "orcad schematics sheets from dsn (cdf)";
	orcad.load_prio = io_orcad_common_load_prio;
	orcad.test_parse_bundled = io_orcad_test_parse_bundled;
	orcad.load_sheet_bundled = io_orcad_load_sheet_bundled;
	orcad.end_bundled = io_orcad_end_bundled;


	orcad.ext_save_sheet = "dsn";
	csch_plug_io_register(&orcad);

	rnd_conf_plug_reg(io_orcad_conf, io_orcad_conf_internal, orcad_cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(io_orcad_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "io_orcad_conf_fields.h"

	return 0;
}

