#ifndef FIO_H
#define FIO_H

#include <stdio.h>

#include "read.h"
#include <genht/htip.h>
#include <libucdf/ucdf.h>
#include <plugins/lib_alien/read_helper.h>

typedef struct io_orcad_rctx_s {
	const char *fn;
	csch_alien_read_ctx_t alien;
	ucdf_ctx_t ucdf;
	ucdf_file_t fp;
	unsigned has_fp:1;

	char *cheat_buf;
	long cheat_offs, cheat_len;

	ucdf_direntry_t *next_page;

	/* high level */
	struct orcad_page_node *page_root;
	struct orcad_xcache_node *cache_root;
	struct orcad_xlibrary_node *library_root;
	struct { /* look up a few commonly used strings in library_root->names and cache their indices to speed up lookups */
		unsigned long part_ref;
		unsigned long value;
		unsigned long name;
	} hwpropnames;

	/* per file */
	htsp_t syms;               /* symbols loaded from the Cache file; key: symbol name; val: (cache_sym_t *) */
	unsigned cache_loaded:1;   /* 1 if the Cache file got loaded and ->syms got initialized */
	unsigned library_loaded:1; /* 1 if the Library file got loaded and ->library_root got initialized and hwpropnames initialized */


	/* per page */
	htip_t wirenets; /* cache of net_id -> wirenet (csch_cgrp_t *) */
	htip_t pinconns; /* cache of net_id -> terminal (csch_cgrp_t *) */
} io_orcad_rctx_t;

int fio_fopen(io_orcad_rctx_t* const rctx, const char* filename);
long fio_fread(io_orcad_rctx_t* const rctx, char* dst, long len);
int fio_fseek(io_orcad_rctx_t* const rctx, long offs);
int fio_fclose(io_orcad_rctx_t* const rctx); /* not in libucdf */

#endif /* FIO_H */
