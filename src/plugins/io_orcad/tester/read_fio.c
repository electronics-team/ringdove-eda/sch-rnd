#include "read_fio.h"
#include <stddef.h>
#include <errno.h>

int fio_fopen(io_orcad_rctx_t* const rctx, const char* filename)
{
	if(NULL!=(rctx->file=fopen(filename, "rb")))
	{
		return 0;
	}

	rctx->err = errno;

	return -1;
}

long fio_fread(io_orcad_rctx_t* const rctx, char* dst, long len)
{
	return fread(dst, 1, len, rctx->file);
}

int fio_fseek(io_orcad_rctx_t* const rctx, long offs)
{
	return fseek(rctx->file, offs, SEEK_SET);
}

int fio_fclose(io_orcad_rctx_t* const rctx)
{
	fclose(rctx->file);
}
