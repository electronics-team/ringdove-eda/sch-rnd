#include "read_fio.h"
#include "read_parse.h"
#include <stdio.h>
#include <string.h>

int main(int argc, char* argv[])
{
	int i;

	if(1>=argc)
	{
		fprintf(stderr, "Usage: %s ORCAD_CACHE_FILE...\n", argv[0]);
		return 1;
	}

	for(i=1;i<argc;++i)
	{
		struct orcad_node* node;

		io_orcad_rctx_t rctx;
		memset(&rctx, 0, sizeof(rctx));
		rctx.fn = argv[i];

		printf(":: Processing %s\n", rctx.fn);

		if(0!=fio_fopen(&rctx, rctx.fn))
		{
			fprintf(stderr, "Error: Could not open '%s' for read: %s\n",
				rctx.fn, strerror(rctx.err));
			continue;
		}

		if(NULL!=(node=orcad_read_cache(&rctx)))
		{
			orcad_dump(node);
			orcad_free(node);
		}

		fio_fclose(&rctx);
	}

	return 0;
}
