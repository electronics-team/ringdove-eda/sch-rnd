#include "read_fio.h"
#include <stddef.h>
#include <errno.h>

int fio_fopen(io_orcad_rctx_t* const rctx, const char* filename)
{
	return -1;
}

long fio_fread(io_orcad_rctx_t* const rctx, char* dst, long len)
{
	if (!rctx->has_fp)
		return -1;

	if (rctx->cheat_offs + len > rctx->cheat_len)
		len = rctx->cheat_len - rctx->cheat_offs;

	if (len < 0)
		return -1;
	if (len == 0)
		return 0;

	memcpy(dst, rctx->cheat_buf + rctx->cheat_offs, len);
	rctx->cheat_offs += len;
	return len;

	return ucdf_fread(&rctx->fp, dst, len);
}

int fio_fseek(io_orcad_rctx_t* const rctx, long offs)
{
	if (!rctx->has_fp)
		return -1;
	rctx->cheat_offs = offs;
	return 0;
	return ucdf_fseek(&rctx->fp, offs);
}

int fio_fclose(io_orcad_rctx_t* const rctx)
{
	return 0;
}
