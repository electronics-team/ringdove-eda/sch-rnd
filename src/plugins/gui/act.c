/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - GUI
 *  Copyright (C) 2020,2023 Tibor 'Igor2' Palinkas
 *  copied from pcb-rnd, interactive printed circuit board design
 *  Copyright (C) 2017..2019 Tibor 'Igor2' Palinkas
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/pcb-rnd
 *    lead developer: http://repo.hu/projects/pcb-rnd/contact.html
 *    mailing list: pcb-rnd (at) list.repo.hu (send "subscribe")
 */

#include <libcschem/config.h>

#include <librnd/config.h>

#include <librnd/core/actions.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/conf.h>
#include <librnd/core/rnd_conf.h>
#include <librnd/hid/hid.h>
#include <librnd/core/hidlib.h>
#include <librnd/plugins/lib_hid_common/zoompan.h>

#include <libcschem/concrete.h>
#include <sch-rnd/select.h>

#include "act.h"

const char csch_acts_Zoom_[] =
	rnd_gui_acts_zoom
	"Zoom(selected)\n";
const char csch_acth_Zoom[] = "GUI zoom";
/* DOC: zoom.html */
fgw_error_t csch_act_Zoom(fgw_arg_t *res, int argc, fgw_arg_t *argv)
{
	rnd_design_t *hidlib = RND_ACT_DESIGN;
	csch_sheet_t *sheet = (csch_sheet_t *)hidlib;
	const char *csch_acts_Zoom = csch_acts_Zoom_;
	rnd_acts_Zoom = csch_acts_Zoom_;

	RND_GUI_NOGUI();

	if (argc == 2) {
		const char *vp;

		RND_ACT_CONVARG(1, FGW_STR, Zoom, vp = argv[1].val.str);

		if (rnd_strcasecmp(vp, "selected") == 0) {
			rnd_box_t sb;
			if (sch_rnd_get_selection_bbox_gui(&sb, sheet) > 0)
				rnd_gui->zoom_win(rnd_gui, sb.X1, sb.Y1, sb.X2, sb.Y2, 1);
			else
				rnd_message(RND_MSG_ERROR, "Can't zoom to selection: nothing selected\n");
			return 0;
		}
	}

	return rnd_gui_act_zoom(res, argc, argv);
}
