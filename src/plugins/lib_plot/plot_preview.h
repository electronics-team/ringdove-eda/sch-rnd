/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - generic graph plotting
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <librnd/hid/hid.h>
#include <librnd/hid/hid_dad.h>
#include <plugins/lib_plot/plot_data.h>

typedef struct plot_preview_s  plot_preview_t;

struct plot_preview_s {
	/* config */
	plot_axis_type_t type_x;
	plot_data_t pdata;
	void (*readout_cb)(plot_preview_t *ctx, int trace_idx, long x, double y);
	void (*readout_begin_cb)(plot_preview_t *ctx, long x); /* optional: called on a readout for an x, before the first trace */
	void (*readout_end_cb)(plot_preview_t *ctx, long x);   /* optional: called on a readout for an x, after the last trace */
	void *user_data;
	void *hid_ctx;  /* caller needs to fill this in before the first draw */
	int widget_idx; /* caller needs to fill this in before the first draw (RND_DAD_CURRENT()) */
	const rnd_color_t *grid_color; /* x;y value background grid; use default if NULL */

	/* runtime state */
	double miny, maxy;
	long maxx;
	unsigned inited:1;

	double zoom_y;    /* y zoom is independent of the number of data points (x direction); multiplier */

};

/* This is the expose function to be bound to the preview widget, with
   user_ctx_ pointing to csch_plot_preview_t */
void plot_preview_expose_cb(rnd_hid_attribute_t *attrib, rnd_hid_preview_t *prv, rnd_hid_gc_t gc, rnd_hid_expose_ctx_t *e);


/* Performs a readoutat coords clicked */
rnd_bool plot_mouse_cb(rnd_hid_attribute_t *attrib, rnd_hid_preview_t *prv, rnd_hid_mouse_ev_t kind, rnd_coord_t x, rnd_coord_t y);

/* Zoom to window; x1;y1 < x2;y2 */
void plot_zoomto(rnd_hid_attribute_t *attrib, plot_preview_t *ctx, double x1, double y1, double x2, double y2);

/* Redraw content of the plot, keeping zoom/pan; useful when data or config changes */
void plot_redraw(rnd_hid_attribute_t *attrib);
