/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - (pin alternate) function map
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust Fund in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

/*** local lib support ***/

static int funcmap_lht_map_local(rnd_design_t *hl, csch_lib_t *root_dir, const csch_cgrp_t *indirect)
{
	csch_sheet_t *sheet = (csch_sheet_t *)hl;
	anymap_ctx_t *actx = funcmap_get_anymap(sheet);
	const csch_cgrp_t *root_grp;

	/* do not automatically create a local funcmap dir (we are at opening a sheet) */
	root_grp = csch_loclib_get_root(sheet, devmaster, NULL, 0, NULL);
	if (root_grp != NULL)
		anymap_sheet_init_(actx, hl, root_dir, root_grp);

	return 0;
}

static void funcmap_sheet_init(anymap_ctx_t *actx, csch_sheet_t *sheet, csch_lib_t **root_dir_out, int alloc)
{
	csch_lib_t *root_dir = NULL;
	int alloced;
	csch_cgrp_t *root_grp;
	csch_source_arg_t *src;

	src = csch_attrib_src_p("funcmap", NULL);
	if (csch_loclib_get_roots(&root_dir, &root_grp, devmaster, sheet, src, alloc, &alloced) == 0)
		anymap_sheet_init_(actx, &sheet->hidlib, root_dir, root_grp);

	if (root_dir_out != NULL)
		*root_dir_out = root_dir;
}

static int funcmap_lht_load(csch_sheet_t *sheet, void *dst_, csch_lib_t *src, const char *params)
{
	/* real loading happens through funcmap_lib_lookup(); this is called from the
	   lib window (will be needed for the preview) */
	return -1;
}

static int funcmap_loc_list(csch_sheet_t *sheet, csch_lib_t *src)
{
	return anymap_loc_list(sheet, src, "funcmap");
}

static int funcmap_loc_refresh_from_ext(csch_sheet_t *sheet, csch_lib_t *src)
{
	anymap_ctx_t *actx = funcmap_get_anymap(sheet);
	return anymap_loc_refresh_from_ext(actx, sheet, src);
}

