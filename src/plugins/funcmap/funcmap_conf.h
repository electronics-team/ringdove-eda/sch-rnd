#ifndef SCH_RND_FUNCMAP_CONF_H
#define SCH_RND_FUNCMAP_CONF_H

#include <librnd/core/conf.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_BOOLEAN drc_disable;   /* if set: do not run the funcmap Design Rule Checker; funcmap DRC checks foralternative pin functionality grouping violations */
			RND_CFT_LIST search_paths;     /* ordered list of paths that are each recursively searched for funcmap files */
		} funcmap;
	} plugins;
} conf_funcmap_t;

#endif
