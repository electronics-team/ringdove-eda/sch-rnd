#ifndef PCB_PROPDLG_H
#define PCB_PROPDLG_H

#include <librnd/core/actions.h>

extern const char csch_acts_propedit[];
extern const char csch_acth_propedit[];
fgw_error_t csch_act_propedit(fgw_arg_t *res, int argc, fgw_arg_t *argv);

void csch_propdlg_init(void);
void csch_propdlg_uninit(void);

#endif
