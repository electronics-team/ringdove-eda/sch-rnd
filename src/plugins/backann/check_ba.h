/* Execute means: check back annotation entries against the abstract model. */

/* execute a single entry of the list; returns 0 if the next entry could be
   executed or -1 if the chain breaks */
int sch_rnd_backann_check_entry(sch_rnd_backann_t *ctx, sch_rnd_ba_t *ba, long *pend);


/* Execute all entries from the list and return the number of entries
   that potentially need user action; 0 means everything has been back
   annotated succesfully */
long sch_rnd_backann_check(sch_rnd_backann_t *ctx);
