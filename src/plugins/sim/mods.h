#include <libcschem/abstract.h>
#include <libcschem/project.h>
#include <plugins/sim/sim.h>

/* Look at the current project's setup called setup_name and execute all
   the mods it has on abst. If setup_name is NULL, get it from the current
   config. */
int sch_sim_mods_perform(csch_project_t *prj, const char *setup_name, csch_abstract_t *abst, sch_sim_exec_t *sim_exec, int eng_prio);


/* Look up addr and return the corresponding network; addr is either a netname
   or a component-port pair. If create is 1, enable creating new abstract nets,
   else NULL is returned when net is not found */
csch_anet_t *sch_sim_lookup_net(csch_abstract_t *abst, const char *addr, int create);
