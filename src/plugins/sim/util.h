#include <genvector/gds_char.h>
#include <liblihata/dom.h>
#include <libfungw/fungw.h>

#include <libcschem/abstract.h>

#include <plugins/sim/sim.h>

/* print the content of a sim mod item (hash) appending it to tmp */
void sch_sim_append_print_mod(gds_t *tmp, lht_node_t *nmod, const char *fld_sep);

/* Allocate and build an analysis or presentation from an named output lihata
   subtree (of a sim setup). Returns 0 on success. */
int sch_sim_analysis_build(sch_sim_analysis_t *dst, csch_abstract_t *abst, lht_node_t *noutput, int quiet);
int sch_sim_presentation_build(sch_sim_presentation_t *dst, csch_abstract_t *abst, lht_node_t *noutput, int quiet);

/* Free all fields (but not the pointer itself) */
void sch_sim_analysis_free(sch_sim_analysis_t *sa);
void sch_sim_presentation_free(sch_sim_presentation_t *sp);


typedef enum {SCH_SIMREQ_NO=0, SCH_SIMREQ_MANDATORY, SCH_SIMREQ_OPTIONAL} sch_sim_field_req_t;

typedef struct {
	sch_sim_field_req_t start, stop, incr, incr_max, numpt, port1, port2, src;
} sch_sim_analysis_field_tab_t;

/* Return the field req table row for type; returns NULL if type is invalid */
const sch_sim_analysis_field_tab_t *sch_sim_get_analysis_fieldreq(sch_sim_analysis_type_t type);


/* Replace funcname in obj with newf; save the original function in origf.
   Useful for e.g. binding target_spice but then hooking into some of its
   engine calls. */
void sch_sim_hook_eng_call(fgw_obj_t *obj, const char *funcname, fgw_error_t (**origf)(fgw_arg_t *, int , fgw_arg_t *), fgw_error_t (*newf)(fgw_arg_t *, int , fgw_arg_t *));


/* Called from compile_project_before and compile_project_after hooks of
   sim_ngspice and sim_* backends in general, to temporarily change the
   test bench for cmpilation as requested by the currently active sim setup
   and then to restore the original value in the config after compilation*/
void sch_sim_set_test_bench(csch_project_t *prj, csch_abstract_t *abst, const char *cookie, int engprio);
void sch_sim_restore_test_bench(csch_project_t *prj, csch_abstract_t *abst, const char *cookie, int engprio);

/* Called in component0 to apply the omit_no_test_bench mechanism */
void sch_sim_omit_no_test_bench_comp(csch_acomp_t *comp, int eng_prio);
void sch_sim_omit_no_test_bench_all(csch_project_t *prj, csch_abstract_t *abst, int engprio);

/* Returns whether omit_no_test_bench is configured in the currently active
   sim setup for prj */
int sch_sim_omit_no_test_bench_is_on(csch_project_t *prj);
