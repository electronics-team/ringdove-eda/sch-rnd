#include <plugins/sim/sim.h>


/*** for direct calls from the sim_gui plugin ***/
int sch_sim_activate(csch_project_t *prj, const char *sim_setup_name, const char *view_name, int compile_now);

sch_sim_setup_t *sch_sim_run_prepare(csch_project_t *prj, const char *setup_name);
int sch_sim_exec(csch_project_t *prj, sch_sim_setup_t *ssu);
void sch_sim_free(csch_project_t *prj, sch_sim_setup_t *ssu);


/*** internal calls ***/
void sch_sim_uninit_act(const char *sim_cookie);
void sch_sim_init_act(const char *sim_cookie);
