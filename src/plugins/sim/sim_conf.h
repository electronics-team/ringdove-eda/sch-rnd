#ifndef SCH_RND_SIM_CONF_H
#define SCH_RND_SIM_CONF_H

#include <librnd/core/conf.h>
#include <libcschem/project.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_HLIST setups;        /* simulation setups, insluding test bench spec and analysis and plot */
			RND_CFT_STRING active_setup; /* name of the setup last activated; used when compiling */
			RND_CFT_BOOLEAN preserve_tmp;/* do not remove the temp dir after executing the simulator; useful for debugging */
		} sim;
	} plugins;
} conf_sim_t;

extern conf_sim_t sch_sim_conf;

#define SCH_SIM_CONF_SETUPS_PATH "plugins/sim/setups"

/* evaluates to (rnd_conflist_t *) of the setups hlist */
#define SCH_SIM_CONF_SETUPS_CONLIST sch_sim_conf.plugins.sim.setups

/* Look up hashlist node for prj:name in setups. Switch to first sheet of
   prj if prj is not the current project. If create is non-zero, create the
   subtree in the project file's config if it does not exist. */
lht_node_t *sch_sim_get_setup(csch_project_t *prj, const char *name, int create);

/* Look up hashlist node for prj:setup:output. Switch to first sheet of
   prj if prj is not the current project. If create is non-zero, create the
   subtree in the project file's config if it does not exist. */
lht_node_t *sch_sim_get_output(csch_project_t *prj, const char *setup_name, const char *output_name, int create);

/* Merge all pending changes of the project config and save it in the
   project file (create the file if needed). Return 0 on success. */
int sch_sim_flush_prj_file(csch_project_t *prj);

/* Make sure parent_hash has an LHT_TEXT child called chname with the value
   newval. Returns:
    0: the node already exists and has the correct value
    1: the node had to be created or modified
   -1: error (parent_hash doesn't exist or is not a hash) */
int sch_sim_update_text_node(lht_node_t *parent_hash, const char *chname, const char *newval);

/* Ensure the named child node of a hash exists and has the right type;
   if it doesn't exist, create it. Returns NULL on error (parent_hash is not
   a hash or child exists but has a different type) */
lht_node_t *sch_sim_lht_dom_hash_ensure(lht_node_t *parent_hash, lht_node_type_t type, const char *node_name);

/* Remove all children of hash */
void sch_sim_lht_dom_hash_clean(lht_node_t *hash);

#endif
