#include <libcschem/abstract.h>

/* Get the output refdes/pinnum/netname from abstract attributes */
const char *sch_nle_get_refdes(const csch_acomp_t *comp);
const char *sch_nle_get_pinnum(const csch_aport_t *port);
const char *sch_nle_get_netname(const csch_anet_t *net);

/* Pass a NULL terminated list of attribute const char *keys in the order
   of preference; returns the first attribute value that exists or NULL
   if none found. */
const char *sch_nle_get_alt_attr(const csch_attribs_t *attribs, ...);
