
#ifdef YY_QUERY_API_VER
#define YY_BYACCIC
#define YY_API_MAJOR 1
#define YY_API_MINOR 0
#endif /*YY_QUERY_API_VER*/

#define forgecond_EMPTY        (-1)
#define forgecond_clearin      (forgecond_chr = forgecond_EMPTY)
#define forgecond_errok        (forgecond_errflag = 0)
#define forgecond_RECOVERING() (forgecond_errflag != 0)
#define forgecond_ENOMEM       (-2)
#define forgecond_EOF          0
#line 16 "../plugins/std_forge/cond_gram.c"
#include "../plugins/std_forge/cond_gram.h"
static const forgecond_int_t forgecond_lhs[] = {                   -1,
    0,    1,    1,    1,    1,    1,    1,    1,    1,    1,
    1,    1,    1,    1,    1,    1,    1,    1,    1,    1,
    1,    1,    2,    2,    3,    3,    3,    4,    4,    5,
    6,    5,    5,
};
static const forgecond_int_t forgecond_len[] = {                    2,
    1,    3,    4,    4,    4,    4,    4,    3,    3,    4,
    3,    3,    3,    3,    3,    3,    3,    4,    4,    2,
    2,    1,    1,    2,    1,    2,    2,    1,    3,    1,
    0,    4,    2,
};
static const forgecond_int_t forgecond_defred[] = {                 0,
   23,   25,    0,    0,    0,    0,    0,    0,    0,    0,
   30,    0,   20,   21,    0,   33,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,   24,   27,
   26,    0,    0,    0,    2,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    9,    0,
    8,    0,    0,    0,   32,    0,    0,    5,   10,    7,
    6,    3,    4,
};
static const forgecond_int_t forgecond_dgoto[] = {                  7,
    8,    9,   10,   11,   12,   17,
};
static const forgecond_int_t forgecond_sindex[] = {                -7,
    0,    0,   -7,   -7,   -7,  -10,    0,  158, -226,  -41,
    0,  -22,    0,    0,  132,    0, -224,  -33,  -30,   -7,
   -7,   -7,   -7,   -7,  -29,  -24,  -20,  -17,    0,    0,
    0, -224,  -21,  -16,    0,  -28,   -7,  234,   -7,  260,
  267,  267,   76,   76,   76,   -7,   -7,   -7,    0,   -7,
    0, -228,  -32,  -32,    0,  234,  260,    0,    0,    0,
    0,    0,    0,
};
static const forgecond_int_t forgecond_rindex[] = {                 0,
    0,    0,    0,    0,    0, -205,    0,   55,    9,   35,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,    0,    0,    0,
    0,    0,    0,    0,    0,    0,    0,   18,    0,   74,
   19,   43,   87,  107,  119,    0,    0,    0,    0,    0,
    0,   61,    0,    0,    0,   22,   75,    0,    0,    0,
    0,    0,    0,
};
static const forgecond_int_t forgecond_gindex[] = {                 0,
  241,    0,    4,    0,   -5,    0,
};
#define forgecond_TABLESIZE 329
static const forgecond_int_t forgecond_table[] = {                  4,
    6,    6,    4,    6,   32,   55,    5,   39,   22,    5,
   34,    3,    4,    6,    3,    4,    6,   17,   11,    5,
   36,   19,    5,   16,    3,    4,    6,    3,   30,   31,
   29,   46,    5,    2,   28,   52,   47,    3,   33,   53,
   48,   22,   12,   50,   54,   22,   22,   62,   63,   22,
   22,   22,   31,   22,    1,   22,   11,    0,   17,   11,
   29,   11,   19,   11,    0,    0,    0,   28,   22,   22,
   22,   28,   28,   16,   18,   28,   28,   28,    0,   28,
   12,   28,    0,   12,    0,   12,   13,   12,    0,    0,
   37,    0,    0,   29,   28,   28,   28,   29,   29,    0,
    0,   29,   29,   29,    0,   29,   14,   29,   26,    0,
    0,   16,   18,    0,   16,   18,    0,    0,   15,    0,
   29,   29,   29,   13,   13,    0,    0,   13,   13,   13,
    0,   13,   22,   13,    0,   27,   25,   28,    0,    0,
    0,   17,   11,   14,   14,   19,    0,   14,   14,   14,
    0,   14,    0,   14,    0,   15,   15,    0,   28,   15,
   15,   15,    0,   15,   26,   15,   12,    0,   24,   19,
    0,    0,   35,   22,   20,    0,   21,    0,   23,    0,
    0,    0,    0,    0,   29,    0,    0,    0,    0,    0,
   26,   27,   25,   28,   24,   19,    0,   16,   18,   22,
   20,    0,   21,    0,   23,    0,    0,    0,    0,    0,
   13,    0,    0,    0,    0,   30,   31,   27,   25,   28,
    0,    0,    0,    1,    2,    2,    1,    2,   30,   31,
   14,    0,    0,    0,    0,    0,    1,    2,    0,    1,
    2,    0,   15,   13,   14,   15,    0,    0,    0,    1,
    2,    0,    0,    0,    0,   18,    0,    0,   38,   40,
   41,   42,   43,   44,   45,    0,   26,   49,   51,    0,
   24,   19,    0,    0,    0,   22,   20,   56,   21,   57,
   23,   18,    0,    0,    0,    0,   58,   59,   60,    0,
   61,    0,   26,   27,   25,   28,   24,    0,    0,   26,
    0,   22,   20,   24,   21,    0,   23,    0,   22,    0,
    0,    0,    0,   23,    0,    0,    0,    0,    0,   27,
   25,   28,    0,    0,    0,    0,   27,   25,   28,
};
static const forgecond_int_t forgecond_check[] = {                 33,
   34,   34,   33,   34,   46,   34,   40,   38,    0,   40,
   33,   45,   33,   34,   45,   33,   34,    0,    0,   40,
   17,    0,   40,   34,   45,   33,   34,   45,  257,  258,
  257,   61,   40,  258,    0,   32,   61,   45,   61,   61,
   61,   33,    0,   61,   61,   37,   38,   53,   54,   41,
   42,   43,  258,   45,    0,   47,   38,   -1,   41,   41,
    0,   43,   41,   45,   -1,   -1,   -1,   33,   60,   61,
   62,   37,   38,    0,    0,   41,   42,   43,   -1,   45,
   38,   47,   -1,   41,   -1,   43,    0,   45,   -1,   -1,
  124,   -1,   -1,   33,   60,   61,   62,   37,   38,   -1,
   -1,   41,   42,   43,   -1,   45,    0,   47,   33,   -1,
   -1,   38,   38,   -1,   41,   41,   -1,   -1,    0,   -1,
   60,   61,   62,   37,   38,   -1,   -1,   41,   42,   43,
   -1,   45,  124,   47,   -1,   60,   61,   62,   -1,   -1,
   -1,  124,  124,   37,   38,  124,   -1,   41,   42,   43,
   -1,   45,   -1,   47,   -1,   37,   38,   -1,  124,   41,
   42,   43,   -1,   45,   33,   47,  124,   -1,   37,   38,
   -1,   -1,   41,   42,   43,   -1,   45,   -1,   47,   -1,
   -1,   -1,   -1,   -1,  124,   -1,   -1,   -1,   -1,   -1,
   33,   60,   61,   62,   37,   38,   -1,  124,  124,   42,
   43,   -1,   45,   -1,   47,   -1,   -1,   -1,   -1,   -1,
  124,   -1,   -1,   -1,   -1,  257,  258,   60,   61,   62,
   -1,   -1,   -1,  257,  258,  258,  257,  258,  257,  258,
  124,   -1,   -1,   -1,   -1,   -1,  257,  258,   -1,  257,
  258,   -1,  124,    3,    4,    5,   -1,   -1,   -1,  257,
  258,   -1,   -1,   -1,   -1,  124,   -1,   -1,   18,   19,
   20,   21,   22,   23,   24,   -1,   33,   27,   28,   -1,
   37,   38,   -1,   -1,   -1,   42,   43,   37,   45,   39,
   47,  124,   -1,   -1,   -1,   -1,   46,   47,   48,   -1,
   50,   -1,   33,   60,   61,   62,   37,   -1,   -1,   33,
   -1,   42,   43,   37,   45,   -1,   47,   -1,   42,   -1,
   -1,   -1,   -1,   47,   -1,   -1,   -1,   -1,   -1,   60,
   61,   62,   -1,   -1,   -1,   -1,   60,   61,   62,
};
#define forgecond_FINAL 7
#define forgecond_MAXTOKEN 260
#define forgecond_UNDFTOKEN 269
#define forgecond_TRANSLATE(a) ((a) > forgecond_MAXTOKEN ? forgecond_UNDFTOKEN : (a))
#if forgecond_DEBUG
static const char *const forgecond_name[] = {

"end-of-file",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
"'!'","'\"'",0,0,"'%'","'&'",0,"'('","')'","'*'","'+'",0,"'-'","'.'","'/'",0,0,
0,0,0,0,0,0,0,0,0,0,"'<'","'='","'>'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
"'|'",0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,
0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,"DIGIT","LETTER","UMINUS","UNOT",0,0,0,0,0,0,0,0,
"illegal-symbol",
};
static const char *const forgecond_rule[] = {
"$accept : cond",
"cond : expr",
"expr : '(' expr ')'",
"expr : str '=' '=' str",
"expr : str '!' '=' str",
"expr : expr '=' '=' expr",
"expr : expr '>' '=' expr",
"expr : expr '<' '=' expr",
"expr : expr '>' expr",
"expr : expr '<' expr",
"expr : expr '!' '=' expr",
"expr : expr '+' expr",
"expr : expr '-' expr",
"expr : expr '*' expr",
"expr : expr '/' expr",
"expr : expr '%' expr",
"expr : expr '&' expr",
"expr : expr '|' expr",
"expr : expr '&' '&' expr",
"expr : expr '|' '|' expr",
"expr : '-' expr",
"expr : '!' expr",
"expr : number",
"number : DIGIT",
"number : number DIGIT",
"id : LETTER",
"id : id LETTER",
"id : id DIGIT",
"var : id",
"var : id '.' id",
"str : var",
"$$1 :",
"str : '\"' $$1 id '\"'",
"str : '\"' '\"'",

};
#endif

#line 151 "../plugins/std_forge/cond_gram.y"
 /* start of programs */

/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard cschem attributes
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust Fund in 2023)
 *
 *  This program is free software licensed under the GPL, see the top of
 *  the file cond_gram.y.
 */

static int lex(forgecond_ctx_t *ctx, forgecond_STYPE *yylval, const char **inp)
{
	int c;

	if (ctx->in_quote) {
		c = **inp;
		(*inp)++;
		if (c == '\\') {
			c = **inp;
			(*inp)++;
		}
		else if (c == '\"')
			return c;
		yylval->l = c;
		return LETTER;
	}

	do {
		c = **inp;
		(*inp)++;
	} while(isspace(c)); /* skip space */

	if (c == '\0')
		return forgecond_EOF;

	/* c is now nonblank */
	if (islower(c) || (c == '_')) {
		yylval->l = c;
		return LETTER;
	}
	if (isdigit(c)) {
		yylval->l = c - '0';
		return DIGIT;
	}
	return c;
}

void forgecond_dump(FILE *f, forgecond_ctx_t *ctx)
{
	long n, len;

	fprintf(f, "prg:\n");
	for(n = 0; n < ctx->prg.used; n++) {
		char s[4];
		long l, l1, l2;

		switch(ctx->prg.array[n]) {
			case FGC_NUM:  fprintf(f, " NUM: %ld\n", ctx->prg.array[++n]); break;
			case FGC_STR:  fprintf(f, " STR: [%ld]\n", ctx->prg.array[++n]); break;
			case FGC_VAR1: fprintf(f, " VAR1: [%ld]\n", ctx->prg.array[++n]); break;
			case FGC_VAR2:
				l1 = ctx->prg.array[++n];
				l2 = ctx->prg.array[++n];
				fprintf(f, " VAR2: [%ld] [%ld]\n", l1, l2); break;
			default:
				l = ctx->prg.array[n];
				s[0] = (l >> 16) & 0x0ff;
				s[1] = (l >> 8) & 0x0ff;
				s[2] = l & 0x0ff;
				s[3] = '\0';
				fprintf(f, " %s\n", s);
				break;
		}
	}

	fprintf(f, "strings:\n");
	for(n = 0; n < ctx->strs.used; n += len) {
		len = strlen(ctx->strs.array+n)+1;
		fprintf(f, " [%ld] '%s'\n", n, ctx->strs.array+n);
	}
}

static long pop_(forgecond_ctx_t *ctx, vtl0_t *stack)
{
	if (stack->used > 0)
		return (long)stack->array[--stack->used];
	forgecond_error_cb(ctx, "forgecond: stack underflow (internal error)\n");
	return 0;
}

static const char *get_str(forgecond_ctx_t *ctx, long idx, int allow_strval)
{
	if ((allow_strval) && (idx < 0)) {
		if (idx == STR_VAL_NULL)
			return NULL;
		idx = idx - STR_VAL_BASE;
		if ((idx >= 0) && (idx < sizeof(ctx->var_val)/sizeof(ctx->var_val[0]))) {
			const char *res = ctx->var_val[idx];
			ctx->var_val[idx] = NULL;
			return res;
		}
		forgecond_error_cb(ctx, "forgecond: invalid str val address (internal error)\n");
		return NULL;
	}

	if ((idx < 0) || (idx >= ctx->strs.used)) {
		forgecond_error_cb(ctx, "forgecond: invalid string literal address (internal error)\n");
		return NULL;
	}
	return ctx->strs.array + idx;

}

static const char *pop_str_(forgecond_ctx_t *ctx, vtl0_t *stack)
{
	return get_str(ctx, pop_(ctx, stack), 1);
}

#define push(val)  vtl0_append(stack, val)
#define pop()      pop_(ctx, stack)
#define pop_str()  pop_str_(ctx, stack)

static void push_var_val(forgecond_ctx_t *ctx, vtl0_t *stack, long name_idx2, long name_idx1)
{
	int n;
	const char *name1 = (name_idx1 >= 0) ? get_str(ctx, name_idx1, 0) : NULL;
	const char *name2 = (name_idx2 >= 0) ? get_str(ctx, name_idx2, 0) : NULL;
	const char *res = forgecond_var_resolve_cb(ctx, name2, name1);

	if (res == NULL) {
		push(STR_VAL_NULL);
		return;
	}

	for(n = 0; n < sizeof(ctx->var_val)/sizeof(ctx->var_val[0]); n++) {
		if (ctx->var_val[n] == NULL) {
			ctx->var_val[n] = res;
			push(STR_VAL_BASE+n);
			return;
		}
	}

	forgecond_error_cb(ctx, "forgecond: no slot for str val (internal error)\n");
	push(STR_VAL_NULL);
	return;
}



static int safe_strcmp(const char *a, const char *b)
{
	if ((a == NULL) || (b == NULL)) return -1; /* an invalid string never equals to anyting */
	return strcmp(a, b);
}

long forgecond_exec(forgecond_ctx_t *ctx, vtl0_t *stack)
{
	long n, a, b;
	const char *sa, *sb;

	stack->used = 0;
	for(n = 0; n < ctx->prg.used; n++) {
		switch(ctx->prg.array[n]) {

			/* push immediate */
			case FGC_VAR2:
				a = ctx->prg.array[++n];
				b = ctx->prg.array[++n];
				push_var_val(ctx, stack, a, b);
				break;
				/* fall-thru to push two string indices */
			case FGC_VAR1:
				push_var_val(ctx, stack, ctx->prg.array[++n], -1);
				break;

			case FGC_NUM:
			case FGC_STR:
				push(ctx->prg.array[++n]);
				break;

			case FGC_ADD:      push(pop() + pop()); break;
			case FGC_SUB:      a = pop(); b = pop(); push(b - a); break;
			case FGC_MUL:      push(pop() * pop()); break;
			case FGC_DIV:      a = pop(); b = pop(); push(b / a); break;
			case FGC_MOD:      a = pop(); b = pop(); push(b % a); break;
			case FGC_EQ:       push(pop() == pop()); break;
			case FGC_NEQ:      push(pop() != pop()); break;
			case FGC_GT:       a = pop(); b = pop(); push(b > a); break;
			case FGC_LT:       a = pop(); b = pop(); push(b < a); break;
			case FGC_GTEQ:     a = pop(); b = pop(); push(b >= a); break;
			case FGC_LTEQ:     a = pop(); b = pop(); push(b <= a); break;
			case FGC_BIN_AND:  push(pop() & pop()); break;
			case FGC_BIN_OR:   push(pop() | pop()); break;
			case FGC_LOG_AND:  push(pop() && pop()); break;
			case FGC_LOG_OR:   push(pop() || pop()); break;
			case FGC_NEG:      push(-pop()); break;
			case FGC_NOT:      push(!pop()); break;

			case FGC_STR_EQ:   sa = pop_str(); sb = pop_str(); push(safe_strcmp(sa, sb) == 0); break;
			case FGC_STR_NEQ:  sa = pop_str(); sb = pop_str(); push(safe_strcmp(sa, sb) != 0); break;
		}
	}

	return pop();
}

int forgecond_parse_str(forgecond_ctx_t *ctx, const char *str)
{
	forgecond_res_t res;
	forgecond_yyctx_t yyctx;

	ctx->strs.used = 0;
	ctx->prg.used = 0;
	ctx->in_quote = 0;
	memset(&ctx->var_val, 0, sizeof(ctx->var_val));

	if (str == NULL)
		return -1;

	if (forgecond_parse_init(&yyctx) != 0)
		return -1;

	do {
		forgecond_STYPE lval;
		int tok = lex(ctx, &lval, &str);
		res = forgecond_parse(&yyctx, ctx, tok, &lval);
	} while(res == forgecond_RES_NEXT);

	return (res == forgecond_RES_DONE) ? 0 : -1;
}

void forgecond_error(forgecond_ctx_t *ctx, forgecond_STYPE tok, const char *s)
{
	forgecond_error_cb(ctx, s);
}

void forgecond_uninit(forgecond_ctx_t *ctx)
{
	gds_uninit(&ctx->strs);
	vtl0_uninit(&ctx->prg);
}
#line 447 "../plugins/std_forge/cond_gram.c"

#if forgecond_DEBUG
#include <stdio.h> /* needed for printf */
#endif

#include <stdlib.h> /* needed for malloc, etc */
#include <string.h> /* needed for memset */

/* allocate initial stack or double stack size, up to yyctx->stack_max_depth */
static int forgecond_growstack(forgecond_yyctx_t *yyctx, forgecond_STACKDATA *data)
{
	int i;
	unsigned newsize;
	forgecond_int_t *newss;
	forgecond_STYPE *newvs;

	if ((newsize = data->stacksize) == 0)
		newsize = forgecond_INITSTACKSIZE;
	else if (newsize >= yyctx->stack_max_depth)
		return forgecond_ENOMEM;
	else if ((newsize *= 2) > yyctx->stack_max_depth)
		newsize = yyctx->stack_max_depth;

	i = (int)(data->s_mark - data->s_base);
	newss = (forgecond_int_t *) realloc(data->s_base, newsize * sizeof(*newss));
	if (newss == 0)
		return forgecond_ENOMEM;

	data->s_base = newss;
	data->s_mark = newss + i;

	newvs = (forgecond_STYPE *) realloc(data->l_base, newsize * sizeof(*newvs));
	if (newvs == 0)
		return forgecond_ENOMEM;

	data->l_base = newvs;
	data->l_mark = newvs + i;

	data->stacksize = newsize;
	data->s_last = data->s_base + newsize - 1;
	return 0;
}

static void forgecond_freestack(forgecond_STACKDATA *data)
{
	free(data->s_base);
	free(data->l_base);
	memset(data, 0, sizeof(*data));
}

#define forgecond_ABORT  goto yyabort
#define forgecond_REJECT goto yyabort
#define forgecond_ACCEPT goto yyaccept
#define forgecond_ERROR  goto yyerrlab

int forgecond_parse_init(forgecond_yyctx_t *yyctx)
{
#if forgecond_DEBUG
	const char *yys;

	if ((yys = getenv("forgecond_DEBUG")) != 0) {
		yyctx->yyn = *yys;
		if (yyctx->yyn >= '0' && yyctx->yyn <= '9')
			yyctx->debug = yyctx->yyn - '0';
	}
#endif

	memset(&yyctx->val, 0, sizeof(yyctx->val));
	memset(&yyctx->lval, 0, sizeof(yyctx->lval));

	yyctx->yym = 0;
	yyctx->yyn = 0;
	yyctx->nerrs = 0;
	yyctx->errflag = 0;
	yyctx->chr = forgecond_EMPTY;
	yyctx->state = 0;

	memset(&yyctx->stack, 0, sizeof(yyctx->stack));

	yyctx->stack_max_depth = forgecond_INITSTACKSIZE > 10000 ? forgecond_INITSTACKSIZE : 10000;
	if (yyctx->stack.s_base == NULL && forgecond_growstack(yyctx, &yyctx->stack) == forgecond_ENOMEM)
		return -1;
	yyctx->stack.s_mark = yyctx->stack.s_base;
	yyctx->stack.l_mark = yyctx->stack.l_base;
	yyctx->state = 0;
	*yyctx->stack.s_mark = 0;
	yyctx->jump = 0;
	return 0;
}


#define forgecond_GETCHAR(labidx) \
do { \
	if (used)               { yyctx->jump = labidx; return forgecond_RES_NEXT; } \
	getchar_ ## labidx:;    yyctx->chr = tok; yyctx->lval = *lval; used = 1; \
} while(0)

forgecond_res_t forgecond_parse(forgecond_yyctx_t *yyctx, forgecond_ctx_t *ctx, int tok, forgecond_STYPE *lval)
{
	int used = 0;
#if forgecond_DEBUG
	const char *yys;
#endif

yyloop:;
	if (yyctx->jump == 1) { yyctx->jump = 0; goto getchar_1; }
	if (yyctx->jump == 2) { yyctx->jump = 0; goto getchar_2; }

	if ((yyctx->yyn = forgecond_defred[yyctx->state]) != 0)
		goto yyreduce;
	if (yyctx->chr < 0) {
		forgecond_GETCHAR(1);
		if (yyctx->chr < 0)
			yyctx->chr = forgecond_EOF;
#if forgecond_DEBUG
		if (yyctx->debug) {
			if ((yys = forgecond_name[forgecond_TRANSLATE(yyctx->chr)]) == NULL)
				yys = forgecond_name[forgecond_UNDFTOKEN];
			printf("yyctx->debug: state %d, reading %d (%s)\n", yyctx->state, yyctx->chr, yys);
		}
#endif
	}
	if (((yyctx->yyn = forgecond_sindex[yyctx->state]) != 0) && (yyctx->yyn += yyctx->chr) >= 0 && yyctx->yyn <= forgecond_TABLESIZE && forgecond_check[yyctx->yyn] == (forgecond_int_t) yyctx->chr) {
#if forgecond_DEBUG
		if (yyctx->debug)
			printf("yyctx->debug: state %d, shifting to state %d\n", yyctx->state, forgecond_table[yyctx->yyn]);
#endif
		if (yyctx->stack.s_mark >= yyctx->stack.s_last && forgecond_growstack(yyctx, &yyctx->stack) == forgecond_ENOMEM)
			goto yyoverflow;
		yyctx->state = forgecond_table[yyctx->yyn];
		*++yyctx->stack.s_mark = forgecond_table[yyctx->yyn];
		*++yyctx->stack.l_mark = yyctx->lval;
		yyctx->chr = forgecond_EMPTY;
		if (yyctx->errflag > 0)
			--yyctx->errflag;
		goto yyloop;
	}
	if (((yyctx->yyn = forgecond_rindex[yyctx->state]) != 0) && (yyctx->yyn += yyctx->chr) >= 0 && yyctx->yyn <= forgecond_TABLESIZE && forgecond_check[yyctx->yyn] == (forgecond_int_t) yyctx->chr) {
		yyctx->yyn = forgecond_table[yyctx->yyn];
		goto yyreduce;
	}
	if (yyctx->errflag != 0)
		goto yyinrecovery;

	forgecond_error(ctx, yyctx->lval, "syntax error");

	goto yyerrlab;	/* redundant goto avoids 'unused label' warning */
yyerrlab:
	++yyctx->nerrs;

yyinrecovery:
	if (yyctx->errflag < 3) {
		yyctx->errflag = 3;
		for(;;) {
			if (((yyctx->yyn = forgecond_sindex[*yyctx->stack.s_mark]) != 0) && (yyctx->yyn += forgecond_ERRCODE) >= 0 && yyctx->yyn <= forgecond_TABLESIZE && forgecond_check[yyctx->yyn] == (forgecond_int_t) forgecond_ERRCODE) {
#if forgecond_DEBUG
				if (yyctx->debug)
					printf("yyctx->debug: state %d, error recovery shifting to state %d\n", *yyctx->stack.s_mark, forgecond_table[yyctx->yyn]);
#endif
				if (yyctx->stack.s_mark >= yyctx->stack.s_last && forgecond_growstack(yyctx, &yyctx->stack) == forgecond_ENOMEM)
					goto yyoverflow;
				yyctx->state = forgecond_table[yyctx->yyn];
				*++yyctx->stack.s_mark = forgecond_table[yyctx->yyn];
				*++yyctx->stack.l_mark = yyctx->lval;
				goto yyloop;
			}
			else {
#if forgecond_DEBUG
				if (yyctx->debug)
					printf("yyctx->debug: error recovery discarding state %d\n", *yyctx->stack.s_mark);
#endif
				if (yyctx->stack.s_mark <= yyctx->stack.s_base)
					goto yyabort;
				--yyctx->stack.s_mark;
				--yyctx->stack.l_mark;
			}
		}
	}
	else {
		if (yyctx->chr == forgecond_EOF)
			goto yyabort;
#if forgecond_DEBUG
		if (yyctx->debug) {
			if ((yys = forgecond_name[forgecond_TRANSLATE(yyctx->chr)]) == NULL)
				yys = forgecond_name[forgecond_UNDFTOKEN];
			printf("yyctx->debug: state %d, error recovery discards token %d (%s)\n", yyctx->state, yyctx->chr, yys);
		}
#endif
		yyctx->chr = forgecond_EMPTY;
		goto yyloop;
	}

yyreduce:
#if forgecond_DEBUG
	if (yyctx->debug)
		printf("yyctx->debug: state %d, reducing by rule %d (%s)\n", yyctx->state, yyctx->yyn, forgecond_rule[yyctx->yyn]);
#endif
	yyctx->yym = forgecond_len[yyctx->yyn];
	if (yyctx->yym > 0)
		yyctx->val = yyctx->stack.l_mark[1 - yyctx->yym];
	else
		memset(&yyctx->val, 0, sizeof yyctx->val);

	switch (yyctx->yyn) {
case 2:
#line 105 "../plugins/std_forge/cond_gram.y"
	{ /* nothing to generate */ }
break;
case 3:
#line 106 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_STR_EQ); }
break;
case 4:
#line 107 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_STR_NEQ); }
break;
case 5:
#line 108 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_EQ); }
break;
case 6:
#line 109 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_GTEQ); }
break;
case 7:
#line 110 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_LTEQ); }
break;
case 8:
#line 111 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_GT); }
break;
case 9:
#line 112 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_LT); }
break;
case 10:
#line 113 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_NEQ); }
break;
case 11:
#line 114 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_ADD); }
break;
case 12:
#line 115 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_SUB); }
break;
case 13:
#line 116 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_MUL); }
break;
case 14:
#line 117 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_DIV); }
break;
case 15:
#line 118 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_MOD); }
break;
case 16:
#line 119 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_BIN_AND); }
break;
case 17:
#line 120 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_BIN_OR); }
break;
case 18:
#line 121 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_LOG_AND); }
break;
case 19:
#line 122 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_LOG_OR); }
break;
case 20:
#line 123 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_NEG); }
break;
case 21:
#line 124 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_NOT); }
break;
case 22:
#line 125 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_NUM); append(ctx, yyctx->stack.l_mark[0].l); }
break;
case 23:
#line 129 "../plugins/std_forge/cond_gram.y"
	{  yyctx->val.l = yyctx->stack.l_mark[0].l; }
break;
case 24:
#line 130 "../plugins/std_forge/cond_gram.y"
	{  yyctx->val.l = 10 * yyctx->stack.l_mark[-1].l + yyctx->stack.l_mark[0].l; }
break;
case 25:
#line 134 "../plugins/std_forge/cond_gram.y"
	{  if (ctx->strs.used != 0) gds_append(&ctx->strs, '\0'); yyctx->val.l = ctx->strs.used; gds_append(&ctx->strs, yyctx->stack.l_mark[0].l); }
break;
case 26:
#line 135 "../plugins/std_forge/cond_gram.y"
	{  yyctx->val.l = yyctx->stack.l_mark[-1].l; gds_append(&ctx->strs, yyctx->stack.l_mark[0].l); }
break;
case 27:
#line 136 "../plugins/std_forge/cond_gram.y"
	{  yyctx->val.l = yyctx->stack.l_mark[-1].l; gds_append(&ctx->strs, yyctx->stack.l_mark[0].l + '0'); }
break;
case 28:
#line 140 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_VAR1); append(ctx, yyctx->stack.l_mark[0].l); }
break;
case 29:
#line 141 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_VAR2); append(ctx, yyctx->stack.l_mark[-2].l); append(ctx, yyctx->stack.l_mark[0].l); }
break;
case 30:
#line 145 "../plugins/std_forge/cond_gram.y"
	{ yyctx->val.l = yyctx->stack.l_mark[0].l; }
break;
case 31:
#line 146 "../plugins/std_forge/cond_gram.y"
	{ ctx->in_quote = 1; }
break;
case 32:
#line 147 "../plugins/std_forge/cond_gram.y"
	{ ctx->in_quote = 0; append(ctx, FGC_STR); append(ctx, yyctx->stack.l_mark[-1].l); }
break;
case 33:
#line 148 "../plugins/std_forge/cond_gram.y"
	{ append(ctx, FGC_STR); append(ctx, ctx->strs.used); gds_append(&ctx->strs, '\0'); }
break;
#line 780 "../plugins/std_forge/cond_gram.c"
	}
	yyctx->stack.s_mark -= yyctx->yym;
	yyctx->state = *yyctx->stack.s_mark;
	yyctx->stack.l_mark -= yyctx->yym;
	yyctx->yym = forgecond_lhs[yyctx->yyn];
	if (yyctx->state == 0 && yyctx->yym == 0) {
#if forgecond_DEBUG
		if (yyctx->debug)
			printf("yyctx->debug: after reduction, shifting from state 0 to state %d\n", forgecond_FINAL);
#endif
		yyctx->state = forgecond_FINAL;
		*++yyctx->stack.s_mark = forgecond_FINAL;
		*++yyctx->stack.l_mark = yyctx->val;
		if (yyctx->chr < 0) {
			forgecond_GETCHAR(2);
			if (yyctx->chr < 0)
				yyctx->chr = forgecond_EOF;
#if forgecond_DEBUG
			if (yyctx->debug) {
				if ((yys = forgecond_name[forgecond_TRANSLATE(yyctx->chr)]) == NULL)
					yys = forgecond_name[forgecond_UNDFTOKEN];
				printf("yyctx->debug: state %d, reading %d (%s)\n", forgecond_FINAL, yyctx->chr, yys);
			}
#endif
		}
		if (yyctx->chr == forgecond_EOF)
			goto yyaccept;
		goto yyloop;
	}
	if (((yyctx->yyn = forgecond_gindex[yyctx->yym]) != 0) && (yyctx->yyn += yyctx->state) >= 0 && yyctx->yyn <= forgecond_TABLESIZE && forgecond_check[yyctx->yyn] == (forgecond_int_t) yyctx->state)
		yyctx->state = forgecond_table[yyctx->yyn];
	else
		yyctx->state = forgecond_dgoto[yyctx->yym];
#if forgecond_DEBUG
	if (yyctx->debug)
		printf("yyctx->debug: after reduction, shifting from state %d to state %d\n", *yyctx->stack.s_mark, yyctx->state);
#endif
	if (yyctx->stack.s_mark >= yyctx->stack.s_last && forgecond_growstack(yyctx, &yyctx->stack) == forgecond_ENOMEM)
		goto yyoverflow;
	*++yyctx->stack.s_mark = (forgecond_int_t) yyctx->state;
	*++yyctx->stack.l_mark = yyctx->val;
	goto yyloop;

yyoverflow:
	forgecond_error(ctx, yyctx->lval, "yacc stack overflow");

yyabort:
	forgecond_freestack(&yyctx->stack);
	return forgecond_RES_ABORT;

yyaccept:
	forgecond_freestack(&yyctx->stack);
	return forgecond_RES_DONE;
}
