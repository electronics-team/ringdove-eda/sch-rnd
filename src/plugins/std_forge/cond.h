#ifndef STD_FORGE_COND_H
#define STD_FORGE_COND_H
#include <genvector/gds_char.h>
#include <genvector/vtl0.h>
#include <libcschem/abstract.h>

typedef struct {
	gds_t strs;
	vtl0_t prg;
	unsigned in_quote:1;
	unsigned error:1;
	const char *var_val[2];
	csch_abstract_t *abst;
	csch_project_t *prj;
} forgecond_ctx_t;

int forgecond_parse_str(forgecond_ctx_t *ctx, const char *str);
void forgecond_dump(FILE *f, forgecond_ctx_t *ctx);
long forgecond_exec(forgecond_ctx_t *ctx, vtl0_t *stack);
void forgecond_uninit(forgecond_ctx_t *ctx);


/* Provided by the caller: */
extern const char *forgecond_var_resolve_cb(forgecond_ctx_t *ctx, const char *name1, const char *name2);
extern void forgecond_error_cb(forgecond_ctx_t *ctx, const char *s);

#endif
