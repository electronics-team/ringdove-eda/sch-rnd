#ifndef _forgecond__defines_h_
#define _forgecond__defines_h_

typedef short forgecond_int_t;
#define forgecond_chr yyctx->chr
#define forgecond_val yyctx->val
#define forgecond_lval yyctx->lval
#define forgecond_stack yyctx->stack
#define forgecond_debug yyctx->debug
#define forgecond_nerrs yyctx->nerrs
#define forgecond_errflag yyctx->errflag
#define forgecond_state yyctx->state
#define forgecond_yyn yyctx->yyn
#define forgecond_yym yyctx->yym
#define forgecond_jump yyctx->jump
#line 4 "../plugins/std_forge/cond_gram.y"

/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - standard cschem attributes
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust Fund in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <stdio.h>
#include <ctype.h>
#include "cond.h"

#define STR_VAL_NULL -1000
#define STR_VAL_BASE -100

#define INST(a, b, c)   ((((long)a) << 16) | (((long)b) << 8) | ((long)c))

typedef enum {
	FGC_ABORT    = 0,
	FGC_ADD      = INST('A', 'D', 'D'),
	FGC_SUB      = INST('S', 'U', 'B'),
	FGC_MUL      = INST('M', 'U', 'L'),
	FGC_DIV      = INST('D', 'I', 'V'),
	FGC_MOD      = INST('M', 'O', 'D'),
	FGC_STR_EQ   = INST('S', 'E', 'Q'),
	FGC_STR_NEQ  = INST('S', 'N', 'Q'),
	FGC_EQ       = INST('E', 'Q', 0),
	FGC_NEQ      = INST('N', 'E', 'Q'),
	FGC_GT       = INST('G', 'T', 0),
	FGC_LT       = INST('L', 'T', 0),
	FGC_GTEQ     = INST('G', 'T', 'E'),
	FGC_LTEQ     = INST('L', 'T', 'E'),
	FGC_BIN_AND  = INST('&', 0, 0),
	FGC_BIN_OR   = INST('|', 0, 0),
	FGC_LOG_AND  = INST('&', '&', 0),
	FGC_LOG_OR   = INST('|', '|', 0),
	FGC_NEG      = INST('N', 'E', 'G'),
	FGC_NOT      = INST('N', 'O', 'T'),
	FGC_VAR1     = INST('V', 'R', '1'),
	FGC_VAR2     = INST('V', 'R', '2'),
	FGC_NUM      = INST('N', 'U', 'M'),
	FGC_STR      = INST('S', 'T', 'R')   /* string literal */
} forgecond_inst_t;

static void append(forgecond_ctx_t *ctx, long cod) { vtl0_append(&ctx->prg, cod); }

#line 85 "../plugins/std_forge/cond_gram.y"
typedef union forgecond_tokunion_u
{
	long l;
} forgecond_tokunion_t;
typedef forgecond_tokunion_t forgecond_STYPE;


#define DIGIT 257
#define LETTER 258
#define UMINUS 259
#define UNOT 260
#define forgecond_ERRCODE 256

#ifndef forgecond_INITSTACKSIZE
#define forgecond_INITSTACKSIZE 200
#endif

typedef struct {
	unsigned stacksize;
	forgecond_int_t *s_base;
	forgecond_int_t *s_mark;
	forgecond_int_t *s_last;
	forgecond_STYPE *l_base;
	forgecond_STYPE *l_mark;
#if forgecond_DEBUG
	int debug;
#endif
} forgecond_STACKDATA;

typedef struct {
	int errflag;
	int chr;
	forgecond_STYPE val;
	forgecond_STYPE lval;
	int nerrs;
	int yym, yyn, state;
	int jump;
	int stack_max_depth;
	int debug;

	/* variables for the parser stack */
	forgecond_STACKDATA stack;
} forgecond_yyctx_t;

typedef enum { forgecond_RES_NEXT, forgecond_RES_DONE, forgecond_RES_ABORT } forgecond_res_t;

extern int forgecond_parse_init(forgecond_yyctx_t *yyctx);
extern forgecond_res_t forgecond_parse(forgecond_yyctx_t *yyctx, forgecond_ctx_t *ctx, int tok, forgecond_STYPE *lval);
extern void forgecond_error(forgecond_ctx_t *ctx, forgecond_STYPE tok, const char *msg);


#endif /* _forgecond__defines_h_ */
