#include <libcschem/project.h>

static void sim_setup_dlg(csch_project_t *prj, const char *name);

static void sim_setup_dlg_uninit(void);
static void sim_setup_dlg_init(void);

void sim_setup_dlg_setup_removed(const char *setup_name);

