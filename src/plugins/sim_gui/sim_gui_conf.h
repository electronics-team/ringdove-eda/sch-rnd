#ifndef SCH_RND_SIM_GUI_CONF_H
#define SCH_RND_SIM_GUI_CONF_H

#include <librnd/core/conf.h>
#include <libcschem/project.h>

typedef struct {
	const struct {
		const struct {
			RND_CFT_INTEGER plot_height_px; /* plot widget height in pixels */
			RND_CFT_COLOR plot_grid_color;  /* color of the background grid drawn parallel to x and y axis to mark notable values */
		} sim_gui;
	} plugins;
} conf_sim_gui_t;

extern conf_sim_gui_t sch_sim_gui_conf;

#endif
