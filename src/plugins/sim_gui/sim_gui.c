/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - high level sim, GUI
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <stdio.h>
#include <stdlib.h>
#include <libfungw/fungw.h>
#include <libcschem/config.h>
#include <librnd/core/actions.h>
#include <librnd/core/compat_misc.h>
#include <librnd/core/rnd_printf.h>
#include <librnd/core/plugins.h>
#include <librnd/core/misc_util.h>

#include <libcschem/event.h>

static const char sim_gui_cookie[] = "sim_gui";

#include "sim_setup_dlg.h"

#include "plot_test.c"
#include "sim_dlg.c"
#include "sim_setup_dlg.c"

#include "sim_gui_conf.h"

conf_sim_gui_t sch_sim_gui_conf;

#include "conf_internal.c"

static rnd_action_t sim_gui_action_list[] = {
	{"SimDlg", csch_act_SimDlg, csch_acth_SimDlg, csch_acts_SimDlg},
	{"SimDialog", csch_act_SimDlg, csch_acth_SimDlg, csch_acts_SimDlg},
	{"SimSetupDlg", csch_act_SimSetupDlg, csch_acth_SimSetupDlg, csch_acts_SimSetupDlg},
	{"SimSetupDialog", csch_act_SimSetupDlg, csch_acth_SimSetupDlg, csch_acts_SimSetupDlg},
	{"PlotTest", csch_act_PlotTest, csch_acth_PlotTest, csch_acts_PlotTest}
};

static void sim_gui_sheet_pre_unload(rnd_design_t *dsg, void *user_data, int argc, rnd_event_arg_t argv[])
{
	if (dsg->project->designs.used == 1) {
		/* last sheet being unloaded, the project becomes empty */
		sim_setup_dlg_prj_unload(dsg->project);
		sim_dlg_prj_unload(dsg->project);
	}
}

int pplg_check_ver_sim_gui(int ver_needed) { return 0; }

void pplg_uninit_sim_gui(void)
{
	rnd_remove_actions_by_cookie(sim_gui_cookie);
	sim_dlg_uninit();
	sim_setup_dlg_uninit();
	rnd_event_unbind_allcookie(sim_gui_cookie);
	rnd_conf_plug_unreg("plugins/sim_gui/", sim_gui_conf_internal, sim_gui_cookie);
}

int pplg_init_sim_gui(void)
{
	RND_API_CHK_VER;

	rnd_conf_plug_reg(sch_sim_gui_conf, sim_gui_conf_internal, sim_gui_cookie);
#define conf_reg(field,isarray,type_name,cpath,cname,desc,flags) \
	rnd_conf_reg_field(sch_sim_gui_conf, field,isarray,type_name,cpath,cname,desc,flags);
#include "sim_gui_conf_fields.h"

	RND_REGISTER_ACTIONS(sim_gui_action_list, sim_gui_cookie);

	sim_dlg_init();
	sim_setup_dlg_init();

	rnd_event_bind(CSCH_EVENT_SHEET_PREUNLOAD, sim_gui_sheet_pre_unload, NULL, sim_gui_cookie);

	return 0;
}

