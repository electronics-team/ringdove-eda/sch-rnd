void sch_rnd_xor_draw_obj(csch_sheet_t *sheet, csch_chdr_t *obj, csch_coord_t dx, csch_coord_t dy, rnd_hid_gc_t gc, int thin, int wireframe);

void sch_rnd_xor_draw_buffer(csch_sheet_t *sheet, csch_sheet_t *buffer, csch_coord_t dx, csch_coord_t dy, rnd_hid_gc_t gc, int thin, int wireframe);
