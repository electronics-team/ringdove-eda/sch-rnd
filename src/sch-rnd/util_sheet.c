/*
 *                            COPYRIGHT
 *
 *  sch-rnd - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "config.h"

#include <stdlib.h>
#include <librnd/core/conf.h>
#include <libcschem/libcschem.h>
#include <libcschem/cnc_pen.h>
#include <libcschem/project.h>
#include "sheet.h"

#include "util_sheet.h"

void sch_rnd_sheet_setup(csch_sheet_t *dst, sch_rnd_sheet_setup_cfg_t cfg, int (*chk_copy_pen)(void *udata, csch_sheet_t *dst, csch_cpen_t *p), void *udata)
{
	if (cfg & SCH_RND_SSC_PENS) { /* create temp new sheet only if we are to do anything */
		csch_sheet_t *tmp = sch_rnd_sheet_new((csch_project_t *)dst->hidlib.project);
		htip_entry_t *e;
		for(e = htip_first(&tmp->direct.id2obj); e != NULL; e = htip_next(&tmp->direct.id2obj, e)) {
			csch_cpen_t *p = e->value, *pnew;
			if ((p->hdr.type == CSCH_CTYPE_PEN) && (csch_pen_get(dst, &dst->direct, p->name.str) == NULL)) {
				if ((chk_copy_pen == NULL) || chk_copy_pen(udata, dst, p)) {
					pnew = csch_pen_dup(dst, &dst->direct, p);
					if (cfg & SCH_RND_SSC_PEN_MARK_DEFAULT)
						pnew->copied_from_default = 1;
				}
			}
		}
		csch_project_remove_sheet((csch_project_t *)dst->hidlib.project, tmp);
	}

	if (cfg & SCH_RND_SSC_UUID) {
		minuid_gen(&csch_minuid, dst->direct.uuid);
		minuid_clr(dst->direct.data.grp.src_uuid);
	}
}

void sch_rnd_reconf_for_symbol_as_sheet(void)
{
	rnd_conf_set(RND_CFR_DESIGN, "editor/style/tool_circle_stroke", 0, "sym-decor", RND_POL_OVERWRITE);
	rnd_conf_set(RND_CFR_DESIGN, "editor/style/tool_line_stroke", 0, "sym-decor", RND_POL_OVERWRITE);
	rnd_conf_set(RND_CFR_DESIGN, "editor/style/tool_text_stroke", 0, "sym-decor", RND_POL_OVERWRITE);
	rnd_conf_set(RND_CFR_DESIGN, "editor/style/tool_rect_stroke", 0, "sym-decor", RND_POL_OVERWRITE);
	rnd_conf_set(RND_CFR_DESIGN, "editor/style/tool_rect_fill", 0, "sym-decor-fill", RND_POL_OVERWRITE);
}
