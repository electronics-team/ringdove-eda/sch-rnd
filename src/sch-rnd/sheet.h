csch_sheet_t *sch_rnd_sheet_new(csch_project_t *prj); /* doesn't emit postproc event */
csch_sheet_t *sch_rnd_sheet_new4revert(csch_sheet_t *sheet);
void sch_rnd_sheet_postproc(csch_sheet_t *sheet);

rnd_coord_t sch_rnd_sheet_attr_crd(csch_sheet_t *sheet, const char *key, rnd_coord_t defval);

/* Throw an error when sheet gets too large (only once per sheet) */
void sch_rnd_sheet_warn_size(csch_sheet_t *sheet, csch_coord_t x1, csch_coord_t y1, csch_coord_t x2, csch_coord_t y2);

/* Recalc from scratch - time consuming but accurate */
void sch_rnd_sheet_recalc_bbox(csch_sheet_t *sheet);
