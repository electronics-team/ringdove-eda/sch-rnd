#ifndef CSCH_SELECT_H
#define CSCH_SELECT_H

#include <libcschem/config.h>
#include <libcschem/concrete.h>

/* Select object clicked or select parent object (if already selected) or
   unselect (if clicked away) */
void sch_rnd_select_click(csch_sheet_t *sheet, csch_coord_t x, csch_coord_t y);

void sch_rnd_select_box(csch_sheet_t *sheet, csch_coord_t x1, csch_coord_t y1, csch_coord_t x2, csch_coord_t y2);


void sch_rnd_unselect_all(csch_sheet_t *sheet);

void sch_rnd_select_invert(csch_sheet_t *sheet);

/* Compute bbox of selected objects on sheet; returns number of selected
   objects considered (0 means no selection, negative means error). Fills
   dst in with screen (rnd_coord_t) coordinates */
long sch_rnd_get_selection_bbox_gui(rnd_box_t *dst, csch_sheet_t *sheet);

#endif
