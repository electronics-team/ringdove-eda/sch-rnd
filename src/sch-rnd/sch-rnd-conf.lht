li:sch-rnd-conf-v1 {
	ha:overwrite {
		ha:rc {
			li:preferred_gui = { gtk2_gl; gtk2_gdk; gtk4_gl; lesstif; batch }
			hid_fallback = 1
			menu_file = {default}
			li:library_search_paths = {
				?../../library/symbol
				?$(rc.path.design)/symbol
				?~/.sch-rnd/symbol/
				$(rc.path.share)/symbol
			}
			li:hlibrary_search_paths = {
				?../../library/hlibrary
				?$(rc.path.design)/hlibrary
				?~/.sch-rnd/hlibrary/
				$(rc.path.share)/hlibrary
			}
			li:default_sheet_file = {
				{./default-sheet.lht}
				{$(rc.path.share)/default-sheet.lht}
			}
			li:font_dirs = {
				{../../font}
				{$(rc.path.share)/font}
			}
			emergency_name = {SCH.%ld-%ld.save}
			backup_interval = 60
			backup_name = {%F.%P.backup}
			file_changed_interval = 2
			ha:debug {
				draw_text_xform = 0
				draw_arc_bbox = 0
			}
		}
		ha:editor {
			click_time = 200
			edit_time = 800
			autocomp = 1
			autocomp_time = 1500
			draw_grid = 1
			buffer_number = 0
			lock_floaters = 0
			only_floaters = 0
			grid_unit = mm
			# default "pin grid" as per sch-rnd convention
			grid = 4 k
			li:grids = {
				# our grid coords are: k is the cschem 1000 unit (needs a *1.024 for librnd screen conv)
				{1 k}; {2 k}; {4 k}; {8 k}; {16 k};
			}
			grids_idx = 2

			line_refraction=0
			line_cont=1
			paste_to_local_lib=0
			rubber_band_mode=0
			rubber_band_ortho=0
			ha:style {
				li:tool_circle_stroke          { sheet-decor; decor;}
				li:tool_line_stroke            { sheet-decor; decor;}
				li:tool_text_stroke            { sheet-decor; decor;}
				li:tool_wire_stroke            { wire; }

				li:tool_rect_stroke            { sheet-decor; decor;}
				tool_rect_has_stroke           {true}
				li:tool_rect_fill              { sheet-decor-fill; }
				tool_rect_has_fill             {true}
			}
			ha:selection {
				disable_negative = 0
				symmetric_negative = 0
			}
			ha:default_font {
				family=sans
				style=oblique
			}
			ha:local_grid {
				enable = 0
				radius = 16
			}
			ha:global_grid {
				sparse = 0
				min_dist_px = 4
			}
		}
		ha:appearance {
			layer_alpha = 1
			ha:loglevels {
				debug_tag      = {}
				debug_popup    = false
				info_tag       = {}
				info_popup     = false
				warning_tag    = {<B>}
				warning_popup  = true
				error_tag      = {<R>}
				error_popup    = true
			}
			ha:color {
				background = {#e5e5e5}
				cross = {#00cdcd}
				off_limit = {#aaaaaa}
				grid = {#ff0000}
				attached = {#ff0000}
				hilight_stroke = {#FF0000}
				selected_stroke = {#00cccc}
				selected_fill = {#99ffff}
				connection_good = {#00ff00}
				connection_bad = {#ff0000}
				symbol_meta_embed = {#ff0000}
				symbol_meta_loclib = {#990000}
				non_graphical_background = {#000000}
				non_graphical_text = {#00AA00}
			}
		}

		ha:compile {
			multiport_net_merge = 0
			ha:hierarchic {
				net_allow_rename = 1
				net_rename_prefer_top = 1
			}
			li:views {
				ha:pcb {
					li:engines {
						ha:std_forge  { plugin=std_forge }
						ha:std_cschem { plugin=std_cschem }
						ha:std_devmap { plugin=std_devmap }
						ha:funcmap    { plugin=funcmap }
						ha:target_pcb { plugin=target_pcb }
					}
				}
				ha:sim_ngspice {
					li:engines {
						ha:std_forge  { plugin=std_forge }
						ha:std_cschem { plugin=std_cschem }
						ha:std_devmap { plugin=std_devmap }
						ha:funcmap    { plugin=funcmap }
						ha:target_sim_ngspice { plugin=target_sim_ngspice }
					}
				}
				ha:spice_raw {
					li:engines {
						ha:std_forge  { plugin=std_forge }
						ha:std_cschem { plugin=std_cschem }
						ha:std_devmap { plugin=std_devmap }
						ha:funcmap    { plugin=funcmap }
						ha:target_spice { plugin=target_spice }
					}
				}
				ha:raw {
					li:engines {
						ha:std_forge   { plugin=std_forge }
						ha:std_cschem  { plugin=std_cschem }
						ha:target_none { plugin=target_none }
					}
				}
			}
		}

		ha:plugins {
			ha:lib_hid_common {
				ha:cli_history {
					file = {$(rc.path.home)/.sch-rnd/cli_history}
					slots = 64
				}
			}
			ha:hid_gtk {
				ha:dialog {
					transient_modal = 1
					transient_modeless = 1
					auto_present = 0
				}
			}
		} # plugins

	}
}

