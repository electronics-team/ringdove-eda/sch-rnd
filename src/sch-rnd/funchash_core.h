/* central, auto-generated enum of core function IDs */


#ifndef SCH_RND_FUNCHASH_H
#define SCH_RND_FUNCHASH_H

#include <librnd/core/funchash.h>

#define action_entry(x) F_ ## x,
typedef enum {
#include "funchash_core_list.h"
F_END
} pcb_function_id_t;
#undef action_entry

#endif
