#ifndef CSCH_UTIL_PROJECT_H
#define CSCH_UTIL_PROJECT_H

#include <librnd/core/conf.h>
#include <libcschem/concrete.h>

/* Look at sheet's project and fill in sheet->type accordingly
   (root, aux or unlisted) */
void csch_sheet_type_detect(csch_sheet_t *sheet, const rnd_conflist_t *roots, const rnd_conflist_t *aux);

/* Return 1 if not all root sheets are loaded, 0 otherwise */
int csch_project_is_partial(csch_project_t *prj);


#endif
