/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */


#include "config.h"

#include "event.h"
#include "concrete.h"
#include "cnc_obj.h"
#include "cnc_bitmap.h"
#include "op_common.h"

csch_cbitmap_t *csch_cbitmap_alloc(csch_sheet_t *sheet, csch_cgrp_t *parent, csch_oid_t oid)
{
	csch_cbitmap_t *bitmap;

	bitmap = htip_get(&parent->id2obj, oid);
	if (bitmap != NULL)
		return NULL;
	bitmap = calloc(sizeof(csch_cbitmap_t), 1);

	csch_cobj_init(&bitmap->hdr, sheet, parent, oid, CSCH_CTYPE_BITMAP);
	return bitmap;
}

csch_cbitmap_t *csch_cbitmap_dup(csch_sheet_t *sheet, csch_cgrp_t *parent, const csch_cbitmap_t *src, int keep_id)
{
	csch_cbitmap_t *dst = csch_cbitmap_alloc(sheet, parent, CSCH_KEEP_OID(parent, src->hdr.oid));
	dst->inst_x1 = src->inst_x1;
	dst->inst_y1 = src->inst_y1;
	dst->spec_x1 = src->spec_x1;
	dst->spec_y1 = src->spec_y1;
	dst->sx = src->sx;
	dst->sy = src->sy;
	dst->transp = src->transp;
	TODO("bitmap: copy or reference the pixmap?");
	return dst;
}


void csch_cbitmap_free(csch_cbitmap_t *bitmap)
{
	csch_cobj_uninit(&bitmap->hdr);
	free(bitmap);
}

csch_cbitmap_t *csch_cbitmap_get(csch_sheet_t *sheet, csch_cgrp_t *grp, csch_oid_t oid)
{
	csch_cbitmap_t *bitmap = htip_get(&grp->id2obj, oid);
	if ((bitmap != NULL) && (bitmap->hdr.type != CSCH_CTYPE_BITMAP))
		return NULL;
	return bitmap;
}

