#ifndef CSCH_RTREE_H
#define CSCH_RTREE_H

#include "libcschem/common_types.h"

typedef long csch_rtree_cardinal_t;
typedef csch_coord_t csch_rtree_coord_t;

#define RTR(n)  csch_rtree_ ## n
#define RTRU(n) csch_RTREE_ ## n
#define csch_rtree_privfunc static
#define csch_rtree_size 6
#define csch_rtree_stack_max 1024

#include <genrtree/genrtree_api.h>

#define csch_bbox_invalidate(bbox) (bbox)->x1 = CSCH_COORD_INV
#define csch_bbox_is_invalid(bbox) ((bbox)->x1 == CSCH_COORD_INV)

#define csch_bbox_reset(bbox) \
do { \
	(bbox)->x1 = (bbox)->y1 = CSCH_COORD_MAX; \
	(bbox)->x2 = (bbox)->y2 = CSCH_COORD_MIN; \
} while(0)

#define csch_bbox_bump(bbox, coord, value) \
do { \
	if (value < (bbox)->coord ## 1) { (bbox)->coord ## 1 = value; } \
	if (value > (bbox)->coord ## 2) { (bbox)->coord ## 2 = value; } \
} while(0)

#endif /* CSCH_RTREE2_H */
