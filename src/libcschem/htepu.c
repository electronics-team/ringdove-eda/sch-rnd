#include "htepu.h"
#define HT(x) htepu_ ## x
static htepu_value_t invalid;
#define HT_INVALID_VALUE invalid
#include <genht/ht.c>
#include <genht/hash.h>

int htepu_keyeq(htepu_key_t a, htepu_key_t b)
{
	return (a.obj == b.obj) && (a.epi == b.epi) && (a.x == b.x) && (a.y == b.y);
}

unsigned int htepu_keyhash(htepu_key_t a)
{
	return ptrhash(a.obj) ^ longhash(a.epi) ^ longhash(a.x) ^ longhash(a.y);
}

int htepu_keyeq_xy(htepu_key_t a, htepu_key_t b)
{
	return (a.x == b.x) && (a.y == b.y);
}

unsigned int htepu_keyhash_xy(htepu_key_t a)
{
	return longhash(a.x) ^ longhash(a.y);
}

#undef HT
