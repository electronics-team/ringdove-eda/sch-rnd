#ifndef GENHT_HTEPU_H
#define GENHT_HTEPU_H

#include "config.h"

#include "concrete.h"

/* Endpoint to user data hash */

typedef enum csch_endpoint_idx_e { /* bitfield */
	CSCH_EPI_invalid = 0,
	CSCH_EPI_START = 1,
	CSCH_EPI_END = 2,
	CSCH_EPI_MID = 4,
	CSCH_EPI_ALL = 0xff /* both START and END and everything in between (e.g. 0 length obj) */
} csch_endpoint_idx_t;

typedef struct {
	csch_chdr_t *obj;
	csch_coord_t x, y;
	csch_endpoint_idx_t epi;
} htepu_key_t;

typedef struct htepu_value_s {
	csch_coord_t newx, newy;
	union {
		csch_coord_t crd;
		long l;
		double d;
		void *p;
	} data[4];
} htepu_value_t;


#define HT(x) htepu_ ## x
#include <genht/ht.h>
#undef HT

int htepu_keyeq(htepu_key_t a, htepu_key_t b);
int htepu_keyeq_xy(htepu_key_t a, htepu_key_t b); /* consider x;y only */
unsigned int htepu_keyhash(htepu_key_t a);
unsigned int htepu_keyhash_xy(htepu_key_t a); /* consider x;y only */

#endif
