/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2024 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust Fund in 2024)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_HIERARCHY_H
#define CSCH_HIERARCHY_H

#include <libcschem/concrete.h>
#include <libcschem/abstract.h>
#include <genvector/vtp0.h>

/* describes the current path while descending a hierarchic design */
struct csch_hier_path_s {
	gds_t prefix;        /* path prefix string (cache) */
	csch_hlevel_t *hlev; /* subtree we are in at the moment */
	long hdepth;
};

/* used for enter/leave to store some temporary data on the caller's stack;
   caller should declare it as csch_hier_temp_t htmp and pass it as &htmp */
typedef struct csch_hier_temp_s {
	int prefix_len;
} csch_hier_temp_t;

/* Return 1 if sheet:sym is a subsheet reference, 0 otherwise. If a subsheet ref,
   look up the child subsheet and load *child_out with it. If a sheet needs to
   be loaded from the hlibrary, it is added to the in-memory project; normally
   this means adding it as an external file. If add_to_prj_as_aux is
   not 0, the new sheet is added to the project file as an aux sheet.
   Returns -1 on error (lookup failure) */
int csch_hier_find_sym_child(const csch_sheet_t *sheet, csch_cgrp_t *sym, csch_sheet_t **child_out, int add_to_prj_as_aux);

/* same but with user provided ref strings (in case source is not a concrete
   model symbol, e.g. non-graphical sheets */
int csch_hier_find_child(const csch_sheet_t *sheet, const char *chuuid, const char *chname, const char *chpath,  csch_sheet_t **child_out, int add_to_prj_as_aux);


void csch_hier_path_init(csch_hier_path_t *hpath, csch_abstract_t *abst);
void csch_hier_path_free(csch_hier_path_t *hpath);

int csch_hier_enter(csch_hier_path_t *hpath, csch_hier_temp_t *tmp, csch_cgrp_t *sym, const char *name);
void csch_hier_leave(csch_hier_path_t *hpath, csch_hier_temp_t *tmp);
csch_hlevel_t *csch_hlevel_new(csch_hlevel_t *parent, csch_cgrp_t *sym, const char *name);
void csch_hlevel_free_tree(csch_hlevel_t *hlev);

void csch_hier_build_path(gds_t *prefix, csch_hlevel_t *hlev);

/* The app needs to provide this call */
extern csch_sheet_t *(*csch_app_load_sheet_cb)(csch_project_t *proj, const char *path);

#endif
