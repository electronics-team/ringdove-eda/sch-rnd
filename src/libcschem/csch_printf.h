#include <genvector/gds_char.h>

/* Extra formats:
    %rc     prints csch_coord_t; no suffix is printed
    %$rc    prints csch_coord_t; if round (divisible by 1000) use the 'k' suffix form
    %$$rc   prints csch_coord_t; same as %$rc but no space between num and unit
    %rr     prints rnd_coord_t; same as %rc but scales down the value by 2^10 first
*/
rnd_aft_ret_t csch_printf_app_format(gds_t *string, gds_t *spec, const char **fmt, rnd_unit_allow_t mask, rnd_unit_suffix_t suffix, rnd_aft_arg_t *arg, rnd_aft_arg_t *cookie);

