/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem: design rule checker
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust Fund in 2023)
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#ifndef CSCH_DRC_H
#define CSCH_DRC_H

#include <libcschem/concrete.h>

/* Referencing a violation can not be done using an abstract object ID because
   those IDs get invalidated upon compilation and the typical use case of
   DRC is keeping a list open while fixing violations. Rather reference
   things by name. Any field may be NULL. */
typedef struct csch_drc_abst_ref_s {
	char *net_name;
	char *comp_name;
	char *port_name;
	char *attr_key;
} csch_drc_abst_ref_t;

/* A single violation (part of a list generated for a drc) */
typedef struct csch_drc_violation_s {
	char *title; /* short title for sorting by, in form of topic/title; strdup'd */
	char *desc;  /* long explanation; strdup'd */
	vtp0_t ref;  /* of (csch_drc_abst_ref_t *) indicating where the violation is */
} csch_drc_violation_t;

typedef struct csch_drc_s {
	int ran;
	vtp0_t violations; /* of (csch_drc_violation_t *) */
} csch_drc_t;

/*** For DRC implementations ***/


/* Allocate and free violation */
csch_drc_violation_t *csch_drc_violation_alloc(void);
csch_drc_violation_t *csch_drc_violation_alloc_append(csch_drc_t *ctx);
void csch_drc_violation_free(csch_drc_violation_t *v);

/* Allocate and free reference used in violations */
csch_drc_abst_ref_t *csch_drc_abst_ref_alloc(void);
csch_drc_abst_ref_t *csch_drc_abst_ref_alloc_append(csch_drc_violation_t *v);
void csch_drc_abst_ref_free(csch_drc_abst_ref_t *ref);


/*** Internal ***/

/* Run all DRCs on the project that sheet is part of. Returns the number of
   DRC violations. Results are stored in ctx, the caller needs to call
   csch_drc_uninit(ctx) after the results are processed. */
long csch_drc_project(csch_drc_t *ctx, csch_sheet_t *sheet);

/* Free all fields of ctx (but not ctx itself) */
void csch_drc_uninit(csch_drc_t *ctx);

/* (Un)register drc related actions */
void csch_drc_act_init(void);
void csch_drc_act_uninit(void);

#endif
