/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2018 Tibor 'Igor2' Palinkas
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include "config.h"

#include <stdlib.h>
#include <puplug/puplug.h>
#include <puplug/error.h>
#include <librnd/core/unit.h>
#include <librnd/core/misc_util.h>
#include <librnd/core/compat_misc.h>

#include "libcschem.h"
#include "buildin.h"
#include "project_act.h"
#include "project.h"
#include "project_p4.h"
#include "plug_library.h"
#include "plug_io.h"
#include "integrity.h"
#include "undo.h"
#include "actions_csch.h"
#include "drc.h"

const int *csch_cfg_multiport_net_merge = NULL;
const int *csch_cfg_hier_net_allow_rename = NULL;
const int *csch_cfg_hier_net_rename_prefer_top = NULL;

minuid_session_t csch_minuid;


int csch_init(void)
{
	int pid = rnd_getpid();

	minuid_init(&csch_minuid);
	minuid_salt(&csch_minuid, &pid, sizeof(pid));

	csch_plug_library_init();
	return 0;
}

void csch_uninit(void)
{
	csch_project_uninit();
	csch_project_act_uninit();
	csch_integrity_act_uninit();
	csch_drc_act_uninit();
	csch_undo_act_uninit();
	csch_plug_library_uninit();
}

void csch_uninit_last(void)
{
	csch_p4_uninit();
	csch_plug_io_uninit();
}

int csch_init_actions()
{
	csch_actions_init(&rnd_fgw); /* custom fgw types */
	csch_project_act_init();
	csch_integrity_act_init();
	csch_drc_act_init();
	csch_undo_act_init();
	csch_project_init();
	return 0;
}

static rnd_unit_t csch_unit_tbl[] = {
	{"k",      'c', 1.0/1024000.0,    0, 0,     3, 0},
	{"",        0,  1.0/1024,         0, 0,     3, 0},   /* csch_coord_t no-unit (when an integer coord is provided without a suffix) */
	{"cschem",  0,  1.0/1024,         0, 0,     3, 1}    /* csch_coord_t */
};

rnd_unit_t *csch_unit_k, * csch_unit_base;

unsigned long csch_unit_k_allow, csch_unit_family_cschem;

int csch_init_units(void)
{
	csch_unit_family_cschem = rnd_unit_reg_family();
	rnd_unit_reg_units(csch_unit_tbl, 3, csch_unit_family_cschem);
	rnd_get_value_default_unit = "cschem";
	csch_unit_k_allow = csch_unit_tbl[0].allow;
	csch_unit_k = &csch_unit_tbl[0];
	csch_unit_base = &csch_unit_tbl[1];
	return 0;
}
