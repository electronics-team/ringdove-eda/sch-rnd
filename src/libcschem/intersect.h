/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - libcschem (core library)
 *  Copyright (C) 2022 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 PET Fund in 2022)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/concrete.h>

/* Return number of intersections between o1 and o2; optionally loads iscp
   with intersection points, up to iscp_len. If iscp is NULL, precise
   intersection caclulation is done usign thickness of objects and the number
   of different object-object intersections is returned. If iscp is not NULL,
   only centerline-intersections are considered and iscp coords are all on
   object centerlines (number of total centerline iscs returned, may be
   larger than iscp_len) */
long csch_obj_intersect_obj(csch_sheet_t *sheet, csch_chdr_t *o1, csch_chdr_t *o2, g2d_vect_t *iscp, long iscp_len);
