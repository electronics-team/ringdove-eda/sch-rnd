#ifndef CSCH_VTOIDPATH_H
#define CSCH_VTOIDPATH_H

#include <stdlib.h>
#include <string.h>
#include "libcschem/common_types.h"
#include "libcschem/oidpath.h"

#define GVT(x) csch_vtoidpath_ ## x

#define GVT_ELEM_TYPE csch_oidpath_t
#define GVT_SIZE_TYPE long
#define GVT_DOUBLING_THRS 1024
#define GVT_START_SIZE 4
#define GVT_FUNC
#define GVT_SET_NEW_BYTES_TO 0
#include <genvector/genvector_impl.h>
#define GVT_REALLOC(vect, ptr, size)  realloc(ptr, size)
#define GVT_FREE(vect, ptr)           free(ptr)
#include <genvector/genvector_undef.h>
#endif
