/*
 *                            COPYRIGHT
 *
 *  cschem - modular/flexible schematics editor - sch-rnd (executable)
 *  Copyright (C) 2023 Tibor 'Igor2' Palinkas
 *
 *  (Supported by NLnet NGI0 Entrust in 2023)
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.*
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *  Contact:
 *    Project page: http://repo.hu/projects/sch-rnd
 *    contact lead developer: http://www.repo.hu/projects/sch-rnd/contact.html
 *    mailing list: http://www.repo.hu/projects/sch-rnd/contact.html
 */

#include <libcschem/hierarchy.h>

/* callbacks that implement a non-graphical sheet format */
struct csch_non_graphical_impl_s {
	int (*reload)(csch_sheet_t *sheet);
	int (*compile_sheet)(csch_abstract_t *dst, int viewid, csch_hier_path_t *hpath, const csch_sheet_t *src);
	void (*free_sheet)(csch_sheet_t *sheet);
	char *(*draw_getline)(csch_sheet_t *sheet, long *cnt, void **cookie); /* call to draw next line; *cnt and *cookie are both 0 on initial call and are handled by the callee; return NULL on eof */
};


/* Iterate over all sheets and call ->free_sheet() if the sheet is handled
   by impl. This should be called before a plugin that implements
   non_graphical sheets is unloaded so the callbacks are not left behind */
void csch_non_graphical_impl_pre_unload(csch_non_graphical_impl_t *impl);

