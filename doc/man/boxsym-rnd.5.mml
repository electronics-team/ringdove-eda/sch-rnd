<title>boxsym-rnd</title>
<sect>5</sect>
<date>2022-03-28</date>

<name> boxsym-rnd - boxsym description syntax </name>

<description>
A boxsym file specifies symbol attributes and pins (terminals) for a
single box symbol or for a set of inhomogenousely slotted box symbols.
When slotted symbols are generated, one symbol file is generated for each
slot containing only the pins defined for the given slot (file name
suffixed with slot name) and a monolith symbol is generated (using the
original file name) with all pins in one large box.

<subsect>
	<sstitle> Syntax </sstitle>
	<sstext>
		<p>
		The boxsym syntax is a plain text, line based, hierarchical file format
		with a syntax similar to tEDAx. Leading whitespace in lines are ignored.
		Empty lines and lines starting with a hashmark (#) are ignored. The first
		word of a line is a command.
		<p>
		If the command is <call>begin</call>, the rest of the lines until the
		matching <call>end</call> line are read as a nested level of hierarchy.
		The syntax allows "begin in begin" (nesting to any depth). The second
		word of begin and end are the type of the nested subtree.
		<p>
		Commands issued outside of any begin-end blocks are said to be made in
		global scope.
	</sstext>
</subsect>

<subsect>
	<sstitle> Refdes </sstitle>
	<sstext>
	<p>
	There can be only one refdes line, in global scope. The argument is
	a single word, the refdes attribute. If no refdes is specified, the
	default refdes is used.
	</sstext>
</subsect>


<subsect>
	<sstitle> Attributes </sstitle>
	<sstext>
	<p>
	Attributes are symbol attributes. Attributes specified in the global scope
	will be present in every symbol generated from the file. Attributes specified
	for a slot will be present in the slot's symbol and in the monolith symbol.
	<p>
	An attribute is defined in the following form:
	<p>
	<call>attr</call> <arg>key</arg> <arg>value</arg>
	<p>
	Key is a single word, value is anything up to the end of the line. This
	defintion creates the attribute and places a dyntext+floater text object
	printing the value of the attribute. If command is <call>attr_both</call>,
	the text object is created to print key=value. If command is
	<call>attr_invis</call> the attribute is still created but no text object
	is made.
	</sstext>
</subsect>

<subsect>
	<sstitle> Other global settings </sstitle>
	<sstext>
	<p>
	A line starting with text_size_mult followed by a decimal number (greater
	than zero) changes text pixel length allocation factor. Increasing values
	will allocate more pixels to each text in length (text height is not affected). 
	
	</sstext>
</subsect>

<subsect>
	<sstitle> Defining pins </sstitle>
	<sstext>
	<p>
	Pins are defined using a pin subtree: begin pin pinname, then lines defining
	pin properties and then an end pin. Pinname should be the textual, human
	readable name of the pin (also used as an input for the optional devmap),
	not the physical pin number.
	<p> For example:
	<pre>
begin pin Vcc
	num 14
	loc top
end pin
begin pin B
	num 2:5:10:13
	loc left
	dir in
	invcirc
	funcmap
end pin
	</pre>
	<p>
	Within the pin block the <call>num</call> line specifies the physical
	footprint pin number; for a pin participating in a slot this should be
	a colon separated list of pin number per slot.
	<p>
	The mandatory <call>loc</call> line specifies the location of the pin,
	the side of the box on which it should be placed, one of: left, right,
	top or bottom.
	<p>
	If a <call>dir</call> line is present, extra "pin direction" graphics is
	drawn for the pin inside of the box. Values are in or out 
	<p>
	If <call>invcirc</call> is present, the pin will have a small circle
	near the box to indicate the pin is inverted.
	<p>
	If <call>label</call> is present its value (anything written after "label")
	is printed within the box next to the pin instead of the pin name.
	<p>
	If <call>funcmap</call> is present, the label is replaced with a dyntext
	that prints abstract attribute funcmap/name. This is useful for symbols
	that are going to use a funcmap (alternate pin function mapping). The
	funcmap file is normally also referenced from the component, setting
	the funcmap attribute to the funcmap file name.
	</sstext>
</subsect>

<subsect>
	<sstitle> Arranging pins </sstitle>
	<sstext>
	<p>
	By default pins are listed per side (loc), tightly packed, in order of
	appearance in the file from the start of the side. This can be changed
	using pinalign lines in global or slot scope:
	<pre>
pinalign left center
	</pre>
	<p>
	will arrange left side pins on the center (vertically).
	</sstext>
</subsect>

<subsect>
	<sstitle> Defining a single box symbol </sstitle>
	<sstext>
		The simplest form of box symbols emit a single file that has a single
		large box with all pins. The boxsym definition for such symbols contain
		the above described constructs and no "begin slot" subtrees.
	</sstext>
</subsect>

<subsect>
	<sstitle> Defining inhomogenous slot symbol </sstitle>
	<sstext>
		In an multi-box, inhomogenous slot symbol there are different symbols
		representing different sections of the component.
		<p>
		The typical example is an MCU which is split up into a couple of
		boxes: a few boxes per port groups and another few per peripheral
		types. Each box represents a slot that appears only once in the MCU.
		<p>
		Another typical example is logic ICs, e.g. 7400 implemented in two
		different slot symbols: one for power (Vcc and gnd), presents once
		in the component, and one for a 3 pin gate, same box used 4 times
		for 4 different slots.
		<p>
		An inhomogenous slot symbol does not have any pin in global scope, but
		defines one or more "begin slot slotname" subtrees for all the different
		type of slots. If the same slot appears multiple times in the component,
		it needs to be defined only once (and multiple pin numbers assigned
		in pin definitions.)
	</sstext>
</subsect>
</description>

<example>

<subsect>
	<sstitle> Single box LDO </sstitle>
	<sstext>
<pre>
refdes U??
attr_invis sym-source       sch-rnd default symbol lib
attr_invis sym-copyright    (C) 2022 Tibor 'Igor2' Palinkas
attr_invis sym-license-dist GPLv2+
attr_invis sym-license-use  Public Domain

pinalign bottom center
pinalign left center
pinalign right center

shape box
shape_fill yes
begin pin in
	num 1
	loc left
end pin

begin pin gnd
	num 2
	loc bottom
end pin

begin pin out
	num 3
	loc right
end pin


</pre>
	</sstext>
</subsect>

<subsect>
	<sstitle> Inhomogenous slot 7400</sstitle>
	<sstext>
<pre>
refdes U??

pinalign right center
pinalign bottom center
pinalign top center
attr device 7400

begin slot power
	shape box
	shape_fill yes
	begin pin Vcc
		num 14
		loc top
	end pin
	begin pin gnd
		num 7
		loc bottom
	end pin
	pinalign top center
	pinalign bottom center
end slot


begin slot logic
	attr device 7400
	attr_both slot 1

	begin pin A
		num 1:4:9:12
		loc left
		dir in
	end pin

	begin pin B
		num 2:5:10:13
		loc left
		dir in
	end pin

	begin pin Z
		num 3:6:8:11
		loc right
		dir out
		invcirc
	end pin

	pinalign left center
	pinalign right center
end slot


</pre>
	</sstext>
</subsect>

</example>

<also>boxsym-rnd(1)</also>

