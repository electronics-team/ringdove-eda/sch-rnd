ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAp;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAq; loclib_name=bc817_sot23;
						li:objects {
						}
						ha:attrib {
							footprint=SOT23
							li:portmap {
								{B->pcb/pinnum=1}
								{E->pcb/pinnum=2}
								{C->pcb/pinnum=3}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
			ha:group.2 {
				uuid=mpbUxZA3TcX63n9yc2EAAAA9;
				li:objects {
					ha:group.1 {
						uuid=mpbUxZA3TcX63n9yc2EAAAA+; loclib_name=bc817;
						li:objects {
						}
						ha:attrib {
							spice/model_card={* (C) 2023 Tibor 'Igor2' Palinkas
* License: CC0 (no rights reserved): https://creativecommons.org/publicdomain/zero/1.0/
* Source: based on Vishay's datasheet: https://archive.org/details/bc817_spice_vishay
*
.model BC817 npn (
+ IS=2.43485e-13 VAF=10 BF=270 IKF=1.12487 NE=1.84302 ISE=7.17747e-12
+ IKR=0.149889 ISC=2.42047e-12 NC=3.94859 NR=1.04566 BR=2.51332 RC=0.407107
+ CJC=5.516e-11 FC=0.8 MJC=0.529364 VJC=0.4 CJE=5.10473e-11 MJE=0.412728
+ VJE=0.561234 TF=7.1424e-10 ITF=0.175457 VTF=1.86025 XTF=1.09929 RB=6.50128
+ IRB=0.1 RBM=0.1 RE=0.0814215 TR=1e-07
+ )

}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=spicelib; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=60PZRPnx0Y9mYJZvZgYAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=20000; y=100000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAK; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAL; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=C1
					role=symbol
					value=10u
				}
			}
			ha:group.3 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAM;
				x=-20000; y=-20000;
				li:objects {
					ha:line.1 { x1=40000; y1=120000; x2=32000; y2=120000; stroke=wire; }
					ha:text.2 { x1=33000; y1=120000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.5 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAN;
				x=-20000; y=-20000;
				li:objects {
					ha:line.1 { x1=60000; y1=120000; x2=76000; y2=120000; stroke=wire; }
					ha:line.3 { x1=64000; y1=120000; x2=64000; y2=120000; stroke=junction; }
					ha:line.4 { x1=64000; y1=100000; x2=64000; y2=140000; stroke=wire; }
					ha:text.5 { x1=66000; y1=120000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=int_b
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.7 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAU; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=44000; y=140000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAV; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAW; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R1
					role=symbol
					value=87k
				}
			}
			ha:group.9 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAa; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=44000; y=80000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAb; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAc; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R2
					role=symbol
					value=10k
				}
			}
			ha:group.14 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAr;
				x=-52000; y=-20000;
				li:objects {
					ha:line.1 { x1=120000; y1=112000; x2=120000; y2=56000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.16 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAs;
				x=-40000; y=-20000;
				li:objects {
					ha:line.1 { x1=84000; y1=80000; x2=84000; y2=56000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.18 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=68000; y=140000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAAy; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R3
					role=symbol
					value=33k
				}
			}
			ha:group.19 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAAz;
				x=-52000; y=-20000;
				li:objects {
					ha:line.1 { x1=120000; y1=128000; x2=120000; y2=140000; stroke=wire; }
					ha:line.2 { x1=120000; y1=132000; x2=132000; y2=132000; stroke=wire; }
					ha:line.3 { x1=120000; y1=132000; x2=120000; y2=132000; stroke=junction; }
					ha:text.4 { x1=122000; y1=132000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=int_c
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.22 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAA0;
				x=-40000; y=-20000;
				li:objects {
					ha:line.1 { x1=84000; y1=160000; x2=84000; y2=172000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.24 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAA1;
				x=-52000; y=-20000;
				li:objects {
					ha:line.1 { x1=120000; y1=160000; x2=120000; y2=172000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.26 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAA5; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=80000; y=112000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAA6; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAA7; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=C2
					role=symbol
					value=10u
				}
			}
			ha:group.28 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAA/; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=108000; y=100000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAABA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=60PZRPnx0Y9mYJZvZgYAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R4
					role=symbol
					value=100k
				}
			}
			ha:group.29 {
				uuid=60PZRPnx0Y9mYJZvZgYAAABC;
				x=-60000; y=-20000;
				li:objects {
					ha:line.1 { x1=160000; y1=132000; x2=176000; y2=132000; stroke=wire; }
					ha:line.2 { x1=168000; y1=120000; x2=168000; y2=132000; stroke=wire; }
					ha:line.3 { x1=168000; y1=132000; x2=168000; y2=132000; stroke=junction; }
					ha:text.4 { x1=173000; y1=132000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=out
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.32 {
				uuid=60PZRPnx0Y9mYJZvZgYAAABD;
				x=-60000; y=-20000;
				li:objects {
					ha:line.2 { x1=168000; y1=100000; x2=168000; y2=56000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.46 {
				uuid=60PZRPnx0Y9mYJZvZgYAAABz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=16000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAB0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.48 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAB1; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=44000; y=36000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAB2; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.50 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAB3; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=68000; y=36000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAB4; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.52 {
				uuid=60PZRPnx0Y9mYJZvZgYAAAB5; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=108000; y=36000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAAB6; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.58 {
				uuid=60PZRPnx0Y9mYJZvZgYAAACD; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=68000; y=152000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAACE; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.60 {
				uuid=60PZRPnx0Y9mYJZvZgYAAACF; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=44000; y=152000;
				li:objects {
					ha:group.1 {
						uuid=60PZRPnx0Y9mYJZvZgYAAACG; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.70 {
				uuid=emEhKEkrgdRFzXxchToAAABZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAACK;
				x=56000; y=100000;
				li:objects {
					ha:polygon.1 {
						li:outline {
							ha:line { x1=10266; y1=-1780; x2=9224; y2=-3517; }
							ha:line { x1=9224; y1=-3517; x2=10935; y2=-3368; }
							ha:line { x1=10935; y1=-3368; x2=10266; y2=-1780; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.2 {
						uuid=emEhKEkrgdRFzXxchToAAABa; src_uuid=iNOQfJpO6hT/HFDFGjoAAACL;
						x=12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=emEhKEkrgdRFzXxchToAAABb; src_uuid=iNOQfJpO6hT/HFDFGjoAAACM;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=B
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.4 {
						uuid=emEhKEkrgdRFzXxchToAAABc; src_uuid=iNOQfJpO6hT/HFDFGjoAAACN;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=E
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:text.5 { x1=8000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.6 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.7 { cx=9000; cy=0; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=7000; y1=4000; x2=7000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=4000; y1=0; x2=7000; y2=0; stroke=sym-decor; }
					ha:line.10 { x1=7000; y1=-1000; x2=12000; y2=-4000; stroke=sym-decor; }
					ha:line.11 { x1=7000; y1=1000; x2=12000; y2=4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=bc817_sot23
					name=Q1
					role=symbol
					spice/model=bc817
				}
			}
			ha:connection.74 {
				li:conn {
					/2/3/1
					/2/2/2/1
				}
			}
			ha:connection.75 {
				li:conn {
					/2/5/1
					/2/2/1/1
				}
			}
			ha:connection.102 {
				li:conn {
					/2/7/1/1
					/2/5/4
				}
			}
			ha:connection.103 {
				li:conn {
					/2/9/2/1
					/2/5/4
				}
			}
			ha:connection.104 {
				li:conn {
					/2/16/1
					/2/9/1/1
				}
			}
			ha:connection.105 {
				li:conn {
					/2/22/1
					/2/7/2/1
				}
			}
			ha:connection.106 {
				li:conn {
					/2/48/1/1
					/2/16/1
				}
			}
			ha:connection.107 {
				li:conn {
					/2/60/1/1
					/2/22/1
				}
			}
			ha:connection.108 {
				li:conn {
					/2/19/1
					/2/18/1/1
				}
			}
			ha:connection.109 {
				li:conn {
					/2/24/1
					/2/18/2/1
				}
			}
			ha:connection.116 {
				li:conn {
					/2/50/1/1
					/2/14/1
				}
			}
			ha:connection.120 {
				li:conn {
					/2/58/1/1
					/2/24/1
				}
			}
			ha:connection.121 {
				li:conn {
					/2/70/2/1
					/2/19/1
				}
			}
			ha:connection.122 {
				li:conn {
					/2/70/3/1
					/2/5/1
				}
			}
			ha:connection.123 {
				li:conn {
					/2/70/4/1
					/2/14/1
				}
			}
			ha:connection.124 {
				li:conn {
					/2/26/2/1
					/2/19/2
				}
			}
			ha:connection.125 {
				li:conn {
					/2/29/1
					/2/26/1/1
				}
			}
			ha:connection.126 {
				li:conn {
					/2/29/2
					/2/28/2/1
				}
			}
			ha:connection.127 {
				li:conn {
					/2/32/2
					/2/28/1/1
				}
			}
			ha:connection.130 {
				li:conn {
					/2/52/1/1
					/2/32/2
				}
			}
			ha:group.137 {
				uuid=qO6PuXTr1q4KsRucJCoAAABF; src_uuid=qO6PuXTr1q4KsRucJCoAAABC;
				x=120000; y=108000;
				li:objects {
					ha:text.1 { x1=0; y1=-2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=qO6PuXTr1q4KsRucJCoAAABG; src_uuid=qO6PuXTr1q4KsRucJCoAAABD;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=qO6PuXTr1q4KsRucJCoAAABH; src_uuid=qO6PuXTr1q4KsRucJCoAAABE;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CN2
					role=symbol
					spice/omit=yes
				}
			}
			ha:connection.138 {
				li:conn {
					/2/137/3/1
					/2/29/1
				}
			}
			ha:group.139 {
				uuid=qO6PuXTr1q4KsRucJCoAAABI;
				li:objects {
					ha:line.1 { x1=116000; y1=108000; x2=112000; y2=108000; stroke=wire; }
					ha:line.2 { x1=112000; y1=108000; x2=112000; y2=104000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.140 {
				li:conn {
					/2/139/1
					/2/137/2/1
				}
			}
			ha:group.141 {
				uuid=qO6PuXTr1q4KsRucJCoAAABL; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=112000; y=104000;
				li:objects {
					ha:group.1 {
						uuid=qO6PuXTr1q4KsRucJCoAAABM; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.142 {
				li:conn {
					/2/141/1/1
					/2/139/2
				}
			}
			ha:group.143 {
				uuid=qO6PuXTr1q4KsRucJCoAAABQ; src_uuid=qO6PuXTr1q4KsRucJCoAAABC;
				x=8000; y=96000; mirx=1;
				li:objects {
					ha:text.1 { x1=0; y1=-2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=qO6PuXTr1q4KsRucJCoAAABR; src_uuid=qO6PuXTr1q4KsRucJCoAAABD;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=qO6PuXTr1q4KsRucJCoAAABS; src_uuid=qO6PuXTr1q4KsRucJCoAAABE;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CN1
					role=symbol
					spice/omit=yes
				}
			}
			ha:connection.144 {
				li:conn {
					/2/143/3/1
					/2/3/1
				}
			}
			ha:group.145 {
				uuid=qO6PuXTr1q4KsRucJCoAAABT;
				li:objects {
					ha:line.1 { x1=12000; y1=96000; x2=16000; y2=96000; stroke=wire; }
					ha:line.2 { x1=16000; y1=96000; x2=16000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.146 {
				li:conn {
					/2/145/1
					/2/143/2/1
				}
			}
			ha:connection.147 {
				li:conn {
					/2/46/1/1
					/2/145/2
				}
			}
		}
		ha:attrib {
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={high level SIM: BJT amplifier, tran}
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
    }
   }
  }
}
