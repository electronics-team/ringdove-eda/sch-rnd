ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=LnNTcUkV1CIzG7F2EXUAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.3 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAY;
				x=-28000; y=-32000;
				li:objects {
					ha:line.1 { x1=36000; y1=116000; x2=52000; y2=116000; stroke=wire; }
					ha:text.4 { x1=49000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.8 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAg; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=24000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R1
					role=symbol
					spice/prefix=R
					value=10k
				}
			}
			ha:group.10 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAp; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=52000; y=76000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAr; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=C1
					role=symbol
					spice/prefix=C
					value=1u
				}
			}
			ha:group.11 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAs;
				x=-28000; y=-32000;
				li:objects {
					ha:line.2 { x1=80000; y1=116000; x2=80000; y2=108000; stroke=wire; }
					ha:line.3 { x1=72000; y1=116000; x2=88000; y2=116000; stroke=wire; }
					ha:line.4 { x1=80000; y1=116000; x2=80000; y2=116000; stroke=junction; }
					ha:text.5 { x1=84000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=mid
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.14 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=60000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA1; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R2
					role=symbol
					spice/prefix=R
					value=15k
				}
			}
			ha:group.16 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA2; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=88000; y=76000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA3; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA4; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=C2
					role=symbol
					spice/prefix=C
					value=1.2u
				}
			}
			ha:group.17 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA5;
				x=-28000; y=-32000;
				li:objects {
					ha:line.2 { x1=116000; y1=116000; x2=116000; y2=108000; stroke=wire; }
					ha:line.3 { x1=108000; y1=116000; x2=132000; y2=116000; stroke=wire; }
					ha:line.4 { x1=116000; y1=116000; x2=116000; y2=116000; stroke=junction; }
					ha:text.5 { x1=128000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=out
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.20 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA6;
				x=-28000; y=-32000;
				li:objects {
					ha:line.1 { x1=80000; y1=88000; x2=80000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.22 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA7;
				x=-28000; y=-32000;
				li:objects {
					ha:line.1 { x1=116000; y1=88000; x2=116000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.24 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=12000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.26 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABC; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=52000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABD; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.28 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABE; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=88000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABF; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.36 {
				li:conn {
					/2/8/2/1
					/2/3/1
				}
			}
			ha:connection.37 {
				li:conn {
					/2/11/2
					/2/10/2/1
				}
			}
			ha:connection.38 {
				li:conn {
					/2/11/3
					/2/8/1/1
				}
			}
			ha:connection.40 {
				li:conn {
					/2/14/2/1
					/2/11/3
				}
			}
			ha:connection.41 {
				li:conn {
					/2/17/2
					/2/16/2/1
				}
			}
			ha:connection.42 {
				li:conn {
					/2/17/3
					/2/14/1/1
				}
			}
			ha:connection.43 {
				li:conn {
					/2/20/1
					/2/10/1/1
				}
			}
			ha:connection.44 {
				li:conn {
					/2/22/1
					/2/16/1/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/26/1/1
					/2/20/1
				}
			}
			ha:connection.47 {
				li:conn {
					/2/28/1/1
					/2/22/1
				}
			}
			ha:group.48 {
				uuid=65ViUwZSa7RX5OF8xWcAAAAk; src_uuid=65ViUwZSa7RX5OF8xWcAAAAh;
				x=4000; y=80000; mirx=1;
				li:objects {
					ha:text.1 { x1=0; y1=-2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=65ViUwZSa7RX5OF8xWcAAAAl; src_uuid=65ViUwZSa7RX5OF8xWcAAAAi;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=65ViUwZSa7RX5OF8xWcAAAAm; src_uuid=65ViUwZSa7RX5OF8xWcAAAAj;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(2,1)
					name=CN1
					role=symbol
					spice/omit=yes
				}
			}
			ha:connection.49 {
				li:conn {
					/2/48/3/1
					/2/3/1
				}
			}
			ha:group.50 {
				uuid=65ViUwZSa7RX5OF8xWcAAAAn;
				li:objects {
					ha:line.1 { x1=8000; y1=80000; x2=12000; y2=80000; stroke=wire; }
					ha:line.2 { x1=12000; y1=80000; x2=12000; y2=48000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.51 {
				li:conn {
					/2/50/1
					/2/48/2/1
				}
			}
			ha:group.53 {
				uuid=65ViUwZSa7RX5OF8xWcAAAAr; src_uuid=65ViUwZSa7RX5OF8xWcAAAAh;
				x=108000; y=80000;
				li:objects {
					ha:text.1 { x1=0; y1=-2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=65ViUwZSa7RX5OF8xWcAAAAs; src_uuid=65ViUwZSa7RX5OF8xWcAAAAi;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=65ViUwZSa7RX5OF8xWcAAAAt; src_uuid=65ViUwZSa7RX5OF8xWcAAAAj;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:polygon.4 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=6000; }
							ha:line { x1=0; y1=6000; x2=4000; y2=6000; }
							ha:line { x1=4000; y1=6000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(2,1)
					name=CN2
					role=symbol
					spice/omit=yes
				}
			}
			ha:connection.54 {
				li:conn {
					/2/53/3/1
					/2/17/3
				}
			}
			ha:group.55 {
				uuid=65ViUwZSa7RX5OF8xWcAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=100000; y=48000;
				li:objects {
					ha:group.1 {
						uuid=65ViUwZSa7RX5OF8xWcAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.56 {
				uuid=65ViUwZSa7RX5OF8xWcAAAAy;
				li:objects {
					ha:line.1 { x1=100000; y1=48000; x2=100000; y2=80000; stroke=wire; }
					ha:line.2 { x1=100000; y1=80000; x2=104000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.58 {
				li:conn {
					/2/56/2
					/2/53/2/1
				}
			}
			ha:connection.59 {
				li:conn {
					/2/56/1
					/2/55/1/1
				}
			}
			ha:connection.60 {
				li:conn {
					/2/50/2
					/2/24/1/1
				}
			}
		}
		ha:attrib {
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={high level SIM: passives, tran}
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
