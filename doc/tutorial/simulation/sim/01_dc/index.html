<html>
<body>
<h1> 01_dc: sheet preparation and DC op point </h1>

<h2> Scope </h2>
<p>
In this simulation we are going to calculate the DC operating point of a
simple voltage divider. This is also an introduction to setting up a
sheet for circuit simulation and setting up the high level simulation
infrastructure.

<h2> The schematics </h2>
<p>
The single-sheet schematic contains the voltage divider with all networks
named. There are no extra symbols for the simulation, the circuit is in its
original form, as designed for the PCB workflow.
<p>
<center>
<a href="01_dc.rs"><img src="01_dc.svg" width=400px></a>
<br>Click the image to get the sch-rnd sheet; also requires this <a href="project.lht">project.lht</a> in the same directory</center>
<p>

<h2> SPICE: what is a DC op point </h2>
<p>
In SPICE simulation there are different <i>analysis</i> options available: these
are different simulation actions or operations or modes. Basically each
analysis is a different algorithm to run on the circuit. The simplest
analysis is called the "DC operating point analysis".
<p>
In the op point analysis, the simulator will apply all sources, assume
all inductors are shorted and assume all capacitors are open and then calculate
the voltage for all nodes. (Node is the spice terminology for an equipotential
electrical network.) This simulation is not time or frequency dependent, and
represents a single DC operating point once the circuit has already stabilized.
<p>
In our example, this means 5V would be applied at CN1 then out1 and
out2 are calculated. Since our voltage sources are ideal and capable of supplying
any amount of current, the voltage on the in network will be 5V; but the
voltages on out1 and out2 will depend on the resistor values of R1, R2 and R3.


<h2> Preparing for simulation </h2>

<h3> Symbols and nets </h3>
<p>
Draw the schematics as usual; make sure there is a gnd network, spice won't
work without that. Ideally, use the stock gnd symbol for that. Make sure
all resistors have a unique name and a value. Spice understands the normal
SI suffixes such as k for kilo, but as it is generally not case sensitive,
m and M are both milli, so you will need to write meg to get mega.
<p>
Your symbols also need to have the proper spice pin numbers. First switch
your view from the default pcb to sim_ngspice: there's a button for this on
the top right part of the main window, on the left of the help/support button.
This pops up a view selector; click sim_ngspice twice and the selector will close
and the view will change. The new view will show pin numbers as seen by the
spice target. For plain resistors, pin ordering does not matter, but it is
important for polarized parts like diodes, transistors, and sources. The
stock library has spice pin numbers set up and should work without
modification. Later chapters of this tutorial will explain how to deal with
spice pin numbers in symbols.
<p>
Using the sim_ngspice view CN1 and CN2 are crossed out with red. This means
these symbols are not exported: spice can not simulate connectors. This happens
because the attribute spice/omit is set to yes on these symbols. The stock
library has spice/omit set on mechanical symbols.
<p>
The message log also has an error message saying that modifications could
not be applied to the sim setup because there is no active simulation setup.
This is normal when sim_ngspice is manually selected, which is the rare case.
Normally a simulation setup is activated from the simulation setup dialog and
that configures everything properly.

<h3> Opening the simulation setup dialog </h3>
<p>
High level simulation needs to store a lot of simualtion-specific configuration:
different simulation setups, different output (e.g. plot) options. Since
many real life designs use multiple sheets, these can not be stored on the sheet
level but has to be stored on the project level. Thus the high level sim
feature always operates on the project level and always stores configuration
in the project file, project.lht. This is true even on the single sheet case.
<p>
The dialog box for the high levle sim feature is accessible through the
File menu, Project... submenu, Circuit simulation submenu. This opens the
<i>Simulation selector</i> dialog, presenting a set of different simulation
setups (or sim setups for short). Different sim setups will simulate
<ul>
	<li> different subsets of the circuit
	<li> with different stimulus
	<li> using different networks for the output
	<li> doing different presentation of those results
</ul>
<p>
A typical use case is to have a sim setup for evaluating the board in the
simplest DC case and a few other sim setups exploring how the circuit works
with different AC stimulus. Another typical use case is to take a complex
circuit apart and simulate different stages separately - this can be done
without adding any extra symbols or other clutter in the drawing, only by
high level sim configuration.
<p>
In this example, and in most of the examples, there would be only one
simulation, to keep things simple. Click on that one item on the list to
select it and click the <i>Open...</i> button. (This sim setup was once created
by clicking the <i>New...</i> button, which asked for a name for the new sim
setup, created it blank, and then opened the same sim setup dialog described
below.). Sim setup names are arbitrary user assigned strings, only to help
the user remember which sim setup is for what (sch-rnd is not basing any
decision on the name).
<p>
This opens the <i>simulation setup</i> dialog for the selected sim setup.
<p>
<center>
<img src="sim_setup.png">
<br>Simulation setup dialog, with 3 tabs</center>
<p>
The sim setup dialog has 3 tabs and a control block on the bottom. Each
tab is configuring an important aspect of the given simulation setup
and is going to be described in detail below.

<h3> Sim setup: test bench & modifications </h3>
<p>
The first tab defines the circuit that is going to be simulated:
<ul>
	<li> "test bench to use" can limit the scope, picking parts of the circuit
	     to include int he simulation, omitting other parts; for this example
	     and for most of these simple examples in the tutorial we are going with
	     "whole circuit" (instead of partial circuit simulation)
	<li> "modifications" is a table that lists modifications to be performed on
	     the circuit's abstract model before the simulation. All the tutorial
	     examples heavily depend on this feature.
</ul>
<p>
This is equvalent to the process of physical testing of a circuit:
once the basic circuit is built as documented by the schematics, optionally
a part of it is isolated for testing (this is called test benching in sch-rnd)
and external stimulus, probes and sometimes artifical connections or loading
resistors and alike are added for the measurement (this is the modifications
part).
<p>
Select the only item in the modifications tableand click the Edit button. This
opens the modification editor that shows:
<ul>
	<li> this modification is going to add...
	<li> ... a voltage source (V)
	<li> connected between CN1-2 and GND (it doesn't matter that CN1 is omitted
	     from the export, the connection is really made to networks and CN1 is only
	     used to look up the nets to connect the new voltage source to); since the
	     negative pole is not named, sch-rnd assumes it's GND
	<li> with a dc value of 5 (volts)...
	<li> ... and no AC value or time dependent (function) value
</ul>
<p>
<center>
<img src="sim_mod.png">
<br>Simulation modification, adding a DC voltage source</center>
<p>
Or in short: this is the 5V power supply connected to CN1.
<p>
The advantage of having this as a modification instead of drawing it as a
voltage source symbol on the sheet is the less visual clutter on the drawing,
especially when not dealing with simulation. It is also more natural to say
"connect 5V to CN1 for this test" than to draw symbols of non-existing parts.
<p>
This advantage pays off even more when there are multiple different simulation
setups each having different sources installed at random parts of the circuit.
Without modifications, all sources of all simulation setups would need to be
added to the drawing (or on separate "test bench sheets").
<p>
Note: CN1-2 uses component name - <i>port name</i>;
<i>port name</i> means the name the port is listed by in the abstract
model, which is typically the original terminal name, and not the
"pin number", or the calculated display/name attribute. In case oc CN1-2, port
name and "pin number" or display/name happens to match, but for example using
a polarized symbol, e.g. a diode it is typically A and C (not 1 and 2);
or using a transistor symbol, the port names are B, E, C (and not 1, 2 and 3).
Tip: switch to the raw view to reveal the original port names or use the
abstract model dialog to find them.

<h3> Sim setup: output config </h3>
<p>
The second tab, output config, defines the spice analysis to execute and
how the results are presented. A single simulation setup can define multiple
output, in which case each simulation is ran on the given setup, in the order
they are specified, and are presented in the same order. This is useful to
get multiple printouts or plots of the same or different analyses.
<p>
<center>
<img src="sim_setup2.png">
<br>Simulation setup dialog, second tab</center>
<p>
Select the first entry, which runs the 'op' analysis and presents the result
simply printing values. Click on the edit button. This opens the sim output
config dialog. The top half of the dialog box configures the analysis; the
op analysis does not have parameters so it's sparse in this example. The bottom
half configures the presentation, which is set to 'print' in this case, which 
will simply print the textual value of any property listed. The list can be
edited using New/Edit/Remove buttons. Each item of the list is a network name
or a component-port pair (or a function based on one of these). In this example
we are measuring voltages at two pins of connector CN2.
<p>
<center>
<img src="sim_out.png">
<br>Simulation output configuration for op+print</center>
<p>

<h3> Sim setup: run &amp; output </h3>
<p>
The third tab presents simulation results. Before the simulation is first
ran, this tab is empty. Click on the 'run' button in the bottom control block
of the dialog box; the 'run' button will activate the sim setup (selects
the sim_ngspice view and sets all sim related configuration) and run
the simulation (ngspice) in the background and fill in the third tab once
the simulation finishes.
<p>
In this simple example the output will contain:
<pre>
CN2-3 = 2.38095238e+00
CN2-2 = 1.19047619e+00
</pre>
the voltages measured on pin 3 and 2 of the connector CN2, as requested in
the output config.
<p>
<center>
<img src="sim_setup3.png">
<br>Simulation setup dialog, third tab, after execution</center>
<p>

