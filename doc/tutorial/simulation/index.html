<html>
<body>

<h1> sch-rnd simulation (SPICE) tutorial </h1>
<p>
This tutorial goes through the topic of Circuit Simulation with sch-rnd.
More specifically: lumped element simulation using SPICE (various implementations).
Since sch-rnd itself does not contain any simulation code, all the
methods described here rely on external SPICE simulation software.
<p>
As of writing (2023), there are a bunch of proprietary SPICE implementations
and a few open source alternatives. This tutorial is focusing on the open
source implementations. There are three main open source alternatives:
<ul>
	<li> ngspice: derived from the traditional Berkely SPICE, upgraded with
	     additional extras (such as mixed analog-digital and behavioral
	     simulation by merging xspice and CIDER).
	<li> gnucap: independent implementation; may run faster than ngspice;
	     no mixed analog/digital support in sch-rnd. Not recommended
	     because of various problems in compatibility and stability.
	<li> xyce: should work but is not tested with sch-rnd at the moment.
</ul>
<p>
At the moment <b>sch-rnd recommends using ngspice</b> because that implementation
has the most complete support and is the most well-tested with sch-rnd.


<h2> Two levels of simulation </h2>
<p>
Sch-rnd is designed to allow single schematics to be used for different
workflows, e.g. both spice simulation and PCB layout. There are various
(sometimes optional) abstractions over spice to make this possible.
<p>
The low level of simulation are:
<ul>
	<li> The <b>raw spice</b> approach;
	     this assumes the user speaks spice and wants to run the simulator
	     by hand, outside of sch-rnd (e.g. from a shell or Makefile). When
	     using this method, the schematics will likely end up with spice
	     commands and parameters that depend on a spice implementation.
	     That is, "this sheet is for ngspice" or "this sheet is for gnucap".
	<li> The more generic <b>simulation</b> approach,
	     which introduces a higher abstraction layer over spice. The advantages
	     are that the user does not need to know as much about spice, the
	     same sheet can be used with different spice implementations and
	     sch-rnd can coordinate executing the simulation and can provide a GUI
	     for visualizing the results. It is possible to keep a plot window open,
	     make changes on the sheet (e.g. tune resistor values) and rerun the
	     simulation getting the plot updated with a single click.
	     Simulator execution and GUI are optional,
	     the abstraction itself can be used from command line too. The drawback
	     is that the user needs to learn an abstraction that is specific to
	     sch-rnd, while some minimal spice knowledge (for the models) is still
	     required.
</ul>
<p>
For new users the recommendation is: unless you already have a lot of
experience with spice, it is better to go for the <b>simulation</b> approach.
<p>
The user manual contains <a href="../../user/06_features/simulation/index.html">
a section on simulation.</a>

<h2> Tutorial </h2>
<p>
The tutorial is a system of smallish examples. Each new example adds something
and assumes previous examples are understood. The recommended way of
reading the tutorial is to pick one of the raw
simulator or the high level sim target and click through the examples for
that column.
<p>
Note: the raw examples are always written for ngspice with considerations
for other spice implementations mentioned at the end of each example. The high
level simulation works only with ngspice at the moment.
<p>
<table border=1 cellspacing=0>
<tr>
	<th> title
	<th> raw ngspice
	<th> raw gnucap
	<th> high level sim
	<th> concepts

<tr>
	<td> 01_dc
	<td> <a href="raw/01_dc.html"> yes </a>
	<td> <a href="raw/01_dc.html"> yes </a>
	<td> <a href="sim/01_dc/index.html"> yes </a>
	<td>      sch-rnd: sheet setup
	     <br> sim: dc operating point

<tr>
	<td> 04_passive_tr
	<td> <a href="raw/04_passive_tr.html"> yes </a>
	<td> <a href="raw/04_passive_tr.html"> yes </a>
	<td> <a href="sim/04_passive_tr/index.html"> yes </a>
	<td> sim: transient (time domain) analysis, voltage source (pulsed)


<tr>
	<td> 06_passive_ac
	<td> <a href="raw/06_passive_ac.html"> yes </a>
	<td> <a href="raw/06_passive_ac.html"> yes </a>
	<td> <a href="sim/06_passive_ac/index.html"> yes </a>
	<td> sim: ac (frequency domain) analysis


<tr>
	<td> 10_bjt_amp_tr
	<td> <a href="raw/10_bjt_amp_tr.html"> yes </a>
	<td> <a href="raw/10_bjt_amp_tr.html"> yes </a>
	<td> <a href="sim/10_bjt_amp_tr/index.html"> yes </a>
	<td>      sch-rnd: spice model, spice pinout
	     <br> sim: time domain sim and SINE() source

<tr>
	<td> 12_bjt_amp_ac
	<td> <a href="raw/12_bjt_amp_ac.html"> yes </a>
	<td> <a href="raw/12_bjt_amp_ac.html"> yes </a>
	<td> <a href="sim/12_bjt_amp_ac/index.html"> yes </a>
	<td>      sch-rnd: spice pinout with portmap
	     <br> sim: ac analysis


<tr>
	<td> 16_opamp_dc
	<td> <a href="raw/16_opamp_dc.html"> yes </a>
	<td> error
	<td> <a href="sim/16_opamp_dc/index.html"> yes </a>
	<td> sim: dc sweep

<tr>
	<td> 18_opamp_ac
	<td> <a href="raw/18_opamp_ac.html"> yes </a>
	<td> error
	<td> <a href="sim/18_opamp_ac/index.html"> yes </a>
	<td> sch-rnd: 2-slot opamp

<tr>
	<td> 22_custom_sym
	<td> <a href="raw/22_custom_sym.html"> yes </a>
	<td> <a href="raw/22_custom_sym.html"> yes </a>
	<td> <a href="sim/22_custom_sym/index.html"> yes </a>
	<td> sch-rnd: symbol inline model card, spice/prefix

<tr>
	<td> 30_mixed
	<td> <a href="raw/30_mixed.html"> yes </a>
	<td> N/S
	<td> TODO
	<td>      sch-rnd: ADC/DAC bridging from terminal attributes
	     <br> sim: mixed analog+digital
</table>
<p>
Legend:
<ul>
	<li> error: supported in theory, but the simulation fails with an error
	<li> N/S: not supported
	<li> TODO: sch-rnd code missing or example not yet developed
</ul>
