ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=LnNTcUkV1CIzG7F2EXUAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAV; src_uuid=iNOQfJpO6hT/HFDFGjoAAABx;
				x=12000; y=72000; rot=270.000000; mirx=1;
				li:objects {
					ha:arc.1 { cx=10000; cy=0; r=6000; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:arc.2 { cx=8000; cy=0; r=2000; sang=0.000000; dang=180.000000; stroke=sym-decor; }
					ha:arc.3 { cx=12000; cy=0; r=2000; sang=180.000000; dang=180.000000; stroke=sym-decor; }
					ha:group.4 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAW; src_uuid=iNOQfJpO6hT/HFDFGjoAAABy;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
						}
					}
					ha:group.5 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAX; src_uuid=iNOQfJpO6hT/HFDFGjoAAABz;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
						}
					}
					ha:text.6 { x1=8000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=V1
					li:portmap {
						{P->spice/pinnum=1}
						{N->spice/pinnum=2}
					}
					role=symbol
					spice/params=dc 0 ac 1
					spice/prefix=V
				}
			}
			ha:group.3 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAY;
				x=-32000; y=-36000;
				li:objects {
					ha:line.1 { x1=36000; y1=116000; x2=52000; y2=116000; stroke=wire; }
					ha:line.2 { x1=44000; y1=108000; x2=44000; y2=116000; stroke=wire; }
					ha:line.3 { x1=44000; y1=116000; x2=44000; y2=116000; stroke=junction; }
					ha:text.4 { x1=36000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.6 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAZ;
				x=-28000; y=-36000;
				li:objects {
					ha:line.1 { x1=40000; y1=88000; x2=40000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.8 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAg; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=20000; y=80000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R1
					role=symbol
					spice/prefix=R
					value=10k
				}
			}
			ha:group.10 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAp; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=48000; y=72000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAAr; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=C1
					role=symbol
					spice/prefix=C
					value=1u
				}
			}
			ha:group.11 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAs;
				x=-32000; y=-36000;
				li:objects {
					ha:line.2 { x1=80000; y1=116000; x2=80000; y2=108000; stroke=wire; }
					ha:line.3 { x1=72000; y1=116000; x2=88000; y2=116000; stroke=wire; }
					ha:line.4 { x1=80000; y1=116000; x2=80000; y2=116000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.14 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAAz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=56000; y=80000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA1; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=R2
					role=symbol
					spice/prefix=R
					value=1k
				}
			}
			ha:group.16 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA2; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAh;
				x=84000; y=72000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA3; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAi;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=LnNTcUkV1CIzG7F2EXUAAAA4; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAj;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=C2
					role=symbol
					spice/prefix=C
					value=100n
				}
			}
			ha:group.17 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA5;
				x=-32000; y=-36000;
				li:objects {
					ha:line.2 { x1=116000; y1=116000; x2=116000; y2=108000; stroke=wire; }
					ha:line.3 { x1=108000; y1=116000; x2=132000; y2=116000; stroke=wire; }
					ha:line.4 { x1=116000; y1=116000; x2=116000; y2=116000; stroke=junction; }
					ha:text.5 { x1=128000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=out
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.20 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA6;
				x=-32000; y=-36000;
				li:objects {
					ha:line.1 { x1=80000; y1=88000; x2=80000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.22 {
				uuid=LnNTcUkV1CIzG7F2EXUAAAA7;
				x=-32000; y=-36000;
				li:objects {
					ha:line.1 { x1=116000; y1=88000; x2=116000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.24 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=12000; y=44000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.26 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABC; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=48000; y=44000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABD; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.28 {
				uuid=LnNTcUkV1CIzG7F2EXUAAABE; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=84000; y=44000;
				li:objects {
					ha:group.1 {
						uuid=LnNTcUkV1CIzG7F2EXUAAABF; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.33 {
				uuid=F3n6uHwVbWw4HTjelNgAAAAf; src_uuid=TeGEOMuew6iCb2kzckAAAAAD;
				x=4000; y=100000;
				li:objects {
					ha:text.1 { x1=2000; y1=-4000; dyntext=0; stroke=sym-decor; text=raw spice; }
					ha:text.2 { x1=2000; y1=-8000; dyntext=0; stroke=sym-decor; text=command; }
					ha:polygon.3 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=-10000; }
							ha:line { x1=0; y1=-10000; x2=12000; y2=-10000; }
							ha:line { x1=12000; y1=-10000; x2=12000; y2=0; }
							ha:line { x1=12000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
				}
				ha:attrib {
					-sym-comment={ Fill in spice/command and use export_spice (e.g. the spice_raw view) to get that string exported at the end of the spice netlist file. }
					-sym-copyright=(C) 2023 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					role=symbol
					spice/command={print ac vdb(out) zp(out)
op
ac dec 10 1 1000k > plot.txt
}
				}
			}
			ha:connection.34 {
				li:conn {
					/2/3/2
					/2/2/5/1
				}
			}
			ha:connection.35 {
				li:conn {
					/2/6/1
					/2/2/4/1
				}
			}
			ha:connection.36 {
				li:conn {
					/2/8/2/1
					/2/3/1
				}
			}
			ha:connection.37 {
				li:conn {
					/2/11/2
					/2/10/2/1
				}
			}
			ha:connection.39 {
				li:conn {
					/2/11/3
					/2/8/1/1
				}
			}
			ha:connection.40 {
				li:conn {
					/2/14/2/1
					/2/11/3
				}
			}
			ha:connection.41 {
				li:conn {
					/2/17/2
					/2/16/2/1
				}
			}
			ha:connection.43 {
				li:conn {
					/2/17/3
					/2/14/1/1
				}
			}
			ha:connection.44 {
				li:conn {
					/2/20/1
					/2/10/1/1
				}
			}
			ha:connection.45 {
				li:conn {
					/2/22/1
					/2/16/1/1
				}
			}
			ha:connection.46 {
				li:conn {
					/2/24/1/1
					/2/6/1
				}
			}
			ha:connection.47 {
				li:conn {
					/2/26/1/1
					/2/20/1
				}
			}
			ha:connection.48 {
				li:conn {
					/2/28/1/1
					/2/22/1
				}
			}
		}
		ha:attrib {
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={SIM raw spice: passives, ac (for gnucap)}
		}
	}
}
