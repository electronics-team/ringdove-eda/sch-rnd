ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAV;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAW; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
			ha:group.2 {
				uuid=y4qxH+ae2qngQHst1jcAAAA1;
				li:objects {
					ha:group.1 {
						uuid=y4qxH+ae2qngQHst1jcAAAA2; loclib_name=lm358;
						li:objects {
						}
						ha:attrib {
							spice/model_card={* lm358 - low power opamp model (single slot)
*
* (C) 2023 Tibor 'Igor2' Palinkas
* License: CC0 (no rights reserved): https://creativecommons.org/publicdomain/zero/1.0/
* Source: from ST's datasheet: https://archive.org/details/st-ts321
* (st321 is reasonably close to lm358 for simple simulation cases; see
* warnings on page 7)
*
*
** CONNECTIONS:
* 1 inverting input
* 2 non-inverting INPUT
* 3 output
* 4 positive power supply
* 5 negative power supply
.SUBCKT LM358 1 2 3 4 5

.MODEL MDTH D IS=1E-8 KF=3.104131E-15 CJO=10F

* INPUT STAGE
CIP 2 5 1.000000E-12
CIN 1 5 1.000000E-12
EIP 10 5 2 5 1
EIN 16 5 1 5 1
RIP 10 11 2.600000E+01
RIN 15 16 2.600000E+01
RIS 11 15 2.003862E+02
DIP 11 12 MDTH 400E-12
DIN 15 14 MDTH 400E-12
VOFP 12 13 DC 0
VOFN 13 14 DC 0
IPOL 13 5 1.000000E-05
CPS 11 15 3.783376E-09
DINN 17 13 MDTH 400E-12
VIN 17 5 0.000000e+00
DINR 15 18 MDTH 400E-12
VIP 4 18 2.000000E+00
FCP 4 5 VOFP 3.400000E+01
FCN 5 4 VOFN 3.400000E+01
FIBP 2 5 VOFN 2.000000E-03
FIBN 5 1 VOFP 2.000000E-03

* AMPLIFYING STAGE
FIP 5 19 VOFP 3.600000E+02
FIN 5 19 VOFN 3.600000E+02
RG1 19 5 3.652997E+06
RG2 19 4 3.652997E+06
CC 19 5 6.000000E-09
DOPM 19 22 MDTH 400E-12
DONM 21 19 MDTH 400E-12
HOPM 22 28 VOUT 7.500000E+03
VIPM 28 4 1.500000E+02
HONM 21 27 VOUT 7.500000E+03
VINM 5 27 1.500000E+02
EOUT 26 23 19 5 1
VOUT 23 5 0
ROUT 26 3 20
COUT 3 5 1.000000E-12
DOP 19 25 MDTH 400E-12
VOP 4 25 2.242230E+00
DON 24 19 MDTH 400E-12
VON 24 5 7.922301E-01
.ENDS
}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=spicelib; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=Hif/m8o2mo/CrYnTszoAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAP; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=80000; y=116000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAQ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAR; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAS; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAT; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAU; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					-slot=1
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=U1
					role=symbol
					spice/model=lm358
				}
			}
			ha:group.3 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAd; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=24000; y=112000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAe; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAf; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R1
					role=symbol
					value=1k
				}
			}
			ha:group.4 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAg;
				x=-20000; y=0;
				li:objects {
					ha:line.1 { x1=64000; y1=112000; x2=76000; y2=112000; stroke=wire; }
					ha:line.2 { x1=72000; y1=112000; x2=72000; y2=84000; stroke=wire; }
					ha:line.3 { x1=72000; y1=112000; x2=72000; y2=112000; stroke=junction; }
					ha:line.4 { x1=72000; y1=84000; x2=84000; y2=84000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.5 {
				li:conn {
					/2/4/1
					/2/2/2/1
				}
			}
			ha:group.7 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAk; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=64000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAl; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAm; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R2
					role=symbol
					value=100k
				}
			}
			ha:group.9 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAn;
				x=-20000; y=0;
				li:objects {
					ha:line.1 { x1=104000; y1=84000; x2=112000; y2=84000; stroke=wire; }
					ha:line.2 { x1=112000; y1=84000; x2=112000; y2=116000; stroke=wire; }
					ha:line.3 { x1=100000; y1=116000; x2=116000; y2=116000; stroke=wire; }
					ha:line.4 { x1=112000; y1=116000; x2=112000; y2=116000; stroke=junction; }
					ha:text.5 { x1=112000; y1=116000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=out
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.12 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAo;
				x=-12000; y=0;
				li:objects {
					ha:line.1 { x1=36000; y1=112000; x2=24000; y2=112000; stroke=wire; }
					ha:text.2 { x1=28000; y1=112000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
					ha:line.3 { x1=24000; y1=104000; x2=24000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.14 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAt; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=48000; y=120000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAAu; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.15 {
				uuid=Hif/m8o2mo/CrYnTszoAAAAv;
				x=-20000; y=0;
				li:objects {
					ha:line.1 { x1=68000; y1=120000; x2=76000; y2=120000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.16 {
				li:conn {
					/2/15/1
					/2/2/1/1
				}
			}
			ha:group.18 {
				uuid=Hif/m8o2mo/CrYnTszoAAAA8; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=68000; y=128000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAA9; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.19 {
				uuid=Hif/m8o2mo/CrYnTszoAAAA+;
				x=-20000; y=0;
				li:objects {
					ha:line.1 { x1=88000; y1=124000; x2=88000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.22 {
				uuid=Hif/m8o2mo/CrYnTszoAAABH; src_uuid=iNOQfJpO6hT/HFDFGjoAAABv;
				x=68000; y=104000; rot=180.000000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAABI; src_uuid=iNOQfJpO6hT/HFDFGjoAAABw;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=6000; y1=7000; x2=18000; y2=10000; rot=180.000000; halign=center; dyntext=1; stroke=sym-primary; text=%../A.rail%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:forge {
						delete,forge/tmp
						scalar,forge/tmp
						{sub,^,1:,forge/tmp}
						suba,$,rail,forge/tmp
						array,connect
						append,connect,forge/tmp
					}
					rail=Vneg
					role=symbol
				}
			}
			ha:group.23 {
				uuid=Hif/m8o2mo/CrYnTszoAAABJ;
				x=-20000; y=0;
				li:objects {
					ha:line.1 { x1=88000; y1=104000; x2=88000; y2=108000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.28 {
				uuid=Hif/m8o2mo/CrYnTszoAAABh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=12000; y=80000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAABi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.29 {
				uuid=Hif/m8o2mo/CrYnTszoAAABj;
				x=-12000; y=0;
				li:objects {
					ha:line.1 { x1=24000; y1=80000; x2=24000; y2=84000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.33 {
				uuid=Hif/m8o2mo/CrYnTszoAAABs; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=56000; y=68000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAABt; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.34 {
				uuid=Hif/m8o2mo/CrYnTszoAAABw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=56000; y=40000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAABx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.35 {
				uuid=Hif/m8o2mo/CrYnTszoAAABy;
				x=-104000; y=-28000;
				li:objects {
					ha:line.1 { x1=160000; y1=92000; x2=160000; y2=96000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.38 {
				uuid=Hif/m8o2mo/CrYnTszoAAABz;
				x=-104000; y=-28000;
				li:objects {
					ha:line.1 { x1=160000; y1=68000; x2=160000; y2=72000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.42 {
				uuid=Hif/m8o2mo/CrYnTszoAAAB8; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=92000; y=68000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAAB9; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.43 {
				uuid=Hif/m8o2mo/CrYnTszoAAAB+;
				x=-104000; y=-28000;
				li:objects {
					ha:line.1 { x1=184000; y1=92000; x2=184000; y2=104000; stroke=wire; }
					ha:line.2 { x1=184000; y1=104000; x2=196000; y2=104000; stroke=wire; }
					ha:line.3 { x1=196000; y1=104000; x2=196000; y2=96000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.46 {
				uuid=Hif/m8o2mo/CrYnTszoAAACB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABv;
				x=80000; y=40000; rot=180.000000;
				li:objects {
					ha:group.1 {
						uuid=Hif/m8o2mo/CrYnTszoAAACC; src_uuid=iNOQfJpO6hT/HFDFGjoAAABw;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=6000; y1=7000; x2=18000; y2=10000; rot=180.000000; halign=center; dyntext=1; stroke=sym-primary; text=%../A.rail%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:forge {
						delete,forge/tmp
						scalar,forge/tmp
						{sub,^,1:,forge/tmp}
						suba,$,rail,forge/tmp
						array,connect
						append,connect,forge/tmp
					}
					rail=Vneg
					role=symbol
				}
			}
			ha:group.47 {
				uuid=Hif/m8o2mo/CrYnTszoAAACD;
				x=-104000; y=-28000;
				li:objects {
					ha:line.1 { x1=184000; y1=72000; x2=184000; y2=68000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.50 {
				uuid=guptF1eHUCXR7MoVgJUAAAA2; src_uuid=TeGEOMuew6iCb2kzckAAAAAD;
				x=8000; y=56000;
				li:objects {
					ha:text.1 { x1=2000; y1=-4000; dyntext=0; stroke=sym-decor; text=raw spice; }
					ha:text.2 { x1=2000; y1=-8000; dyntext=0; stroke=sym-decor; text=command; }
					ha:polygon.3 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=-10000; }
							ha:line { x1=0; y1=-10000; x2=12000; y2=-10000; }
							ha:line { x1=12000; y1=-10000; x2=12000; y2=0; }
							ha:line { x1=12000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
				}
				ha:attrib {
					-sym-comment={ Fill in spice/command and use export_spice (e.g. the spice_raw view) to get that string exported at the end of the spice netlist file. }
					-sym-copyright=(C) 2023 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					role=symbol
					spice/command={ dc V1 -50m 60m 2m
plot v(in) v(out)}
				}
			}
			ha:group.51 {
				uuid=XFxbV/afs+qvJqWppTgAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB0;
				x=12000; y=104000; rot=270.000000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=XFxbV/afs+qvJqWppTgAAABC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB1;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=XFxbV/afs+qvJqWppTgAAABD; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB2;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:arc.3 { cx=10000; cy=0; r=6000; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.4 { x1=7000; y1=2000; x2=7000; y2=-2000; stroke=sym-decor; }
					ha:line.5 { x1=5000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=0; x2=15000; y2=0; stroke=sym-decor; }
					ha:text.7 { x1=0; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.8 { x1=0; y1=4000; dyntext=1; stroke=sym-secondary; text=%../A.spice/params%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=V1
					role=symbol
					spice/params=dc 0
				}
			}
			ha:group.54 {
				uuid=XFxbV/afs+qvJqWppTgAAABE; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB0;
				x=56000; y=64000; rot=270.000000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=XFxbV/afs+qvJqWppTgAAABF; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB1;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=XFxbV/afs+qvJqWppTgAAABG; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB2;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:arc.3 { cx=10000; cy=0; r=6000; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.4 { x1=7000; y1=2000; x2=7000; y2=-2000; stroke=sym-decor; }
					ha:line.5 { x1=5000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=0; x2=15000; y2=0; stroke=sym-decor; }
					ha:text.7 { x1=0; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.8 { x1=0; y1=4000; dyntext=1; stroke=sym-secondary; text=%../A.spice/params%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=V2
					role=symbol
					spice/params=dc 5
				}
			}
			ha:group.57 {
				uuid=XFxbV/afs+qvJqWppTgAAABH; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB0;
				x=80000; y=64000; rot=270.000000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=XFxbV/afs+qvJqWppTgAAABI; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB1;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=N
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=XFxbV/afs+qvJqWppTgAAABJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB2;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=P
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:arc.3 { cx=10000; cy=0; r=6000; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.4 { x1=7000; y1=2000; x2=7000; y2=-2000; stroke=sym-decor; }
					ha:line.5 { x1=5000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=0; x2=15000; y2=0; stroke=sym-decor; }
					ha:text.7 { x1=0; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.8 { x1=0; y1=4000; dyntext=1; stroke=sym-secondary; text=%../A.spice/params%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=V3
					role=symbol
					spice/params=dc 5
				}
			}
			ha:connection.89 {
				li:conn {
					/2/29/1
					/2/28/1/1
				}
			}
			ha:connection.90 {
				li:conn {
					/2/35/1
					/2/33/1/1
				}
			}
			ha:connection.91 {
				li:conn {
					/2/38/1
					/2/34/1/1
				}
			}
			ha:connection.93 {
				li:conn {
					/2/43/3
					/2/42/1/1
				}
			}
			ha:connection.94 {
				li:conn {
					/2/47/1
					/2/46/1/1
				}
			}
			ha:connection.95 {
				li:conn {
					/2/51/1/1
					/2/29/1
				}
			}
			ha:connection.96 {
				li:conn {
					/2/51/2/1
					/2/12/3
				}
			}
			ha:connection.97 {
				li:conn {
					/2/54/1/1
					/2/38/1
				}
			}
			ha:connection.98 {
				li:conn {
					/2/54/2/1
					/2/35/1
				}
			}
			ha:connection.99 {
				li:conn {
					/2/57/1/1
					/2/47/1
				}
			}
			ha:connection.100 {
				li:conn {
					/2/57/2/1
					/2/43/1
				}
			}
			ha:connection.102 {
				li:conn {
					/2/3/2/1
					/2/12/1
				}
			}
			ha:connection.103 {
				li:conn {
					/2/4/1
					/2/3/1/1
				}
			}
			ha:connection.104 {
				li:conn {
					/2/7/2/1
					/2/4/4
				}
			}
			ha:connection.105 {
				li:conn {
					/2/9/1
					/2/7/1/1
				}
			}
			ha:connection.106 {
				li:conn {
					/2/9/3
					/2/2/3/1
				}
			}
			ha:connection.107 {
				li:conn {
					/2/15/1
					/2/14/1/1
				}
			}
			ha:connection.108 {
				li:conn {
					/2/19/1
					/2/18/1/1
				}
			}
			ha:connection.109 {
				li:conn {
					/2/19/1
					/2/2/11/1
				}
			}
			ha:connection.110 {
				li:conn {
					/2/23/1
					/2/2/10/1
				}
			}
			ha:connection.111 {
				li:conn {
					/2/23/1
					/2/22/1/1
				}
			}
		}
		ha:attrib {
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title={SIM raw spice: ompamp circuit, dc gain}
		}
	}
}
