ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=OOg+ImBfqQSX8Difwh8AAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=88000; y=136000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAK; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAL; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
					ha:text.6 { x1=16000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../A.footprint%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R1
					role=symbol
					value=1k
				}
			}
			ha:group.3 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAM; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=88000; y=108000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAN; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAO; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
					ha:text.6 { x1=16000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../A.footprint%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					footprint=1206
					name=R2
					role=symbol
					value=4k7
				}
			}
			ha:group.4 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAT; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=88000; y=140000;
				li:objects {
					ha:group.1 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAU; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.5 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=88000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAa; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.6 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAb;
				li:objects {
					ha:line.1 { x1=88000; y1=140000; x2=88000; y2=136000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.7 {
				li:conn {
					/2/6/1
					/2/2/2/1
				}
			}
			ha:connection.8 {
				li:conn {
					/2/6/1
					/2/4/1/1
				}
			}
			ha:group.9 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAc;
				li:objects {
					ha:line.2 { x1=72000; y1=112000; x2=88000; y2=112000; stroke=wire; }
					ha:line.4 { x1=88000; y1=116000; x2=88000; y2=108000; stroke=wire; }
					ha:line.5 { x1=88000; y1=112000; x2=88000; y2=112000; stroke=junction; }
					ha:text.6 { x1=80000; y1=112000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=div
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.12 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAd;
				li:objects {
					ha:line.1 { x1=88000; y1=88000; x2=88000; y2=84000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.13 {
				li:conn {
					/2/12/1
					/2/3/1/1
				}
			}
			ha:group.15 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAm; src_uuid=OOg+ImBfqQSX8Difwh8AAAAi;
				x=68000; y=108000; mirx=1;
				li:objects {
					ha:text.1 { x1=8000; y1=0; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAn; src_uuid=OOg+ImBfqQSX8Difwh8AAAAj;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAo; src_uuid=OOg+ImBfqQSX8Difwh8AAAAk;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAp; src_uuid=OOg+ImBfqQSX8Difwh8AAAAl;
						x=0; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					footprint=connector(3,1)
					name=CN1
					role=symbol
				}
			}
			ha:connection.16 {
				li:conn {
					/2/12/1
					/2/5/1/1
				}
			}
			ha:connection.18 {
				li:conn {
					/2/15/3/1
					/2/9/2
				}
			}
			ha:group.19 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAs; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=76000; y=120000;
				li:objects {
					ha:group.1 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAt; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.20 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=76000; y=104000;
				li:objects {
					ha:group.1 {
						uuid=OOg+ImBfqQSX8Difwh8AAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.21 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAy;
				li:objects {
					ha:line.1 { x1=72000; y1=108000; x2=76000; y2=108000; stroke=wire; }
					ha:line.2 { x1=76000; y1=108000; x2=76000; y2=104000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.22 {
				li:conn {
					/2/21/1
					/2/15/2/1
				}
			}
			ha:connection.23 {
				li:conn {
					/2/21/2
					/2/20/1/1
				}
			}
			ha:group.24 {
				uuid=OOg+ImBfqQSX8Difwh8AAAAz;
				li:objects {
					ha:line.1 { x1=72000; y1=116000; x2=76000; y2=116000; stroke=wire; }
					ha:line.2 { x1=76000; y1=116000; x2=76000; y2=120000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.25 {
				li:conn {
					/2/24/1
					/2/15/4/1
				}
			}
			ha:connection.26 {
				li:conn {
					/2/24/2
					/2/19/1/1
				}
			}
			ha:connection.27 {
				li:conn {
					/2/9/4
					/2/2/1/1
				}
			}
			ha:connection.28 {
				li:conn {
					/2/9/4
					/2/3/2/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title=resistor divider plugin
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 1
     grid = 2.0480 mm
    }
   }
  }
}
