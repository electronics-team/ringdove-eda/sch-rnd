ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABC;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABD; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
					ha:group.2 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABE; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=iNOQfJpO6hT/HFDFGjoAAAEl;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEm;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.4 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAEz;
				li:objects {
					ha:line.3 { x1=96000; y1=84000; x2=96000; y2=60000; stroke=wire; }
					ha:line.4 { x1=96000; y1=60000; x2=56000; y2=60000; stroke=wire; }
					ha:line.5 { x1=56000; y1=60000; x2=56000; y2=80000; stroke=wire; }
					ha:line.8 { x1=56000; y1=80000; x2=56000; y2=80000; stroke=junction; }
					ha:line.9 { x1=88000; y1=84000; x2=100000; y2=84000; stroke=wire; }
					ha:line.10 { x1=96000; y1=84000; x2=96000; y2=84000; stroke=junction; }
					ha:line.11 { x1=24000; y1=80000; x2=64000; y2=80000; stroke=wire; }
					ha:text.12 { x1=38000; y1=80000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=in
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.10 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAE2;
				li:objects {
					ha:line.1 { x1=76000; y1=100000; x2=76000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.13 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAE3;
				x=76000; y=68000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAE4;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.14 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAE5;
				li:objects {
					ha:line.1 { x1=76000; y1=76000; x2=76000; y2=68000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.16 {
				li:conn {
					/2/14/1
					/2/13/1/1
				}
			}
			ha:group.17 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAE6;
				x=60000; y=88000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAE7;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.18 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAE8;
				li:objects {
					ha:line.1 { x1=64000; y1=88000; x2=60000; y2=88000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.20 {
				li:conn {
					/2/18/1
					/2/17/1/1
				}
			}
			ha:group.21 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAE9;
				x=100000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAE+;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAE/;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=6000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=9000; y1=5000; x2=9000; y2=-5000; stroke=sym-decor; }
					ha:line.6 { x1=11000; y1=5000; x2=11000; y2=-5000; stroke=sym-decor; }
					ha:line.7 { x1=4000; y1=0; x2=9000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=11000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					name=C1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=100n
				}
			}
			ha:connection.22 {
				li:conn {
					/2/4/9
					/2/21/2/1
				}
			}
			ha:group.23 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFA;
				x=128000; y=116000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFB;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFC;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=R1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=470k
				}
			}
			ha:group.27 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFD;
				x=128000; y=76000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFE;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFF;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=R2
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
					value=470k
				}
			}
			ha:group.29 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFG;
				x=128000; y=52000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFH;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.30 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFI;
				li:objects {
					ha:line.1 { x1=128000; y1=56000; x2=128000; y2=52000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.31 {
				li:conn {
					/2/30/1
					/2/27/1/1
				}
			}
			ha:connection.32 {
				li:conn {
					/2/30/1
					/2/29/1/1
				}
			}
			ha:group.36 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFJ;
				x=140000; y=88000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFK;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.38 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFL;
				li:objects {
					ha:line.1 { x1=140000; y1=88000; x2=144000; y2=88000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.40 {
				li:conn {
					/2/38/1
					/2/36/1/1
				}
			}
			ha:group.60 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFO;
				li:objects {
					ha:line.1 { x1=128000; y1=124000; x2=128000; y2=116000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.61 {
				li:conn {
					/2/60/1
					/2/23/2/1
				}
			}
			ha:group.63 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFP;
				x=196000; y=80000;
				li:objects {
					ha:text.1 { x1=10000; y1=-4000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFQ;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFR;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFS;
						x=0; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN2
					role=symbol
				}
			}
			ha:group.67 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFW;
				li:objects {
					ha:line.1 { x1=140000; y1=60000; x2=140000; y2=80000; stroke=wire; }
					ha:line.2 { x1=176000; y1=64000; x2=176000; y2=84000; stroke=wire; }
					ha:line.3 { x1=128000; y1=84000; x2=128000; y2=84000; stroke=junction; }
					ha:line.4 { x1=140000; y1=60000; x2=176000; y2=60000; stroke=wire; }
					ha:line.5 { x1=140000; y1=80000; x2=140000; y2=80000; stroke=junction; }
					ha:line.6 { x1=128000; y1=80000; x2=128000; y2=80000; stroke=junction; }
					ha:line.7 { x1=144000; y1=80000; x2=128000; y2=80000; stroke=wire; }
					ha:line.8 { x1=176000; y1=84000; x2=192000; y2=84000; stroke=wire; }
					ha:line.9 { x1=176000; y1=60000; x2=176000; y2=64000; stroke=wire; }
					ha:line.10 { x1=176000; y1=84000; x2=176000; y2=84000; stroke=junction; }
					ha:line.11 { x1=128000; y1=76000; x2=128000; y2=96000; stroke=wire; }
					ha:line.12 { x1=176000; y1=84000; x2=168000; y2=84000; stroke=wire; }
					ha:line.13 { x1=120000; y1=84000; x2=128000; y2=84000; stroke=wire; }
					ha:text.14 { x1=178000; y1=84000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
					ha:text.15 { x1=124000; y1=84000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=mid
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.69 {
				li:conn {
					/2/67/8
					/2/63/3/1
				}
			}
			ha:connection.70 {
				li:conn {
					/2/67/11
					/2/23/1/1
				}
			}
			ha:connection.71 {
				li:conn {
					/2/67/11
					/2/27/2/1
				}
			}
			ha:connection.73 {
				li:conn {
					/2/67/13
					/2/21/1/1
				}
			}
			ha:group.75 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFX;
				x=188000; y=76000;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFY;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:group.77 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFb;
				li:objects {
					ha:line.1 { x1=192000; y1=80000; x2=188000; y2=80000; stroke=wire; }
					ha:line.2 { x1=188000; y1=80000; x2=188000; y2=76000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.78 {
				li:conn {
					/2/77/1
					/2/63/2/1
				}
			}
			ha:connection.79 {
				li:conn {
					/2/77/2
					/2/75/1/1
				}
			}
			ha:group.80 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFc;
				li:objects {
					ha:line.1 { x1=192000; y1=88000; x2=188000; y2=88000; stroke=wire; }
					ha:line.2 { x1=188000; y1=88000; x2=188000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.81 {
				li:conn {
					/2/80/1
					/2/63/4/1
				}
			}
			ha:line.93 { x1=24000; y1=84000; x2=28000; y2=84000; stroke=wire; }
			ha:group.95 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFf;
				x=20000; y=76000; mirx=1;
				li:objects {
					ha:text.1 { x1=10000; y1=-4000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFg;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFh;
						x=0; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFi;
						x=0; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; floater=1; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=10000; }
							ha:line { x1=0; y1=10000; x2=4000; y2=10000; }
							ha:line { x1=4000; y1=10000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN1
					role=symbol
				}
			}
			ha:group.98 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFj;
				x=28000; y=72000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=iNOQfJpO6hT/HFDFGjoAAAFk;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					li:connect {
						{1:GND}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.100 {
				li:conn {
					/2/4/11
					/2/95/3/1
				}
			}
			ha:group.101 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFl;
				li:objects {
					ha:line.1 { x1=28000; y1=84000; x2=28000; y2=88000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.103 {
				uuid=iNOQfJpO6hT/HFDFGjoAAAFm;
				li:objects {
					ha:line.1 { x1=24000; y1=76000; x2=28000; y2=76000; stroke=wire; }
					ha:line.2 { x1=28000; y1=76000; x2=28000; y2=72000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.104 {
				li:conn {
					/2/103/1
					/2/95/2/1
				}
			}
			ha:connection.105 {
				li:conn {
					/2/103/2
					/2/98/1/1
				}
			}
			ha:group.106 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABR; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=88000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABS; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABT; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABU; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABV; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABW; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.13 { x1=-21000; y1=12000; dyntext=1; stroke=sym-secondary; text=%../A.-slot%; floater=1; }
				}
				ha:attrib {
					-slot=1
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=U1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.107 {
				li:conn {
					/2/106/1/1
					/2/18/1
				}
			}
			ha:connection.108 {
				li:conn {
					/2/106/2/1
					/2/4/11
				}
			}
			ha:connection.109 {
				li:conn {
					/2/106/3/1
					/2/4/9
				}
			}
			ha:connection.110 {
				li:conn {
					/2/106/10/1
					/2/14/1
				}
			}
			ha:connection.111 {
				li:conn {
					/2/106/11/1
					/2/10/1
				}
			}
			ha:group.112 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABX; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAH;
				x=168000; y=84000;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABY; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=-20000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in+
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=-20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in-
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABa; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=-20000; y1=-8000; x2=-20000; y2=8000; stroke=sym-decor; }
					ha:line.5 { x1=-20000; y1=8000; x2=-4000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-4000; y1=0; x2=-20000; y2=-8000; stroke=sym-decor; }
					ha:line.7 { x1=-18000; y1=5000; x2=-18000; y2=3000; stroke=sym-decor; }
					ha:line.8 { x1=-19000; y1=4000; x2=-17000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=-19000; y1=-4000; x2=-17000; y2=-4000; stroke=sym-decor; }
					ha:group.10 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABb; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=-12000; y=-4000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V-
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABc; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=-12000; y=8000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=V+
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:text.12 { x1=-21000; y1=9000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.13 { x1=-21000; y1=12000; dyntext=1; stroke=sym-secondary; text=%../A.-slot%; floater=1; }
				}
				ha:attrib {
					-slot=2
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=lm358_so8
					name=U1
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.113 {
				li:conn {
					/2/112/1/1
					/2/38/1
				}
			}
			ha:connection.114 {
				li:conn {
					/2/112/2/1
					/2/67/7
				}
			}
			ha:connection.115 {
				li:conn {
					/2/112/3/1
					/2/67/12
				}
			}
			ha:group.116 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABh; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=28000; y=88000;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABi; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:connection.117 {
				li:conn {
					/2/116/1/1
					/2/101/1
				}
			}
			ha:group.118 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABj; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=76000; y=100000;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABk; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.119 {
				li:conn {
					/2/118/1/1
					/2/10/1
				}
			}
			ha:group.120 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABl; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=128000; y=124000;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABm; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.121 {
				li:conn {
					/2/120/1/1
					/2/60/1
				}
			}
			ha:group.122 {
				uuid=bOwQFqA7m2jl+cvU6t4AAABn; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=188000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=bOwQFqA7m2jl+cvU6t4AAABo; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
					sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					sym-license-dist=GPLv2+
					sym-license-use=Public Domain
					sym-source=sch-rnd default symbol lib
				}
			}
			ha:connection.123 {
				li:conn {
					/2/122/1/1
					/2/80/2
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1 of 1
			print_page=A/4
			title=Slot + devmap example
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 1
     grid = 2.0480 mm
    }
   }
  }
}
