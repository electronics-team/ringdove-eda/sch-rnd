ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAP;
				li:objects {
					ha:group.1 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAQ; loclib_name=2n7002_sot23;
						li:objects {
						}
						ha:attrib {
							device=2n7002
							footprint=SOT23
							li:portmap {
								{G->pcb/pinnum=1}
								{S->pcb/pinnum=2}
								{D->pcb/pinnum=3}
							}
						}
					}
					ha:group.2 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAj; loclib_name=led5;
						li:objects {
						}
						ha:attrib {
							device=led5
							footprint=LED5
							li:portmap {
								{C->pcb/pinnum=1}
								{A->pcb/pinnum=2}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=mPJMPTQBLMqdGSJWpRwAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAL; src_uuid=iNOQfJpO6hT/HFDFGjoAAACC;
				x=96000; y=104000;
				li:objects {
					ha:group.1 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAM; src_uuid=iNOQfJpO6hT/HFDFGjoAAACD;
						x=12000; y=12000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=D
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAN; src_uuid=iNOQfJpO6hT/HFDFGjoAAACE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=G
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:text.3 { x1=8000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.5 { cx=11000; cy=3000; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=9000; y1=-1000; x2=9000; y2=1000; stroke=sym-decor; }
					ha:line.8 { x1=9000; y1=2000; x2=9000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=9000; y1=5000; x2=9000; y2=7000; stroke=sym-decor; }
					ha:line.10 { x1=9000; y1=3000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.11 { x1=9000; y1=0; x2=12000; y2=0; stroke=sym-decor; }
					ha:line.12 { x1=9000; y1=6000; x2=12000; y2=6000; stroke=sym-decor; }
					ha:line.13 { x1=12000; y1=6000; x2=12000; y2=8000; stroke=sym-decor; }
					ha:line.14 { x1=12000; y1=-4000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.15 { x1=13000; y1=3000; x2=15000; y2=3000; stroke=sym-decor; }
					ha:line.16 { x1=15000; y1=3000; x2=14000; y2=4000; stroke=sym-decor; }
					ha:line.17 { x1=14000; y1=4000; x2=13000; y2=3000; stroke=sym-decor; }
					ha:line.18 { x1=13000; y1=4000; x2=15000; y2=4000; stroke=sym-decor; }
					ha:line.19 { x1=12000; y1=7000; x2=14000; y2=7000; stroke=sym-decor; }
					ha:line.20 { x1=14000; y1=-1000; x2=12000; y2=-1000; stroke=sym-decor; }
					ha:line.21 { x1=8000; y1=7000; x2=8000; y2=0; stroke=sym-decor; }
					ha:polygon.22 {
						li:outline {
							ha:line { x1=10000; y1=4000; x2=9000; y2=3000; }
							ha:line { x1=9000; y1=3000; x2=10000; y2=2000; }
							ha:line { x1=10000; y1=2000; x2=10000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.23 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAO; src_uuid=iNOQfJpO6hT/HFDFGjoAAACF;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=S
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.24 { x1=14000; y1=-1000; x2=14000; y2=3000; stroke=sym-decor; }
					ha:line.25 { x1=14000; y1=4000; x2=14000; y2=7000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=2n7002_sot23
					name=./Q1
					role=symbol
					ha:spice/prefix = { value=M; prio=31050; }
				}
			}
			ha:group.3 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAX; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=108000; y=168000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAY; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAZ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					footprint=1206
					name=./R1
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=510
				}
			}
			ha:group.4 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAg; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAQ;
				x=108000; y=124000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAh; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAR;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAi; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAS;
						x=-16000; y=0; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:line.3 { x1=-4000; y1=0; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.4 { x1=-12000; y1=0; x2=-10000; y2=0; stroke=sym-decor; }
					ha:line.5 { x1=-10000; y1=4000; x2=-6000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=-6000; y1=0; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.7 { x1=-10000; y1=4000; x2=-10000; y2=-4000; stroke=sym-decor; }
					ha:line.8 { x1=-6000; y1=4000; x2=-6000; y2=-4000; stroke=sym-decor; }
					ha:text.9 { x1=-4000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.10 { x1=-8000; y1=13000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.11 { x1=-8000; y1=8000; x2=-6000; y2=11000; stroke=sym-decor; }
					ha:line.12 { x1=-6000; y1=11000; x2=-7000; y2=10000; stroke=sym-decor; }
					ha:line.13 { x1=-6000; y1=11000; x2=-6517; y2=9545; stroke=sym-decor; }
					ha:line.14 { x1=-10000; y1=7000; x2=-8000; y2=10000; stroke=sym-decor; }
					ha:line.15 { x1=-8000; y1=10000; x2=-8000; y2=8000; stroke=sym-decor; }
					ha:line.16 { x1=-8303; y1=6354; x2=-6303; y2=9354; stroke=sym-decor; }
					ha:line.17 { x1=-6303; y1=9354; x2=-7303; y2=8354; stroke=sym-decor; }
					ha:line.18 { x1=-6303; y1=9354; x2=-6820; y2=7899; stroke=sym-decor; }
					ha:line.19 { x1=-10303; y1=5354; x2=-8303; y2=8354; stroke=sym-decor; }
					ha:line.20 { x1=-8303; y1=8354; x2=-8303; y2=6354; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=led5
					name=./D1
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.5 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAk;
				li:objects {
					ha:line.1 { x1=108000; y1=116000; x2=108000; y2=124000; stroke=wire; }
					ha:text.2 { x1=110000; y1=120000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=./led_fet
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.6 {
				li:conn {
					/2/5/1
					/2/2/1/1
				}
			}
			ha:connection.7 {
				li:conn {
					/2/5/1
					/2/4/1/1
				}
			}
			ha:group.8 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAl;
				li:objects {
					ha:line.1 { x1=108000; y1=140000; x2=108000; y2=148000; stroke=wire; }
					ha:text.2 { x1=110000; y1=144000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=./res_led
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.9 {
				li:conn {
					/2/8/1
					/2/3/1/1
				}
			}
			ha:connection.10 {
				li:conn {
					/2/8/1
					/2/4/2/1
				}
			}
			ha:connection.12 {
				li:conn {
					/2/3/2/1
					/2/14/3
				}
			}
			ha:group.14 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAo;
				li:objects {
					ha:line.1 { x1=84000; y1=176000; x2=108000; y2=176000; stroke=wire; }
					ha:line.3 { x1=108000; y1=168000; x2=108000; y2=176000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.16 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAp;
				li:objects {
					ha:line.1 { x1=96000; y1=104000; x2=84000; y2=104000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.17 {
				li:conn {
					/2/16/1
					/2/2/2/1
				}
			}
			ha:group.20 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAs;
				li:objects {
					ha:line.1 { x1=108000; y1=96000; x2=108000; y2=88000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.21 {
				li:conn {
					/2/20/1
					/2/2/23/1
				}
			}
			ha:group.22 {
				uuid=mPJMPTQBLMqdGSJWpRwAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=108000; y=88000;
				li:objects {
					ha:group.1 {
						uuid=mPJMPTQBLMqdGSJWpRwAAAAy; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.23 {
				li:conn {
					/2/22/1/1
					/2/20/1
				}
			}
			ha:group.24 {
				uuid=2SRAr8t6oTchmYmlPFsAAAA9; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=84000; y=176000;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=Vin
					role=terminal
				}
			}
			ha:connection.25 {
				li:conn {
					/2/24/1
					/2/14/1
				}
			}
			ha:group.26 {
				uuid=2SRAr8t6oTchmYmlPFsAAAA/; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=84000; y=104000;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=ctrl
					role=terminal
				}
			}
			ha:connection.27 {
				li:conn {
					/2/26/1
					/2/16/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=2/2
			print_page=A/4
			title=LED and driver
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 1
     grid = 2.0480 mm
    }
   }
  }
}
