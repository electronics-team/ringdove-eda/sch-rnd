ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=Of4kyoIScCrE774KCa4AAABc;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAABd; loclib_name=attiny24.funcmap;
						li:objects {
						}
						ha:attrib {
							li:funcmap/ports {
								{ PB0/PB0      -> sigtype=digital; }
								{ PB0/PCINT8   -> sigtype=digital; dir=input }
								{ PB0/XTAL1    -> sigtype=analog;  }
								{ PB1/PB1      -> sigtype=digital; }
								{ PB1/PCINT9   -> sigtype=digital; dir=input  }
								{ PB1/XTAL2    -> sigtype=analog;  }
								{ PB3/PB3      -> sigtype=digital; }
								{ PB3/PCINT11  -> sigtype=digital; dir=input  }
								{ PB3/RESET    -> sigtype=digital; dir=input  }
								{ PB2/PB2      -> sigtype=digital; }
								{ PB2/PCINT10  -> sigtype=digital; dir=input  }
								{ PB2/OC0A     -> sigtype=digital; dir=output }
								{ PB2/INT0     -> sigtype=digital; dir=input  }
								{ PA7/PA7      -> sigtype=digital; }
								{ PA7/PCINT7   -> sigtype=digital; dir=input  }
								{ PA7/OC0B     -> sigtype=digital; dir=output }
								{ PA7/ADC7     -> sigtype=digital; dir=input  }
								{ PA6/PA6      -> sigtype=digital; }
								{ PA6/PCINT6   -> sigtype=digital; dir=input  }
								{ PA6/MOSI     -> sigtype=digital; }
								{ PA6/DI       -> sigtype=digital; dir=input  }
								{ PA6/SDA      -> sigtype=digital; }
								{ PA6/OC1A     -> sigtype=digital; dir=output }
								{ PA6/ADC6     -> sigtype=analog;  dir=input  }
								{ PA0/PA0      -> sigtype=digital; }
								{ PA0/PCINT0   -> sigtype=digital; dir=input  }
								{ PA0/AREF+ADC0-> sigtype=analog;  dir=input  }
								{ PA1/PA1      -> sigtype=digital; }
								{ PA1/PCINT1   -> sigtype=digital; dir=input  }
								{ PA1/AIN0     -> sigtype=analog;  dir=input  }
								{ PA1/ADC1     -> sigtype=analog;  dir=input  }
								{ PA2/PA2      -> sigtype=digital; }
								{ PA2/PCINT2   -> sigtype=digital; dir=input  }
								{ PA2/AIN1     -> sigtype=analog;  dir=input  }
								{ PA2/ADC2     -> sigtype=analog;  dir=input  }
								{ PA3/PA3      -> sigtype=digital; }
								{ PA3/PCINT3   -> sigtype=digital; dir=input  }
								{ PA3/ADC3     -> sigtype=analog;  dir=input  }
								{ PA4/PA4      -> sigtype=digital; }
								{ PA4/PCINT4   -> sigtype=digital; dir=input  }
								{ PA4/SCK      -> sigtype=digital; }
								{ PA4/SCL      -> sigtype=digital; }
								{ PA4/ADC4     -> sigtype=analog;  dir=input  }
								{ PA5/PA5      -> sigtype=digital; }
								{ PA5/PCINT5   -> sigtype=digital; dir=input  }
								{ PA5/MISO     -> sigtype=digital; }
								{ PA5/DO       -> sigtype=digital; dir=output }
								{ PA5/OC1B     -> sigtype=digital; dir=output }
								{ PA5/ADC5     -> sigtype=analog;  dir=input  }
							}
							li:funcmap/strong_groups {
								{ SPI          -> SCK, MISO, MOSI }
								{ I2C          -> SDA, SCL }
								{ USI          -> SCK, DI, DO }
							}
							li:funcmap/weak_groups {
								{ gpio         -> PB0, PB1, PB2, PB3, PA6, PA7, PA0, PA1, PA2, PA3, PA4, PA5 }
								{ pcint        -> PCINT8, PCINT9, PCINT11, PCINT10, PCINT7, PCINT6, PCINT0, PCINT1, PCINT2, PCINT3, PCINT4, PCINT5 }
								{ xtal         -> XTAL1, XTAL2 }
								{ PWM          -> OC0A, OC0B, OC1A, OC1B }
								{ adc          -> AREF+ADC0, ADC1, ADC2, ADC3, ADC4, ADC5, ADC6, ADC7 }
								{ comparator   -> AIN0, AIN1 }
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=funcmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=Of4kyoIScCrE774KCa4AAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.8 {
				uuid=Of4kyoIScCrE774KCa4AAAAK;
				x=136000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAAAL; src_uuid=Of4kyoIScCrE774KCa4AAAAD;
						x=-8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=ctrl
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Of4kyoIScCrE774KCa4AAAAM; src_uuid=Of4kyoIScCrE774KCa4AAAAD;
						x=0; y=8000; rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vin
							role=terminal
						}
					}
					ha:polygon.3 {
						li:outline {
							ha:line { x1=-8000; y1=8000; x2=-8000; y2=-8000; }
							ha:line { x1=-8000; y1=-8000; x2=8000; y2=-8000; }
							ha:line { x1=8000; y1=-8000; x2=8000; y2=8000; }
							ha:line { x1=8000; y1=8000; x2=-8000; y2=8000; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:text.4 { x1=-8000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.5 { x1=-1000; y1=-1000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=led
					name=S1
					role=symbol
				}
			}
			ha:group.9 {
				uuid=Of4kyoIScCrE774KCa4AAABN; src_uuid=+oSyprNFSw9F5MMrBkcAAAAP;
				x=68000; y=68000;
				li:objects {
					ha:text.1 { x1=-8000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=36000; }
							ha:line { x1=0; y1=36000; x2=24000; y2=36000; }
							ha:line { x1=24000; y1=36000; x2=24000; y2=0; }
							ha:line { x1=24000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=Of4kyoIScCrE774KCa4AAABO; src_uuid=+oSyprNFSw9F5MMrBkcAAAAB;
						x=0; y=24000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB0
							pinnum=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=Of4kyoIScCrE774KCa4AAABP; src_uuid=+oSyprNFSw9F5MMrBkcAAAAC;
						x=0; y=20000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB1
							pinnum=3
							role=terminal
						}
					}
					ha:group.5 {
						uuid=Of4kyoIScCrE774KCa4AAABQ; src_uuid=+oSyprNFSw9F5MMrBkcAAAAD;
						x=0; y=16000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB2
							pinnum=5
							role=terminal
						}
					}
					ha:group.6 {
						uuid=Of4kyoIScCrE774KCa4AAABR; src_uuid=+oSyprNFSw9F5MMrBkcAAAAE;
						x=0; y=12000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB3
							pinnum=4
							role=terminal
						}
					}
					ha:group.7 {
						uuid=Of4kyoIScCrE774KCa4AAABS; src_uuid=+oSyprNFSw9F5MMrBkcAAAAF;
						x=24000; y=32000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA0
							pinnum=13
							role=terminal
						}
					}
					ha:group.8 {
						uuid=Of4kyoIScCrE774KCa4AAABT; src_uuid=+oSyprNFSw9F5MMrBkcAAAAG;
						x=24000; y=28000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA1
							pinnum=12
							role=terminal
						}
					}
					ha:group.9 {
						uuid=Of4kyoIScCrE774KCa4AAABU; src_uuid=+oSyprNFSw9F5MMrBkcAAAAH;
						x=24000; y=24000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA2
							pinnum=11
							role=terminal
						}
					}
					ha:group.10 {
						uuid=Of4kyoIScCrE774KCa4AAABV; src_uuid=+oSyprNFSw9F5MMrBkcAAAAI;
						x=24000; y=20000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA3
							pinnum=10
							role=terminal
						}
					}
					ha:group.11 {
						uuid=Of4kyoIScCrE774KCa4AAABW; src_uuid=+oSyprNFSw9F5MMrBkcAAAAJ;
						x=24000; y=16000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA4
							pinnum=9
							role=terminal
						}
					}
					ha:group.12 {
						uuid=Of4kyoIScCrE774KCa4AAABX; src_uuid=+oSyprNFSw9F5MMrBkcAAAAK;
						x=24000; y=12000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							funcmap/name=OC1B
							name=PA5
							pinnum=8
							role=terminal
						}
					}
					ha:group.13 {
						uuid=Of4kyoIScCrE774KCa4AAABY; src_uuid=+oSyprNFSw9F5MMrBkcAAAAL;
						x=24000; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							funcmap/name=OC1A
							name=PA6
							pinnum=7
							role=terminal
						}
					}
					ha:group.14 {
						uuid=Of4kyoIScCrE774KCa4AAABZ; src_uuid=+oSyprNFSw9F5MMrBkcAAAAM;
						x=24000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							funcmap/name=PA7
							name=PA7
							pinnum=6
							role=terminal
						}
					}
					ha:group.15 {
						uuid=Of4kyoIScCrE774KCa4AAABa; src_uuid=+oSyprNFSw9F5MMrBkcAAAAN;
						x=12000; y=36000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=0; stroke=term-secondary; text=VCC; }
						}
						ha:attrib {
							name=VCC
							pinnum=1
							role=terminal
						}
					}
					ha:group.16 {
						uuid=Of4kyoIScCrE774KCa4AAABb; src_uuid=+oSyprNFSw9F5MMrBkcAAAAO;
						x=12000; y=0; rot=-90.000000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=0; stroke=term-secondary; text=GND; }
						}
						ha:attrib {
							name=GND
							pinnum=14
							role=terminal
						}
					}
					ha:text.17 { x1=-8000; y1=-8000; dyntext=1; stroke=sym-secondary; text=%../A.funcmap%; floater=1; }
					ha:text.18 { x1=-8000; y1=-12000; dyntext=1; stroke=sym-secondary; text=%../A.footprint%; floater=1; }
					ha:text.19 { x1=-8000; y1=-16000; dyntext=1; stroke=sym-secondary; text=%../A.device%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2023 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=edakrill/igor2
					-symbol-generator=boxsym-rnd
					device=attiny24
					footprint=so(14)
					funcmap=attiny24.funcmap
					name=U1
					role=symbol
				}
			}
			ha:group.10 {
				uuid=Of4kyoIScCrE774KCa4AAABi; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=80000; y=112000;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAABj; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.11 {
				uuid=Of4kyoIScCrE774KCa4AAABk;
				li:objects {
					ha:line.1 { x1=80000; y1=112000; x2=80000; y2=108000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.12 {
				li:conn {
					/2/11/1
					/2/9/15/1
				}
			}
			ha:connection.13 {
				li:conn {
					/2/11/1
					/2/10/1/1
				}
			}
			ha:group.17 {
				uuid=Of4kyoIScCrE774KCa4AAABp; src_uuid=Of4kyoIScCrE774KCa4AAAAK;
				x=136000; y=60000;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAABq; src_uuid=Of4kyoIScCrE774KCa4AAAAD;
						x=-8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=ctrl
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Of4kyoIScCrE774KCa4AAABr; src_uuid=Of4kyoIScCrE774KCa4AAAAD;
						x=0; y=8000; rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vin
							role=terminal
						}
					}
					ha:polygon.3 {
						li:outline {
							ha:line { x1=-8000; y1=8000; x2=-8000; y2=-8000; }
							ha:line { x1=-8000; y1=-8000; x2=8000; y2=-8000; }
							ha:line { x1=8000; y1=-8000; x2=8000; y2=8000; }
							ha:line { x1=8000; y1=8000; x2=-8000; y2=8000; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:text.4 { x1=-8000; y1=8000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.5 { x1=-1000; y1=-1000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=led
					name=S2
					role=symbol
				}
			}
			ha:group.20 {
				uuid=Of4kyoIScCrE774KCa4AAAB6; src_uuid=iNOQfJpO6hT/HFDFGjoAAABv;
				x=136000; y=72000;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAAB7; src_uuid=iNOQfJpO6hT/HFDFGjoAAABw;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-6000; y1=4000; x2=6000; y2=7000; halign=center; dyntext=1; stroke=sym-primary; text=%../A.rail%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:forge {
						delete,forge/tmp
						scalar,forge/tmp
						{sub,^,1:,forge/tmp}
						suba,$,rail,forge/tmp
						array,connect
						append,connect,forge/tmp
					}
					rail=V9V
					role=symbol
				}
			}
			ha:connection.21 {
				li:conn {
					/2/20/1/1
					/2/17/2/1
				}
			}
			ha:group.22 {
				uuid=Of4kyoIScCrE774KCa4AAAB8; src_uuid=iNOQfJpO6hT/HFDFGjoAAABv;
				x=136000; y=104000;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAAB9; src_uuid=iNOQfJpO6hT/HFDFGjoAAABw;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-6000; y1=4000; x2=6000; y2=7000; halign=center; dyntext=1; stroke=sym-primary; text=%../A.rail%; floater=1; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:forge {
						delete,forge/tmp
						scalar,forge/tmp
						{sub,^,1:,forge/tmp}
						suba,$,rail,forge/tmp
						array,connect
						append,connect,forge/tmp
					}
					rail=V9V
					role=symbol
				}
			}
			ha:connection.23 {
				li:conn {
					/2/22/1/1
					/2/8/2/1
				}
			}
			ha:group.24 {
				uuid=Of4kyoIScCrE774KCa4AAACC; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=80000; y=64000;
				li:objects {
					ha:group.1 {
						uuid=Of4kyoIScCrE774KCa4AAACD; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.25 {
				li:conn {
					/2/24/1/1
					/2/9/16/1
				}
			}
			ha:group.26 {
				uuid=Of4kyoIScCrE774KCa4AAACE;
				li:objects {
					ha:line.1 { x1=96000; y1=76000; x2=112000; y2=76000; stroke=wire; }
					ha:line.2 { x1=112000; y1=76000; x2=112000; y2=60000; stroke=wire; }
					ha:line.3 { x1=112000; y1=60000; x2=124000; y2=60000; stroke=wire; }
					ha:text.4 { x1=112000; y1=60000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=pwm1a
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.27 {
				li:conn {
					/2/26/1
					/2/9/13/1
				}
			}
			ha:connection.28 {
				li:conn {
					/2/26/3
					/2/17/1/1
				}
			}
			ha:group.29 {
				uuid=Of4kyoIScCrE774KCa4AAACF;
				li:objects {
					ha:line.1 { x1=96000; y1=80000; x2=112000; y2=80000; stroke=wire; }
					ha:line.2 { x1=112000; y1=80000; x2=112000; y2=92000; stroke=wire; }
					ha:line.3 { x1=112000; y1=92000; x2=124000; y2=92000; stroke=wire; }
					ha:text.4 { x1=112000; y1=92000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=pwm1b
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.30 {
				li:conn {
					/2/29/1
					/2/9/12/1
				}
			}
			ha:connection.31 {
				li:conn {
					/2/29/3
					/2/8/1/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1/2
			print_page=A/4
			title=programmable blinking LEDs
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
