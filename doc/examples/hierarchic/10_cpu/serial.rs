ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAr;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAs; loclib_name=2n7002_sot23;
						li:objects {
						}
						ha:attrib {
							device=2n7002
							footprint=SOT23
							li:portmap {
								{G->pcb/pinnum=1}
								{S->pcb/pinnum=2}
								{D->pcb/pinnum=3}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=GOjvawM2mgwjKV+ohqMAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.7 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAQ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=108000; y=140000;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAR; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAS; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=8000; y1=-2000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=4000; y1=2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					footprint=1206
					name=R101
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=1k
				}
			}
			ha:group.9 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAW;
				li:objects {
					ha:line.1 { x1=96000; y1=140000; x2=108000; y2=140000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/9/1
					/2/7/2/1
				}
			}
			ha:group.12 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAX;
				li:objects {
					ha:line.1 { x1=96000; y1=144000; x2=104000; y2=144000; stroke=wire; }
					ha:line.2 { x1=104000; y1=144000; x2=104000; y2=148000; stroke=wire; }
					ha:line.3 { x1=104000; y1=148000; x2=140000; y2=148000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.14 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAY;
				li:objects {
					ha:line.1 { x1=128000; y1=140000; x2=140000; y2=140000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.15 {
				li:conn {
					/2/14/1
					/2/7/1/1
				}
			}
			ha:group.16 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAb; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=140000; y=148000; mirx=1;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=term_rx
					role=terminal
				}
			}
			ha:group.17 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAe; src_uuid=AHibvjaMiL5NH+9/wR0AAAAI;
				x=140000; y=140000;
				li:objects {
					ha:line.1 { x1=4000; y1=0; x2=0; y2=0; stroke=term-decor; }
					ha:text.2 { x1=4500; y1=-1500; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=17000; y1=0; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=17000; y1=0; x2=16000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=16000; y1=-2000; x2=4000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=4000; y1=2000; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=4000; y1=2000; x2=4000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet output net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=term_tx
					role=terminal
				}
			}
			ha:connection.18 {
				li:conn {
					/2/16/1
					/2/12/3
				}
			}
			ha:connection.19 {
				li:conn {
					/2/17/1
					/2/14/1
				}
			}
			ha:group.20 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAn; src_uuid=iNOQfJpO6hT/HFDFGjoAAACC;
				x=120000; y=80000;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAo; src_uuid=iNOQfJpO6hT/HFDFGjoAAACD;
						x=12000; y=12000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=D
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAp; src_uuid=iNOQfJpO6hT/HFDFGjoAAACE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=G
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:text.3 { x1=8000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.5 { cx=11000; cy=3000; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=9000; y1=-1000; x2=9000; y2=1000; stroke=sym-decor; }
					ha:line.8 { x1=9000; y1=2000; x2=9000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=9000; y1=5000; x2=9000; y2=7000; stroke=sym-decor; }
					ha:line.10 { x1=9000; y1=3000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.11 { x1=9000; y1=0; x2=12000; y2=0; stroke=sym-decor; }
					ha:line.12 { x1=9000; y1=6000; x2=12000; y2=6000; stroke=sym-decor; }
					ha:line.13 { x1=12000; y1=6000; x2=12000; y2=8000; stroke=sym-decor; }
					ha:line.14 { x1=12000; y1=-4000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.15 { x1=13000; y1=3000; x2=15000; y2=3000; stroke=sym-decor; }
					ha:line.16 { x1=15000; y1=3000; x2=14000; y2=4000; stroke=sym-decor; }
					ha:line.17 { x1=14000; y1=4000; x2=13000; y2=3000; stroke=sym-decor; }
					ha:line.18 { x1=13000; y1=4000; x2=15000; y2=4000; stroke=sym-decor; }
					ha:line.19 { x1=12000; y1=7000; x2=14000; y2=7000; stroke=sym-decor; }
					ha:line.20 { x1=14000; y1=-1000; x2=12000; y2=-1000; stroke=sym-decor; }
					ha:line.21 { x1=8000; y1=7000; x2=8000; y2=0; stroke=sym-decor; }
					ha:polygon.22 {
						li:outline {
							ha:line { x1=10000; y1=4000; x2=9000; y2=3000; }
							ha:line { x1=9000; y1=3000; x2=10000; y2=2000; }
							ha:line { x1=10000; y1=2000; x2=10000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.23 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAACF;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=S
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.24 { x1=14000; y1=-1000; x2=14000; y2=3000; stroke=sym-decor; }
					ha:line.25 { x1=14000; y1=4000; x2=14000; y2=7000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=2n7002_sot23
					name=Q102
					role=symbol
					ha:spice/prefix = { value=M; prio=31050; }
				}
			}
			ha:group.21 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=132000; y=100000; rot=90.000000;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAy; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=8000; y1=-2000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=4000; y1=2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					footprint=1206
					name=R102
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=1k
				}
			}
			ha:group.22 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAz;
				li:objects {
					ha:line.1 { x1=132000; y1=92000; x2=132000; y2=100000; stroke=wire; }
					ha:line.2 { x1=132000; y1=96000; x2=144000; y2=96000; stroke=wire; }
					ha:line.3 { x1=132000; y1=96000; x2=132000; y2=96000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.24 {
				li:conn {
					/2/22/1
					/2/21/2/1
				}
			}
			ha:group.25 {
				uuid=GOjvawM2mgwjKV+ohqMAAAA0;
				li:objects {
					ha:line.1 { x1=132000; y1=120000; x2=132000; y2=124000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.26 {
				li:conn {
					/2/25/1
					/2/21/1/1
				}
			}
			ha:group.27 {
				uuid=GOjvawM2mgwjKV+ohqMAAAA5; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=132000; y=124000;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAAA6; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:connection.28 {
				li:conn {
					/2/27/1/1
					/2/25/1
				}
			}
			ha:connection.29 {
				li:conn {
					/2/22/1
					/2/20/1/1
				}
			}
			ha:group.30 {
				uuid=GOjvawM2mgwjKV+ohqMAAAA7;
				li:objects {
					ha:line.2 { x1=96000; y1=128000; x2=108000; y2=128000; stroke=wire; }
					ha:line.3 { x1=108000; y1=128000; x2=108000; y2=80000; stroke=wire; }
					ha:line.4 { x1=108000; y1=80000; x2=120000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.32 {
				li:conn {
					/2/30/4
					/2/20/2/1
				}
			}
			ha:group.33 {
				uuid=GOjvawM2mgwjKV+ohqMAAABA; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=132000; y=68000;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAABB; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.34 {
				uuid=GOjvawM2mgwjKV+ohqMAAABC;
				li:objects {
					ha:line.1 { x1=132000; y1=72000; x2=132000; y2=68000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.35 {
				li:conn {
					/2/34/1
					/2/20/23/1
				}
			}
			ha:connection.36 {
				li:conn {
					/2/34/1
					/2/33/1/1
				}
			}
			ha:group.47 {
				uuid=GOjvawM2mgwjKV+ohqMAAABY; src_uuid=AHibvjaMiL5NH+9/wR0AAAAI;
				x=144000; y=96000;
				li:objects {
					ha:line.1 { x1=4000; y1=0; x2=0; y2=0; stroke=term-decor; }
					ha:text.2 { x1=4500; y1=-1500; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=17000; y1=0; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=17000; y1=0; x2=16000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=16000; y1=-2000; x2=4000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=4000; y1=2000; x2=16000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=4000; y1=2000; x2=4000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet output net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=inv_tx
					role=terminal
				}
			}
			ha:connection.48 {
				li:conn {
					/2/47/1
					/2/22/2
				}
			}
			ha:group.49 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAAv; src_uuid=iNOQfJpO6hT/HFDFGjoAAACC;
				x=196000; y=80000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAAw; src_uuid=iNOQfJpO6hT/HFDFGjoAAACD;
						x=12000; y=12000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=D
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAAx; src_uuid=iNOQfJpO6hT/HFDFGjoAAACE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=G
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:text.3 { x1=8000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=4000; y1=8000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.5 { cx=11000; cy=3000; r=5500; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=9000; y1=-1000; x2=9000; y2=1000; stroke=sym-decor; }
					ha:line.8 { x1=9000; y1=2000; x2=9000; y2=4000; stroke=sym-decor; }
					ha:line.9 { x1=9000; y1=5000; x2=9000; y2=7000; stroke=sym-decor; }
					ha:line.10 { x1=9000; y1=3000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.11 { x1=9000; y1=0; x2=12000; y2=0; stroke=sym-decor; }
					ha:line.12 { x1=9000; y1=6000; x2=12000; y2=6000; stroke=sym-decor; }
					ha:line.13 { x1=12000; y1=6000; x2=12000; y2=8000; stroke=sym-decor; }
					ha:line.14 { x1=12000; y1=-4000; x2=12000; y2=3000; stroke=sym-decor; }
					ha:line.15 { x1=13000; y1=3000; x2=15000; y2=3000; stroke=sym-decor; }
					ha:line.16 { x1=15000; y1=3000; x2=14000; y2=4000; stroke=sym-decor; }
					ha:line.17 { x1=14000; y1=4000; x2=13000; y2=3000; stroke=sym-decor; }
					ha:line.18 { x1=13000; y1=4000; x2=15000; y2=4000; stroke=sym-decor; }
					ha:line.19 { x1=12000; y1=7000; x2=14000; y2=7000; stroke=sym-decor; }
					ha:line.20 { x1=14000; y1=-1000; x2=12000; y2=-1000; stroke=sym-decor; }
					ha:line.21 { x1=8000; y1=7000; x2=8000; y2=0; stroke=sym-decor; }
					ha:polygon.22 {
						li:outline {
							ha:line { x1=10000; y1=4000; x2=9000; y2=3000; }
							ha:line { x1=9000; y1=3000; x2=10000; y2=2000; }
							ha:line { x1=10000; y1=2000; x2=10000; y2=4000; }
						}
						stroke=sym-decor;
						fill=sym-decor;
					}
					ha:group.23 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAAy; src_uuid=iNOQfJpO6hT/HFDFGjoAAACF;
						x=12000; y=-4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=S
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.24 { x1=14000; y1=-1000; x2=14000; y2=3000; stroke=sym-decor; }
					ha:line.25 { x1=14000; y1=4000; x2=14000; y2=7000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=2n7002_sot23
					name=Q103
					role=symbol
					ha:spice/prefix = { value=M; prio=31050; }
				}
			}
			ha:group.50 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAAz; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=184000; y=100000; rot=90.000000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAA0; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAA1; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=8000; y1=-2000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=4000; y1=2000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					footprint=1206
					name=R103
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=1k
				}
			}
			ha:group.54 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAA3; src_uuid=GOjvawM2mgwjKV+ohqMAAAA0;
				x=316000; y=0; mirx=1;
				li:objects {
					ha:line.1 { x1=132000; y1=120000; x2=132000; y2=124000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.56 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAA4; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=184000; y=124000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAA5; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.58 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAA6; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=184000; y=68000; mirx=1;
				li:objects {
					ha:group.1 {
						uuid=Q8cSXpoK0QkYCcKy+asAAAA7; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.59 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAA8; src_uuid=GOjvawM2mgwjKV+ohqMAAABC;
				x=316000; y=0; mirx=1;
				li:objects {
					ha:line.1 { x1=132000; y1=72000; x2=132000; y2=68000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.64 {
				li:conn {
					/2/50/2/1
					/2/70/2
				}
			}
			ha:connection.65 {
				li:conn {
					/2/49/1/1
					/2/70/2
				}
			}
			ha:connection.66 {
				li:conn {
					/2/54/1
					/2/50/1/1
				}
			}
			ha:connection.67 {
				li:conn {
					/2/56/1/1
					/2/54/1
				}
			}
			ha:connection.68 {
				li:conn {
					/2/59/1
					/2/49/23/1
				}
			}
			ha:connection.69 {
				li:conn {
					/2/59/1
					/2/58/1/1
				}
			}
			ha:group.70 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAA+;
				li:objects {
					ha:line.1 { x1=96000; y1=132000; x2=172000; y2=132000; stroke=wire; }
					ha:line.2 { x1=184000; y1=92000; x2=184000; y2=100000; stroke=wire; }
					ha:line.3 { x1=184000; y1=96000; x2=172000; y2=96000; stroke=wire; }
					ha:line.4 { x1=184000; y1=96000; x2=184000; y2=96000; stroke=junction; }
					ha:line.5 { x1=172000; y1=132000; x2=172000; y2=96000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.72 {
				uuid=Q8cSXpoK0QkYCcKy+asAAAA/;
				li:objects {
					ha:line.1 { x1=196000; y1=80000; x2=208000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.73 {
				li:conn {
					/2/72/1
					/2/49/2/1
				}
			}
			ha:group.74 {
				uuid=Q8cSXpoK0QkYCcKy+asAAABB; src_uuid=AHibvjaMiL5NH+9/wR0AAAAG;
				x=208000; y=80000; mirx=1;
				li:objects {
					ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
					ha:text.2 { x1=-5000; y1=-1500; mirx=1; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
					ha:line.3 { x1=-4000; y1=0; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.4 { x1=-4000; y1=0; x2=-5000; y2=-2000; stroke=sheet-decor; }
					ha:line.5 { x1=-5000; y1=-2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
					ha:line.6 { x1=-17000; y1=2000; x2=-5000; y2=2000; stroke=sheet-decor; }
					ha:line.7 { x1=-17000; y1=2000; x2=-17000; y2=-2000; stroke=sheet-decor; }
				}
				ha:attrib {
					-sym-comment={ Sheet level terminal (not really a symbol) for subsheet input net in a hierarchy }
					-sym-copyright=(C) 2024 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=inv_rx
					role=terminal
				}
			}
			ha:connection.75 {
				li:conn {
					/2/74/1
					/2/72/1
				}
			}
			ha:group.76 {
				uuid=Q8cSXpoK0QkYCcKy+asAAABC;
				x=76000; y=148000;
				li:objects {
					ha:group.1 {
						uuid=Q8cSXpoK0QkYCcKy+asAAABD; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=rx1
							pinnum=12
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Q8cSXpoK0QkYCcKy+asAAABE; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=tx1
							pinnum=13
							role=terminal
						}
					}
					ha:group.3 {
						uuid=Q8cSXpoK0QkYCcKy+asAAABF; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-16000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=rx2
							pinnum=17
							role=terminal
						}
					}
					ha:group.4 {
						uuid=Q8cSXpoK0QkYCcKy+asAAABG; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-20000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=tx2
							pinnum=18
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=16000; y1=0; x2=16000; y2=-24000; }
							ha:line { x1=16000; y1=-24000; x2=0; y2=-24000; }
							ha:line { x1=0; y1=-24000; x2=0; y2=0; }
							ha:line { x1=0; y1=0; x2=16000; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:text.6 { x1=0; y1=0; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=U1
					role=symbol
				}
			}
			ha:connection.77 {
				li:conn {
					/2/12/1
					/2/76/1/1
				}
			}
			ha:connection.78 {
				li:conn {
					/2/9/1
					/2/76/2/1
				}
			}
			ha:connection.79 {
				li:conn {
					/2/70/1
					/2/76/3/1
				}
			}
			ha:connection.80 {
				li:conn {
					/2/30/2
					/2/76/4/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=4/4
			print_page=A/4
			title={serial ports: terminal and inverted}
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
