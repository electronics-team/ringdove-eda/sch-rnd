ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=GOjvawM2mgwjKV+ohqMAAAAr;
				li:objects {
					ha:group.1 {
						uuid=GOjvawM2mgwjKV+ohqMAAAAs; loclib_name=2n7002_sot23;
						li:objects {
						}
						ha:attrib {
							device=2n7002
							footprint=SOT23
							li:portmap {
								{G->pcb/pinnum=1}
								{S->pcb/pinnum=2}
								{D->pcb/pinnum=3}
							}
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAC0; loclib_name=1n4148_minimelf;
						li:objects {
						}
						ha:attrib {
							device=1n4148
							footprint=minimelf
							li:portmap {
								{C->pcb/pinnum=1}
								{A->pcb/pinnum=2}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=GOjvawM2mgwjKV+ohqMAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.91 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAA9;
				x=76000; y=156000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAA+; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-12000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=row3
							pinnum=9
							role=terminal
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAA/; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-16000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=row4
							pinnum=8
							role=terminal
						}
					}
					ha:group.3 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAABA; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-24000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=col1
							pinnum=10
							role=terminal
						}
					}
					ha:group.4 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAABB; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-28000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=col2
							pinnum=11
							role=terminal
						}
					}
					ha:polygon.5 {
						li:outline {
							ha:line { x1=16000; y1=0; x2=16000; y2=-40000; }
							ha:line { x1=16000; y1=-40000; x2=0; y2=-40000; }
							ha:line { x1=0; y1=-40000; x2=0; y2=0; }
							ha:line { x1=0; y1=0; x2=16000; y2=0; }
						}
						stroke=sym-decor;
						fill=sym-decor-fill;
					}
					ha:group.6 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAABC; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=row1
							pinnum=6
							role=terminal
						}
					}
					ha:group.7 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAABD; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=row2
							pinnum=7
							role=terminal
						}
					}
					ha:group.8 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAABE; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-32000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=col3
							pinnum=15
							role=terminal
						}
					}
					ha:group.9 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAABF; src_uuid=GOjvawM2mgwjKV+ohqMAAAAD;
						x=20000; y=-36000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=col4
							pinnum=16
							role=terminal
						}
					}
					ha:text.10 { x1=0; y1=0; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					footprint=dip(20)
					name=U1
					role=symbol
				}
			}
			ha:group.93 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAACf; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=140000; y=180000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAACg; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAACh; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW201
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.95 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAACx; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=132000; y=176000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAACy; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAACz; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D201
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.96 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=-4000; y=52000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.99 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAC9; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=176000; y=180000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAC+; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAC/; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW211
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.100 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADA; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=168000; y=176000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADB; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D211
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.101 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADD; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=32000; y=52000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.104 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADL; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=212000; y=180000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADM; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADN; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW221
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.105 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADO; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=204000; y=176000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADP; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADQ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D221
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.106 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADR; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=68000; y=52000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.109 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADn; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=140000; y=144000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADo; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADp; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW202
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.110 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADq; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=132000; y=140000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADr; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADs; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D202
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.111 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADt; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=-4000; y=16000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.114 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADu; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=176000; y=144000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADv; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADw; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW212
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.115 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAADx; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=168000; y=140000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADy; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAADz; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D212
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.116 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAD0; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=32000; y=16000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.119 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAD1; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=212000; y=144000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAD2; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAD3; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW222
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.120 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAD4; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=204000; y=140000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAD5; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAD6; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D222
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.121 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAD7; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=68000; y=16000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.124 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAD8; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=140000; y=108000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAD9; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAD+; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW203
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.125 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAD/; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=132000; y=104000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEA; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEB; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D203
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.126 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEC; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=-4000; y=-20000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.129 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAED; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=176000; y=108000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEE; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEF; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW213
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.130 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEG; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=168000; y=104000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEH; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEI; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D213
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.131 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEJ; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=32000; y=-20000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:group.134 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEK; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACZ;
				x=212000; y=108000;
				li:objects {
					ha:text.1 { x1=6000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEL; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACa;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:line.3 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.4 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:group.5 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEM; src_uuid=Zj+oUtIHRUPA2aMj1qcAAACb;
						x=8000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:line.6 { x1=6800; y1=0; x2=8000; y2=0; stroke=sym-decor; }
					ha:arc.7 { cx=6400; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.8 { x1=0; y1=0; x2=0; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=0; y1=0; x2=1200; y2=0; stroke=sym-decor; }
					ha:arc.10 { cx=1600; cy=0; r=400; sang=0.000000; dang=360.000000; stroke=sym-decor; }
					ha:line.11 { x1=1200; y1=3600; x2=6800; y2=3600; stroke=sym-decor; }
					ha:line.12 { x1=1600; y1=8000; x2=6400; y2=8000; stroke=sym-decor; }
					ha:line.13 { x1=1600; y1=8000; x2=1600; y2=7200; stroke=sym-decor; }
					ha:line.14 { x1=6400; y1=8000; x2=6400; y2=7200; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=3600; x2=4000; y2=8000; stroke=sym-decor; }
				}
				ha:attrib {
					footprint=dip(6)
					name=SW223
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.135 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEN; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAN;
				x=204000; y=104000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEO; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAO;
						x=16000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=C
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=Zj+oUtIHRUPA2aMj1qcAAAEP; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAP;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=A
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:text.3 { x1=12000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-secondary; text=%../a.devmap%; floater=1; }
					ha:text.4 { x1=8000; y1=5000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:line.5 { x1=12000; y1=0; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.6 { x1=4000; y1=0; x2=6000; y2=0; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=4000; x2=10000; y2=0; stroke=sym-decor; }
					ha:line.8 { x1=10000; y1=0; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.9 { x1=6000; y1=4000; x2=6000; y2=-4000; stroke=sym-decor; }
					ha:line.10 { x1=10000; y1=4000; x2=10000; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					devmap=1n4148_minimelf
					name=D223
					role=symbol
					ha:spice/prefix = { value=D; prio=31050; }
				}
			}
			ha:group.136 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEQ; src_uuid=Zj+oUtIHRUPA2aMj1qcAAAC1;
				x=68000; y=-20000;
				li:objects {
					ha:line.1 { x1=136000; y1=124000; x2=136000; y2=128000; stroke=wire; }
					ha:line.2 { x1=136000; y1=128000; x2=140000; y2=128000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.165 {
				li:conn {
					/2/96/1
					/2/95/2/1
				}
			}
			ha:connection.166 {
				li:conn {
					/2/96/2
					/2/93/2/1
				}
			}
			ha:connection.167 {
				li:conn {
					/2/101/1
					/2/100/2/1
				}
			}
			ha:connection.168 {
				li:conn {
					/2/101/2
					/2/99/2/1
				}
			}
			ha:connection.169 {
				li:conn {
					/2/106/1
					/2/105/2/1
				}
			}
			ha:connection.170 {
				li:conn {
					/2/106/2
					/2/104/2/1
				}
			}
			ha:connection.171 {
				li:conn {
					/2/111/1
					/2/110/2/1
				}
			}
			ha:connection.172 {
				li:conn {
					/2/111/2
					/2/109/2/1
				}
			}
			ha:connection.173 {
				li:conn {
					/2/116/1
					/2/115/2/1
				}
			}
			ha:connection.174 {
				li:conn {
					/2/116/2
					/2/114/2/1
				}
			}
			ha:connection.175 {
				li:conn {
					/2/121/1
					/2/120/2/1
				}
			}
			ha:connection.176 {
				li:conn {
					/2/121/2
					/2/119/2/1
				}
			}
			ha:connection.179 {
				li:conn {
					/2/126/1
					/2/125/2/1
				}
			}
			ha:connection.180 {
				li:conn {
					/2/126/2
					/2/124/2/1
				}
			}
			ha:connection.183 {
				li:conn {
					/2/131/1
					/2/130/2/1
				}
			}
			ha:connection.184 {
				li:conn {
					/2/131/2
					/2/129/2/1
				}
			}
			ha:connection.187 {
				li:conn {
					/2/136/1
					/2/135/2/1
				}
			}
			ha:connection.188 {
				li:conn {
					/2/136/2
					/2/134/2/1
				}
			}
			ha:connection.189 {
				li:conn {
					/2/105/1/1
					/2/207/4
				}
			}
			ha:connection.190 {
				li:conn {
					/2/100/1/1
					/2/207/5
				}
			}
			ha:connection.191 {
				li:conn {
					/2/95/1/1
					/2/207/7
				}
			}
			ha:connection.192 {
				li:conn {
					/2/120/1/1
					/2/209/3
				}
			}
			ha:connection.193 {
				li:conn {
					/2/115/1/1
					/2/209/4
				}
			}
			ha:connection.194 {
				li:conn {
					/2/110/1/1
					/2/209/6
				}
			}
			ha:connection.195 {
				li:conn {
					/2/135/1/1
					/2/211/4
				}
			}
			ha:connection.196 {
				li:conn {
					/2/130/1/1
					/2/211/5
				}
			}
			ha:connection.197 {
				li:conn {
					/2/125/1/1
					/2/211/7
				}
			}
			ha:connection.198 {
				li:conn {
					/2/93/5/1
					/2/213/4
				}
			}
			ha:connection.199 {
				li:conn {
					/2/124/5/1
					/2/213/6
				}
			}
			ha:connection.200 {
				li:conn {
					/2/109/5/1
					/2/213/8
				}
			}
			ha:connection.201 {
				li:conn {
					/2/99/5/1
					/2/215/5
				}
			}
			ha:connection.202 {
				li:conn {
					/2/129/5/1
					/2/215/7
				}
			}
			ha:connection.203 {
				li:conn {
					/2/114/5/1
					/2/215/9
				}
			}
			ha:connection.204 {
				li:conn {
					/2/104/5/1
					/2/217/4
				}
			}
			ha:connection.205 {
				li:conn {
					/2/134/5/1
					/2/217/6
				}
			}
			ha:connection.206 {
				li:conn {
					/2/119/5/1
					/2/217/8
				}
			}
			ha:group.207 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEZ;
				li:objects {
					ha:line.1 { x1=96000; y1=152000; x2=104000; y2=152000; stroke=wire; }
					ha:line.2 { x1=104000; y1=152000; x2=104000; y2=156000; stroke=wire; }
					ha:line.4 { x1=204000; y1=156000; x2=204000; y2=160000; stroke=wire; }
					ha:line.5 { x1=168000; y1=160000; x2=168000; y2=156000; stroke=wire; }
					ha:line.6 { x1=168000; y1=156000; x2=168000; y2=156000; stroke=junction; }
					ha:line.7 { x1=132000; y1=160000; x2=132000; y2=156000; stroke=wire; }
					ha:line.8 { x1=132000; y1=156000; x2=132000; y2=156000; stroke=junction; }
					ha:line.9 { x1=104000; y1=156000; x2=204000; y2=156000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.208 {
				li:conn {
					/2/207/1
					/2/91/6/1
				}
			}
			ha:group.209 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEa;
				li:objects {
					ha:line.1 { x1=96000; y1=148000; x2=124000; y2=148000; stroke=wire; }
					ha:line.2 { x1=124000; y1=120000; x2=204000; y2=120000; stroke=wire; }
					ha:line.3 { x1=204000; y1=120000; x2=204000; y2=124000; stroke=wire; }
					ha:line.4 { x1=168000; y1=124000; x2=168000; y2=120000; stroke=wire; }
					ha:line.5 { x1=168000; y1=120000; x2=168000; y2=120000; stroke=junction; }
					ha:line.6 { x1=132000; y1=124000; x2=132000; y2=120000; stroke=wire; }
					ha:line.7 { x1=132000; y1=120000; x2=132000; y2=120000; stroke=junction; }
					ha:line.8 { x1=124000; y1=148000; x2=124000; y2=120000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.210 {
				li:conn {
					/2/209/1
					/2/91/7/1
				}
			}
			ha:group.211 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEb;
				li:objects {
					ha:line.1 { x1=96000; y1=144000; x2=120000; y2=144000; stroke=wire; }
					ha:line.2 { x1=120000; y1=144000; x2=120000; y2=84000; stroke=wire; }
					ha:line.4 { x1=204000; y1=84000; x2=204000; y2=88000; stroke=wire; }
					ha:line.5 { x1=168000; y1=88000; x2=168000; y2=84000; stroke=wire; }
					ha:line.6 { x1=168000; y1=84000; x2=168000; y2=84000; stroke=junction; }
					ha:line.7 { x1=132000; y1=88000; x2=132000; y2=84000; stroke=wire; }
					ha:line.8 { x1=132000; y1=84000; x2=132000; y2=84000; stroke=junction; }
					ha:line.9 { x1=120000; y1=84000; x2=204000; y2=84000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.212 {
				li:conn {
					/2/211/1
					/2/91/1/1
				}
			}
			ha:group.213 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEc;
				li:objects {
					ha:line.1 { x1=96000; y1=132000; x2=116000; y2=132000; stroke=wire; }
					ha:line.3 { x1=116000; y1=80000; x2=116000; y2=132000; stroke=wire; }
					ha:line.4 { x1=152000; y1=180000; x2=156000; y2=180000; stroke=wire; }
					ha:line.5 { x1=156000; y1=180000; x2=156000; y2=80000; stroke=wire; }
					ha:line.6 { x1=152000; y1=108000; x2=156000; y2=108000; stroke=wire; }
					ha:line.7 { x1=156000; y1=108000; x2=156000; y2=108000; stroke=junction; }
					ha:line.8 { x1=152000; y1=144000; x2=156000; y2=144000; stroke=wire; }
					ha:line.9 { x1=156000; y1=144000; x2=156000; y2=144000; stroke=junction; }
					ha:line.10 { x1=116000; y1=80000; x2=156000; y2=80000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.214 {
				li:conn {
					/2/213/1
					/2/91/3/1
				}
			}
			ha:group.215 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEd;
				li:objects {
					ha:line.1 { x1=96000; y1=128000; x2=112000; y2=128000; stroke=wire; }
					ha:line.3 { x1=112000; y1=76000; x2=112000; y2=128000; stroke=wire; }
					ha:line.4 { x1=112000; y1=76000; x2=192000; y2=76000; stroke=wire; }
					ha:line.5 { x1=188000; y1=180000; x2=192000; y2=180000; stroke=wire; }
					ha:line.7 { x1=188000; y1=108000; x2=192000; y2=108000; stroke=wire; }
					ha:line.8 { x1=192000; y1=108000; x2=192000; y2=108000; stroke=junction; }
					ha:line.9 { x1=188000; y1=144000; x2=192000; y2=144000; stroke=wire; }
					ha:line.10 { x1=192000; y1=144000; x2=192000; y2=144000; stroke=junction; }
					ha:line.11 { x1=192000; y1=76000; x2=192000; y2=180000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.216 {
				li:conn {
					/2/215/1
					/2/91/4/1
				}
			}
			ha:group.217 {
				uuid=Zj+oUtIHRUPA2aMj1qcAAAEe;
				li:objects {
					ha:line.1 { x1=96000; y1=124000; x2=108000; y2=124000; stroke=wire; }
					ha:line.2 { x1=108000; y1=124000; x2=108000; y2=72000; stroke=wire; }
					ha:line.3 { x1=108000; y1=72000; x2=228000; y2=72000; stroke=wire; }
					ha:line.4 { x1=224000; y1=180000; x2=228000; y2=180000; stroke=wire; }
					ha:line.6 { x1=224000; y1=108000; x2=228000; y2=108000; stroke=wire; }
					ha:line.7 { x1=228000; y1=108000; x2=228000; y2=108000; stroke=junction; }
					ha:line.8 { x1=224000; y1=144000; x2=228000; y2=144000; stroke=wire; }
					ha:line.9 { x1=228000; y1=144000; x2=228000; y2=144000; stroke=junction; }
					ha:line.10 { x1=228000; y1=72000; x2=228000; y2=180000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.218 {
				li:conn {
					/2/217/1
					/2/91/8/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=2/4
			print_page=A/4
			title=button matrix, 3*3
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
