ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=HV4brSYpErb7Smew8cEAAAAW;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAAAX; loclib_name=lm358_so8;
						li:objects {
						}
						ha:attrib {
							device=lm358
							footprint=so(8)
							li:portmap {
								{1/in- -> pcb/pinnum=2}
								{1/in+ -> pcb/pinnum=3}
								{1/out -> pcb/pinnum=1}
								{1/V+ -> pcb/pinnum=8}
								{1/V- -> pcb/pinnum=4}
								{2/in- -> pcb/pinnum=6}
								{2/in+ -> pcb/pinnum=5}
								{2/out -> pcb/pinnum=7}
								{2/V+ -> pcb/pinnum=8}
								{2/V- -> pcb/pinnum=4}
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=devmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=HV4brSYpErb7Smew8cEAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.33 {
				uuid=HV4brSYpErb7Smew8cEAAAAn;
				x=156000; y=88000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAAAo; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=4000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in1p
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.2 {
						uuid=HV4brSYpErb7Smew8cEAAAAp; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=4000; y=-8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in1n
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.3 {
						uuid=HV4brSYpErb7Smew8cEAAAAq; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						x=24000; y=-4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out1
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.4 { x1=4000; y1=-12000; x2=4000; y2=4000; stroke=sym-decor; }
					ha:line.5 { x1=4000; y1=4000; x2=20000; y2=-4000; stroke=sym-decor; }
					ha:line.6 { x1=20000; y1=-4000; x2=4000; y2=-12000; stroke=sym-decor; }
					ha:line.7 { x1=6000; y1=1000; x2=6000; y2=-1000; stroke=sym-decor; }
					ha:line.8 { x1=5000; y1=0; x2=7000; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=5000; y1=-8000; x2=7000; y2=-8000; stroke=sym-decor; }
					ha:group.10 {
						uuid=HV4brSYpErb7Smew8cEAAAAr; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAM;
						x=12000; y=4000; rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=-4000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vpos
							role=terminal
							ha:spice/pinnum = { value=4; prio=31050; }
						}
					}
					ha:group.11 {
						uuid=HV4brSYpErb7Smew8cEAAAAs; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAI;
						x=4000; y=-16000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in2p
							role=terminal
							ha:spice/pinnum = { value=2; prio=31050; }
						}
					}
					ha:group.12 {
						uuid=HV4brSYpErb7Smew8cEAAAAt; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAJ;
						x=4000; y=-24000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=in2n
							role=terminal
							ha:spice/pinnum = { value=1; prio=31050; }
						}
					}
					ha:group.13 {
						uuid=HV4brSYpErb7Smew8cEAAAAu; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAK;
						x=24000; y=-20000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-4000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=out2
							role=terminal
							ha:spice/pinnum = { value=3; prio=31050; }
						}
					}
					ha:line.14 { x1=4000; y1=-28000; x2=4000; y2=-12000; stroke=sym-decor; }
					ha:line.15 { x1=4000; y1=-12000; x2=20000; y2=-20000; stroke=sym-decor; }
					ha:line.16 { x1=20000; y1=-20000; x2=4000; y2=-28000; stroke=sym-decor; }
					ha:line.17 { x1=6000; y1=-15000; x2=6000; y2=-17000; stroke=sym-decor; }
					ha:line.18 { x1=5000; y1=-16000; x2=7000; y2=-16000; stroke=sym-decor; }
					ha:line.19 { x1=5000; y1=-24000; x2=7000; y2=-24000; stroke=sym-decor; }
					ha:group.20 {
						uuid=HV4brSYpErb7Smew8cEAAAAv; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAL;
						x=12000; y=-24000; rot=270.000000; mirx=1; miry=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=0; y1=-1000; rot=180.000000; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=Vneg
							role=terminal
							ha:spice/pinnum = { value=5; prio=31050; }
						}
					}
					ha:text.21 { x1=12000; y1=-12000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:text.22 { x1=12000; y1=-16000; dyntext=1; stroke=sym-secondary; text=%../A.cschem/child/name%; floater=1; }
				}
				ha:attrib {
					cschem/child/name=dual_opamp.rs
					name=S1
					role=symbol
				}
			}
			ha:group.34 {
				uuid=HV4brSYpErb7Smew8cEAAAAw;
				li:objects {
					ha:line.1 { x1=156000; y1=80000; x2=140000; y2=80000; stroke=wire; }
					ha:line.2 { x1=140000; y1=80000; x2=140000; y2=104000; stroke=wire; }
					ha:line.3 { x1=140000; y1=104000; x2=184000; y2=104000; stroke=wire; }
					ha:line.4 { x1=184000; y1=104000; x2=184000; y2=84000; stroke=wire; }
					ha:line.7 { x1=184000; y1=84000; x2=184000; y2=84000; stroke=junction; }
					ha:line.8 { x1=180000; y1=84000; x2=228000; y2=84000; stroke=wire; }
					ha:text.9 { x1=200000; y1=84000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=div1
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.35 {
				li:conn {
					/2/34/1
					/2/33/2/1
				}
			}
			ha:connection.36 {
				li:conn {
					/2/33/3/1
					/2/34/8
				}
			}
			ha:group.37 {
				uuid=HV4brSYpErb7Smew8cEAAAAx;
				li:objects {
					ha:line.1 { x1=168000; y1=92000; x2=168000; y2=116000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.38 {
				li:conn {
					/2/37/1
					/2/33/10/1
				}
			}
			ha:group.39 {
				uuid=HV4brSYpErb7Smew8cEAAAA2; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=168000; y=116000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAAA3; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:connection.40 {
				li:conn {
					/2/39/1/1
					/2/37/1
				}
			}
			ha:group.41 {
				uuid=HV4brSYpErb7Smew8cEAAAA4;
				li:objects {
					ha:line.1 { x1=156000; y1=64000; x2=140000; y2=64000; stroke=wire; }
					ha:line.2 { x1=140000; y1=64000; x2=140000; y2=44000; stroke=wire; }
					ha:line.3 { x1=140000; y1=44000; x2=184000; y2=44000; stroke=wire; }
					ha:line.5 { x1=184000; y1=44000; x2=184000; y2=68000; stroke=wire; }
					ha:line.8 { x1=184000; y1=68000; x2=184000; y2=68000; stroke=junction; }
					ha:line.9 { x1=180000; y1=68000; x2=216000; y2=68000; stroke=wire; }
					ha:line.10 { x1=216000; y1=68000; x2=216000; y2=80000; stroke=wire; }
					ha:line.11 { x1=216000; y1=80000; x2=228000; y2=80000; stroke=wire; }
					ha:text.12 { x1=200000; y1=68000; dyntext=1; stroke=wire; text=%../A.name%; floater=1; }
				}
				ha:attrib {
					name=div2
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.42 {
				li:conn {
					/2/41/1
					/2/33/12/1
				}
			}
			ha:connection.43 {
				li:conn {
					/2/33/13/1
					/2/41/9
				}
			}
			ha:group.44 {
				uuid=HV4brSYpErb7Smew8cEAAAA5;
				li:objects {
					ha:line.1 { x1=168000; y1=60000; x2=168000; y2=28000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.45 {
				li:conn {
					/2/44/1
					/2/33/20/1
				}
			}
			ha:group.46 {
				uuid=HV4brSYpErb7Smew8cEAAAA+; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=168000; y=28000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAAA/; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:connection.47 {
				li:conn {
					/2/46/1/1
					/2/44/1
				}
			}
			ha:group.48 {
				uuid=HV4brSYpErb7Smew8cEAAABG; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=104000; y=112000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABH; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=HV4brSYpErb7Smew8cEAAABI; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					name=R1
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=100k
				}
			}
			ha:group.49 {
				uuid=HV4brSYpErb7Smew8cEAAABM; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=116000; y=112000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABN; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=HV4brSYpErb7Smew8cEAAABO; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					name=R2
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=100k
				}
			}
			ha:group.50 {
				uuid=HV4brSYpErb7Smew8cEAAABR; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=104000; y=116000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABS; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.51 {
				uuid=HV4brSYpErb7Smew8cEAAABT; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=116000; y=116000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABU; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.52 {
				uuid=HV4brSYpErb7Smew8cEAAABV;
				li:objects {
					ha:line.1 { x1=104000; y1=116000; x2=104000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.53 {
				li:conn {
					/2/52/1
					/2/48/2/1
				}
			}
			ha:connection.54 {
				li:conn {
					/2/52/1
					/2/50/1/1
				}
			}
			ha:group.55 {
				uuid=HV4brSYpErb7Smew8cEAAABW;
				li:objects {
					ha:line.1 { x1=116000; y1=116000; x2=116000; y2=112000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.56 {
				li:conn {
					/2/55/1
					/2/49/2/1
				}
			}
			ha:connection.57 {
				li:conn {
					/2/55/1
					/2/51/1/1
				}
			}
			ha:group.58 {
				uuid=HV4brSYpErb7Smew8cEAAABd; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=104000; y=68000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABe; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=HV4brSYpErb7Smew8cEAAABf; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					name=R3
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=100k
				}
			}
			ha:group.59 {
				uuid=HV4brSYpErb7Smew8cEAAABg; src_uuid=iNOQfJpO6hT/HFDFGjoAAABC;
				x=116000; y=68000; rot=270.000000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABh; src_uuid=iNOQfJpO6hT/HFDFGjoAAABD;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=HV4brSYpErb7Smew8cEAAABi; src_uuid=iNOQfJpO6hT/HFDFGjoAAABE;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=-3000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.3 { x1=12000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../a.value%; floater=1; }
					ha:text.4 { x1=8000; y1=2000; rot=90.000000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.5 {
						li:outline {
							ha:line { x1=4000; y1=2000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=16000; y2=-2000; }
							ha:line { x1=16000; y1=-2000; x2=16000; y2=2000; }
							ha:line { x1=16000; y1=2000; x2=4000; y2=2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					ha:device = { value=resistor; prio=31050; }
					name=R4
					role=symbol
					ha:spice/prefix = { value=R; prio=31050; }
					value=120k
				}
			}
			ha:group.60 {
				uuid=HV4brSYpErb7Smew8cEAAABj;
				li:objects {
					ha:line.1 { x1=116000; y1=92000; x2=116000; y2=68000; stroke=wire; }
					ha:line.2 { x1=156000; y1=72000; x2=116000; y2=72000; stroke=wire; }
					ha:line.3 { x1=116000; y1=72000; x2=116000; y2=72000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.61 {
				li:conn {
					/2/60/1
					/2/49/1/1
				}
			}
			ha:connection.62 {
				li:conn {
					/2/60/1
					/2/59/2/1
				}
			}
			ha:group.63 {
				uuid=HV4brSYpErb7Smew8cEAAABk;
				li:objects {
					ha:line.1 { x1=104000; y1=92000; x2=104000; y2=68000; stroke=wire; }
					ha:line.2 { x1=156000; y1=88000; x2=104000; y2=88000; stroke=wire; }
					ha:line.3 { x1=104000; y1=88000; x2=104000; y2=88000; stroke=junction; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.64 {
				li:conn {
					/2/63/1
					/2/48/1/1
				}
			}
			ha:connection.65 {
				li:conn {
					/2/63/1
					/2/58/2/1
				}
			}
			ha:group.66 {
				uuid=HV4brSYpErb7Smew8cEAAABn; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=104000; y=44000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABo; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.67 {
				uuid=HV4brSYpErb7Smew8cEAAABp; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=116000; y=44000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAABq; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.68 {
				uuid=HV4brSYpErb7Smew8cEAAABr;
				li:objects {
					ha:line.1 { x1=104000; y1=48000; x2=104000; y2=44000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.69 {
				li:conn {
					/2/68/1
					/2/58/1/1
				}
			}
			ha:connection.70 {
				li:conn {
					/2/68/1
					/2/66/1/1
				}
			}
			ha:group.71 {
				uuid=HV4brSYpErb7Smew8cEAAABs;
				li:objects {
					ha:line.1 { x1=116000; y1=48000; x2=116000; y2=44000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.72 {
				li:conn {
					/2/71/1
					/2/59/1/1
				}
			}
			ha:connection.73 {
				li:conn {
					/2/71/1
					/2/67/1/1
				}
			}
			ha:connection.74 {
				li:conn {
					/2/60/2
					/2/33/11/1
				}
			}
			ha:connection.75 {
				li:conn {
					/2/63/2
					/2/33/1/1
				}
			}
			ha:group.76 {
				uuid=HV4brSYpErb7Smew8cEAAAB9; src_uuid=HV4brSYpErb7Smew8cEAAAB4;
				x=232000; y=76000;
				li:objects {
					ha:text.1 { x1=0; y1=-6000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:group.2 {
						uuid=HV4brSYpErb7Smew8cEAAAB+; src_uuid=HV4brSYpErb7Smew8cEAAAB5;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:group.3 {
						uuid=HV4brSYpErb7Smew8cEAAAB/; src_uuid=HV4brSYpErb7Smew8cEAAAB6;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.4 {
						uuid=HV4brSYpErb7Smew8cEAAACA; src_uuid=HV4brSYpErb7Smew8cEAAAB7;
						x=0; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=3
							role=terminal
						}
					}
					ha:group.5 {
						uuid=HV4brSYpErb7Smew8cEAAACB; src_uuid=HV4brSYpErb7Smew8cEAAAB8;
						x=0; y=12000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../A.name%; }
						}
						ha:attrib {
							name=4
							role=terminal
						}
					}
					ha:polygon.6 {
						li:outline {
							ha:line { x1=0; y1=-2000; x2=0; y2=14000; }
							ha:line { x1=0; y1=14000; x2=4000; y2=14000; }
							ha:line { x1=4000; y1=14000; x2=4000; y2=-2000; }
							ha:line { x1=4000; y1=-2000; x2=0; y2=-2000; }
						}
						stroke=sym-decor;
					}
				}
				ha:attrib {
					name=CONN1
					role=symbol
					spice/omit=yes
				}
			}
			ha:group.77 {
				uuid=HV4brSYpErb7Smew8cEAAACE; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB6;
				x=224000; y=92000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAACF; src_uuid=iNOQfJpO6hT/HFDFGjoAAAB7;
						rot=270.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=2500; y1=4000; x2=-2500; y2=4000; stroke=sym-decor; }
					ha:text.3 { x1=-4000; y1=4000; x2=4000; y2=7000; halign=center; dyntext=0; stroke=sym-primary; text=Vcc; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:Vcc}
					}
					role=symbol
				}
			}
			ha:group.78 {
				uuid=HV4brSYpErb7Smew8cEAAACI; src_uuid=iNOQfJpO6hT/HFDFGjoAAABm;
				x=224000; y=56000;
				li:objects {
					ha:group.1 {
						uuid=HV4brSYpErb7Smew8cEAAACJ; src_uuid=iNOQfJpO6hT/HFDFGjoAAABn;
						rot=90.000000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							drc/require_graphical_conn=1
							ha:name = { value=1; prio=220; }
							role=terminal
						}
					}
					ha:line.2 { x1=-1500; y1=-5000; x2=1500; y2=-5000; stroke=sym-decor; }
					ha:line.3 { x1=-500; y1=-6000; x2=500; y2=-6000; stroke=sym-decor; }
					ha:line.4 { x1=-2500; y1=-4000; x2=2500; y2=-4000; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					li:connect {
						{1:GND}
					}
					role=symbol
				}
			}
			ha:group.79 {
				uuid=HV4brSYpErb7Smew8cEAAACK;
				li:objects {
					ha:line.1 { x1=228000; y1=88000; x2=224000; y2=88000; stroke=wire; }
					ha:line.2 { x1=224000; y1=88000; x2=224000; y2=92000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.80 {
				li:conn {
					/2/79/1
					/2/76/5/1
				}
			}
			ha:connection.81 {
				li:conn {
					/2/79/2
					/2/77/1/1
				}
			}
			ha:group.82 {
				uuid=HV4brSYpErb7Smew8cEAAACL;
				li:objects {
					ha:line.1 { x1=228000; y1=76000; x2=224000; y2=76000; stroke=wire; }
					ha:line.2 { x1=224000; y1=76000; x2=224000; y2=56000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.83 {
				li:conn {
					/2/82/1
					/2/76/2/1
				}
			}
			ha:connection.84 {
				li:conn {
					/2/82/2
					/2/78/1/1
				}
			}
			ha:connection.85 {
				li:conn {
					/2/41/11
					/2/76/3/1
				}
			}
			ha:connection.86 {
				li:conn {
					/2/34/8
					/2/76/4/1
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1/2
			print_page=A/4
			title=dual opamp symbol unslotted for SPICE
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
