refdes U??

pinalign left center
pinalign right center
attr device attiny24
attr funcmap attiny24
text_size_mult 0.7

shape box

begin pin Vcc
	num 1
	loc left
end pin

begin pin PB0
	num 2
	loc left
	funcmap
end pin

begin pin PB1
	num 3
	loc left
	funcmap
end pin

begin pin PB3
	num 4
	loc left
	funcmap
end pin

begin pin PB2
	num 5
	loc left
	funcmap
end pin

begin pin PA7
	num 6
	loc left
	funcmap
end pin

begin pin PA6
	num 7
	loc left
	funcmap
end pin



begin pin GND
	num 14
	loc right
end pin

begin pin PA0
	num 13
	loc right
	funcmap
end pin

begin pin PA1
	num 12
	loc right
	funcmap
end pin

begin pin PA2
	num 11
	loc right
	funcmap
end pin

begin pin PA3
	num 10
	loc right
	funcmap
end pin

begin pin PA4
	num 9
	loc right
	funcmap
end pin

begin pin PA5
	num 8
	loc right
	funcmap
end pin

