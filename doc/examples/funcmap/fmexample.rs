ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
			ha:group.1 {
				uuid=9zxyxUYSJTIZuLJrNpIAAAAv;
				li:objects {
					ha:group.1 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAw; loclib_name=attiny24;
						li:objects {
						}
						ha:attrib {
							li:funcmap/ports {
								{ PB0/PB0      -> sigtype=digital; }
								{ PB0/PCINT8   -> sigtype=digital; dir=input }
								{ PB0/XTAL1    -> sigtype=analog;  }
								{ PB1/PB1      -> sigtype=digital; }
								{ PB1/PCINT9   -> sigtype=digital; dir=input  }
								{ PB1/XTAL2    -> sigtype=analog;  }
								{ PB3/PB3      -> sigtype=digital; }
								{ PB3/PCINT11  -> sigtype=digital; dir=input  }
								{ PB3/RESET    -> sigtype=digital; dir=input  }
								{ PB2/PB2      -> sigtype=digital; }
								{ PB2/PCINT10  -> sigtype=digital; dir=input  }
								{ PB2/OC0A     -> sigtype=digital; dir=output }
								{ PB2/INT0     -> sigtype=digital; dir=input  }
								{ PA7/PA7      -> sigtype=digital; }
								{ PA7/PCINT7   -> sigtype=digital; dir=input  }
								{ PA7/OC0B     -> sigtype=digital; dir=output }
								{ PA7/ADC7     -> sigtype=digital; dir=input  }
								{ PA6/PA6      -> sigtype=digital; }
								{ PA6/PCINT6   -> sigtype=digital; dir=input  }
								{ PA6/MOSI     -> sigtype=digital; }
								{ PA6/DI       -> sigtype=digital; dir=input  }
								{ PA6/SDA      -> sigtype=digital; }
								{ PA6/OC1A     -> sigtype=digital; dir=output }
								{ PA6/ADC6     -> sigtype=analog;  dir=input  }
								{ PA0/PA0      -> sigtype=digital; }
								{ PA0/PCINT0   -> sigtype=digital; dir=input  }
								{ PA0/AREF+ADC0-> sigtype=analog;  dir=input  }
								{ PA1/PA1      -> sigtype=digital; }
								{ PA1/PCINT1   -> sigtype=digital; dir=input  }
								{ PA1/AIN0     -> sigtype=analog;  dir=input  }
								{ PA1/ADC1     -> sigtype=analog;  dir=input  }
								{ PA2/PA2      -> sigtype=digital; }
								{ PA2/PCINT2   -> sigtype=digital; dir=input  }
								{ PA2/AIN1     -> sigtype=analog;  dir=input  }
								{ PA2/ADC2     -> sigtype=analog;  dir=input  }
								{ PA3/PA3      -> sigtype=digital; }
								{ PA3/PCINT3   -> sigtype=digital; dir=input  }
								{ PA3/ADC3     -> sigtype=analog;  dir=input  }
								{ PA4/PA4      -> sigtype=digital; }
								{ PA4/PCINT4   -> sigtype=digital; dir=input  }
								{ PA4/SCK      -> sigtype=digital; }
								{ PA4/SCL      -> sigtype=digital; }
								{ PA4/ADC4     -> sigtype=analog;  dir=input  }
								{ PA5/PA5      -> sigtype=digital; }
								{ PA5/PCINT5   -> sigtype=digital; dir=input  }
								{ PA5/MISO     -> sigtype=digital; }
								{ PA5/DO       -> sigtype=digital; dir=output }
								{ PA5/OC1B     -> sigtype=digital; dir=output }
								{ PA5/ADC5     -> sigtype=analog;  dir=input  }
							}
							li:funcmap/strong_groups {
								{ SPI          -> SCK, MISO, MOSI }
								{ I2C          -> SDA, SCL }
								{ USI          -> SCK, DI, DO }
							}
							li:funcmap/weak_groups {
								{ gpio         -> PB0, PB1, PB2, PB3, PA6, PA7, PA0, PA1, PA2, PA3, PA4, PA5 }
								{ pcint        -> PCINT8, PCINT9, PCINT11, PCINT10, PCINT7, PCINT6, PCINT0, PCINT1, PCINT2, PCINT3, PCINT4, PCINT5 }
								{ xtal         -> XTAL1, XTAL2 }
								{ PWM          -> OC0A, OC0B, OC1A, OC1B }
								{ adc          -> AREF+ADC0, ADC1, ADC2, ADC3, ADC4, ADC5, ADC6, ADC7 }
								{ comparator   -> AIN0, AIN1 }
							}
						}
					}
				}
				ha:attrib {
					ha:purpose = { value=funcmap; prio=0; }
				}
			}
		}
	}
	ha:obj_direct.2 {
		uuid=JkQTZjOhkCU1+/zzmokAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.sheet-decor-fill { shape=round; size=125; color=#bbbbbb; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-decor-fill { shape=round; size=125; color=#99ff99; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.1 {
				uuid=8/SYZ/pfBzxyqFA12WQAAAAC; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAg;
				li:objects {
					ha:polygon.11 {
						li:outline {
							ha:line { x1=0; y1=0; x2=80000; y2=0; }
							ha:line { x1=80000; y1=0; x2=80000; y2=20000; }
							ha:line { x1=80000; y1=20000; x2=0; y2=20000; }
							ha:line { x1=0; y1=20000; x2=0; y2=0; }
						}
						stroke=titlebox-frame;
						fill=titlebox-fill;
					}
					ha:line.12 { x1=0; y1=10000; x2=80000; y2=10000; stroke=titlebox-frame; }
					ha:line.13 { x1=40000; y1=10000; x2=40000; y2=0; stroke=titlebox-frame; }
					ha:text.20 { x1=1000; y1=16500; dyntext=0; stroke=titlebox-big; text=TITLE; }
					ha:text.21 { x1=1000; y1=10500; x2=79000; y2=16000; dyntext=1; stroke=titlebox-big; text=%../../A.title%; }
					ha:text.22 { x1=1000; y1=5500; dyntext=0; stroke=titlebox-small; text={PROJECT:}; }
					ha:text.23 { x1=13000; y1=5500; x2=39000; y2=9500; dyntext=1; stroke=titlebox-big; text=%project.name%; }
					ha:text.24 { x1=1000; y1=500; dyntext=0; stroke=titlebox-small; text={PAGE:}; }
					ha:text.25 { x1=10000; y1=500; x2=39000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.page%; }
					ha:text.26 { x1=41000; y1=5500; dyntext=0; stroke=titlebox-small; text={FILE:}; }
					ha:text.27 { x1=48000; y1=5500; x2=79000; y2=9500; dyntext=1; stroke=titlebox-big; text=%filename%; }
					ha:text.28 { x1=41000; y1=500; dyntext=0; stroke=titlebox-small; text={MAINTAINER:}; }
					ha:text.29 { x1=55000; y1=500; x2=79000; y2=4500; dyntext=1; stroke=titlebox-big; text=%../../A.maintainer%; }
					ha:text.30 { x1=79000; y1=16000; mirx=1; dyntext=1; stroke=sheet-decor; text=%stance.model% %stance.sub_major% %stance.sub_minor% %stance.test_bench% %view.name%; }
				}
				ha:attrib {
					purpose=titlebox
				}
			}
			ha:group.2 {
				uuid=9zxyxUYSJTIZuLJrNpIAAAAg; src_uuid=39wCeHrJAKAVyPyQU3EAAAAP;
				x=76000; y=56000;
				li:objects {
					ha:text.1 { x1=-8000; y1=-4000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:polygon.2 {
						li:outline {
							ha:line { x1=0; y1=0; x2=0; y2=32000; }
							ha:line { x1=0; y1=32000; x2=16000; y2=32000; }
							ha:line { x1=16000; y1=32000; x2=16000; y2=0; }
							ha:line { x1=16000; y1=0; x2=0; y2=0; }
						}
						stroke=sym-decor;
					}
					ha:group.3 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAh; src_uuid=39wCeHrJAKAVyPyQU3EAAAAB;
						x=0; y=28000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../A.name%; }
						}
						ha:attrib {
							name=Vcc
							pinnum=1
							role=terminal
						}
					}
					ha:group.4 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAi; src_uuid=39wCeHrJAKAVyPyQU3EAAAAC;
						x=0; y=24000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB0
							pinnum=2
							role=terminal
						}
					}
					ha:group.5 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAj; src_uuid=39wCeHrJAKAVyPyQU3EAAAAD;
						x=0; y=20000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB1
							pinnum=3
							role=terminal
						}
					}
					ha:group.6 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAk; src_uuid=39wCeHrJAKAVyPyQU3EAAAAE;
						x=0; y=16000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB3
							pinnum=4
							role=terminal
						}
					}
					ha:group.7 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAl; src_uuid=39wCeHrJAKAVyPyQU3EAAAAF;
						x=0; y=12000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PB2
							pinnum=5
							role=terminal
						}
					}
					ha:group.8 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAm; src_uuid=39wCeHrJAKAVyPyQU3EAAAAG;
						x=0; y=8000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA7
							pinnum=6
							role=terminal
						}
					}
					ha:group.9 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAn; src_uuid=39wCeHrJAKAVyPyQU3EAAAAH;
						x=0; y=4000; mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA6
							pinnum=7
							role=terminal
						}
					}
					ha:group.10 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAo; src_uuid=39wCeHrJAKAVyPyQU3EAAAAI;
						x=16000; y=28000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../A.name%; }
						}
						ha:attrib {
							name=GND
							pinnum=14
							role=terminal
						}
					}
					ha:group.11 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAp; src_uuid=39wCeHrJAKAVyPyQU3EAAAAJ;
						x=16000; y=24000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA0
							pinnum=13
							role=terminal
						}
					}
					ha:group.12 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAq; src_uuid=39wCeHrJAKAVyPyQU3EAAAAK;
						x=16000; y=20000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA1
							pinnum=12
							role=terminal
						}
					}
					ha:group.13 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAr; src_uuid=39wCeHrJAKAVyPyQU3EAAAAL;
						x=16000; y=16000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA2
							pinnum=11
							role=terminal
						}
					}
					ha:group.14 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAs; src_uuid=39wCeHrJAKAVyPyQU3EAAAAM;
						x=16000; y=12000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA3
							pinnum=10
							role=terminal
						}
					}
					ha:group.15 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAt; src_uuid=39wCeHrJAKAVyPyQU3EAAAAN;
						x=16000; y=8000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							name=PA4
							pinnum=9
							role=terminal
						}
					}
					ha:group.16 {
						uuid=9zxyxUYSJTIZuLJrNpIAAAAu; src_uuid=39wCeHrJAKAVyPyQU3EAAAAO;
						x=16000; y=4000;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=4000; y2=0; stroke=term-decor; }
							ha:text.2 { x1=1000; y1=0; dyntext=1; stroke=term-primary; text=%../a.display/name%; }
							ha:text.3 { x1=-500; y1=-2000; mirx=1; dyntext=1; stroke=term-secondary; text=%../a.funcmap/name%; }
						}
						ha:attrib {
							funcmap/name=MISO
							name=PA5
							pinnum=8
							role=terminal
						}
					}
					ha:text.18 { x1=-8000; y1=-8000; dyntext=1; stroke=sym-secondary; text=%../A.device%; floater=1; }
				}
				ha:attrib {
					-symbol-generator=boxsym-rnd
					device=attiny24
					funcmap=attiny24
					name=U1
					role=symbol
				}
			}
		}
		ha:attrib {
			drawing_min_height=200000
			drawing_min_width=287000
			maintainer=Tibor 'Igor2' Palinkas
			page=1/1
			print_page=A/4
			title=sch-rnd funcmap example
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
