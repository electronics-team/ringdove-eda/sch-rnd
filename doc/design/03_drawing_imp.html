<html>
<body>
<h1> 3. cschem implementation - graphical schematics sheets - drawing primitives </h1>

<h2> Goals </h2>

<p>{imp3:0} The graphical schematics sheet is a generic vector graphics
drawing canvas with enough metadata that cschem can translate them into
concrete objects. The goal is to find a balance between generic vector
drawing and "possible to implement in a consistent way in reasonable time".
A balance that svg for example failed to find. Finding the balance
requires temperance: the goal is not to provide a fully generic vector
graphics language but only to provide enough features for drawing
schematics.

<h2> Dualism #1: independent graphics and meaning  </h2>

<p>{imp3:1} A novel aspect is that instead of telling how concrete objects
look like in graphics, cschem rather separates looks and meanings. That is,
there is no single canonical graphical representation of a hub or a wire. The
user or an implementation (or plugins) can generate a group of random graphics
elements and mark it as being a terminal ("pin").

<p>{imp3:2} Likewise, connections are explicitly specified, without binding
it to any specific graphical representation. A typical examples are:
<ul>
	<li> junction mark: can be a little dot or can be invisible (and the schematics
	would indicate non-junction by little jumper arcs)
	<li> bus graphics, especially on bus-bus or bus-wire junctions
	<li> pin graphics
</ul>

<p>{imp3:3} Gschem notes: this is very different from what gschem did: it
stored objects that were both concrete objects and drawing objects. This
means a pin or a junction dot will always look the same in gschem. Connections
were not marked explicitly but calculated on load, from line endings being
on other lines or pin ends. This made it hard to interpret the file by 3rd
party software, as such a software would need to reproduce the same geometric
calculations (potentially with the same rounding and/or tolerances!) to have
the same connection conclusions.

<p>{imp3:4} Is allowing the user to use different drawing styles desirable?
On one hand, cschem won't enforce the One True Style, which could potentially
lead to different users or implementations using different graphics for
the same construct. On the other hand, this is already happening without cschem
too, and EEs don't seem to struggle understanding schematics of a different
style. Cschem won't struggle understanding them too, as the logical data is
not extracted from the graphical looks.

<p>{imp3:5} An interesting question is how implementation A will handle
the graphics created by implementation B. For example a bus-wire junction:
implementation A may draw a 45 degree white line coming out from the bus,
wire connected to the end of this stub, and add a text with the channel name
next to the stub. The stub line and the text are put in a group and this group
is then used by the connection object's gfx. The data that actually makes
the logical connection is the conn property of the connection. When B
needs to edit this stub, it will not understand why the text is there, so
the graphics will not be properly updated. The user may need to do that manually
or use the bus connection graphics offer by B.

<p>{imp3:6} The reference cschem implementation will set its own drawing
conventions; it's recommended, but not enforced that other implementation
follow these conventions.

<h2> Dualism #2: is that really that novel? </h2>

<p>{imp3:7} In a sense it is not: a symbol is usually a custom set of
drawing primitives. The schematics capture software understands that it's
a symbol, it needs to be drawn as is. Orthogonal to that, the software
also has non-graphical metadata with the symbol (e.g. attributes) so it can
understand the symbol.

<p>{imp3:8} In that sense, a symbol in many software (including gschem)
already shows this dualism. The novel aspect in cschem is that it extends
the same idea to all other object types, e.g. terminals ("pins"), junction
graphics, and even wires and buses.

<p>{imp3:9} Revisiting the alternative styles question: a 4 slot digital
inverter gate is already drawn as 4 separate triangular symbols and as
a single box with 4 or 5 sections. Readers are not confused by the variants.
The same way buses and even junctions have alternative styles on existing
schematics. Cschem merely permits having such styles not by defining
and hardwiring each of them in code, but by separating the looks from the works.

<h2> text object and font  </h2>
<p id="imp3:20">{imp3:20} There are two ways to specify font geometry of a text object:
<ol>
	<li>{imp3:21} bbox based: when x2;y2 are specified; this is useful for
	    fixed text objects placed on the sheet by the user; it's more important
	    that these text objects, especially when multiline, stay in place and
	    don't start to overlap with other objects than to have the exact font
	    size matched.
	<li>{imp3:22} pen height based: when the text object does not have
	    x2;y2, pen height property is used; this is useful when the
	    placement and approximate size of the text is important but
	    bbox matching is not. This is the typical case in symbols.
</ol>


<h2> grouping  </h2>

<p>{imp3:10} Cschem has only one kind of group and allows group-in-group.
The typical use is that any concrete object is represented by a group.
For example a symbol is a group, a terminal ("pin") is a group within
the symbol group. What concrete object a group becomes is controlled
by the role attribute of the group.

<p>{imp3:11} A group can be a reference (group_ref) to another group
specified elsewhere. This is how a symbol from a library can be instantiated.
Since groups and group references can do two basic transformation (shift and
rotate), they can place instances freely. This allows both by-reference
and by-copy instantiation. It's up to the user to decide which one to use.

<p>{imp3:12} Only composites have attributes, atoms don't. This means placing
an atom directly on the sheet can not have attributes. Since groups
are cheap, this can be easily solved by putting the atom in its own group.

<p>{imp3:13} Wire-nets are obviously represented by a group that usually
will contain lines representing wires. A connection objects can
connect two groups by referring to group member drawing objects, e.g. a
line (part of a wire-net group) and another line (part of a terminal group).

<h2> default pens </h2>

<p>{imp3:14} The most trivial use of pens is that a group defines its own
set of pens, hardwiring how the drawing of the group works on the low level.
For example for a group that represents a symbol, it means hardwiring the
style of a "pin" (line thickness and color), thus the symbol always looks
the same, no matter in which schematics sheet it is placed.

<p>{imp3:15} However, it's equally important that symbols from different
source can be combined in a sheet and they look the same. Plus the user should
be able to control, on sheet level the style of the symbols. This is achieved
by using the <a href="03_drawing.html#des3:74">pen fallback mechanism</a>:

<p><ul>
	<li> {imp3:17} there is a <a href="#imp3:19"> table </a> about the <b>conventional</b> pen names all sheets expected to have
	<li> {imp3:18} groups intended to be used on any sheet should mostly use <b>conventional</b> pens without defining them as pens for those purposes described below, falling back to the actual pen definitions in the direct root group; for other purposes groups should define local pens; using uppercase pen names avoids collision with future <b>conventional</b> names
</ul>

<p id="imp3:19"><table border=1 cellspacing=0>
<tr><th colspan=2 bgcolor="#aaaaaa"> {imp3:19} the convention for centrally defined pens
<tr><th> name              <th> usage
<tr><td> sheet-decor       <td> default sheet decoration
<tr><td> titlebox-frame    <td> used for lines of the box
<tr><td> titlebox-fill     <td> background fill of the titlebox
<tr><td> titlebox-big      <td> used for title text
<tr><td> titlebox-small    <td> used for other fields listed above
<tr><td> wire              <td> default wire-net (wiring)
<tr><td> bus               <td> default bus (wiring)
<tr><td> hub               <td> default hubs and connection graphics for wire-nets
<tr><td> sym-decor         <td> default symbol decoration lines
<tr><td> sym-primary       <td> default symbol primary data text (e.g. refdes, value)
<tr><td> sym-secondary     <td> default symbol secondary data text (e.g. devmap, footprint)
<tr><td> term-decor        <td> default terminal decoration lines
<tr><td> term-primary      <td> default terminal primary data text (e.g. number, name)
<tr><td> term-secondary    <td> default terminal secondary data text
<tr><td> busterm-decor     <td> default terminal decoration lines
<tr><td> busterm-primary   <td> default terminal primary data text (e.g. number, name)
<tr><td> busterm-secondary <td> default terminal secondary data text
<tr><td> junction          <td> default wire-net junction mark
</table>




