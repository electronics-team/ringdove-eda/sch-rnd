<html>
<body>
<h1> 7. Device mapping </h1>

<p>{des7:0}
Device mapping is a generic mechanism for detaching schematics
symbols from footprints and simulation models. It allows:
<ul>
	<li> interchangeable symbols and interchangeable footprints/models for
	     the same generic device (e.g. transistor) by decoupling
	     their pin numbers from pin names (problem known as the "transistor problem")
	<li> multiple symbols to represent different parts of one physical
	     footprint or simulation model (problem known as "slotting")
</ul>

<p>{des7:1}
These goals are achieved by using symbolic terminal names (instead
of "pin numbers") in the schematic symbols and using auxiliary data to translate
the terminal names into port numbers. The following diagram demonstrates
the process:

<p><img src="07_diag.svg">

<p>{des7:2}
The default implementation of the device mapper is a plugin called <i>std_devmap</i>.
<code>std_devmap</code> has three major features: slotting name translation
(red), device mapping (green) and port mapping (blue). The slotting translation
makes sure the resulting abstract component has the right amount of ports;
the device mapper loads the device-specific port mapping and other
attributes from a database; the port mapper applies pin numbers from
a table (the table typically set up by the device mapper).

<p>{des7:3}
The standard device mapper is an optional mechanism implemented in a plugin. The
user may choose to use it or to:
<ul>
	<li> not include the device mapper in the view at all
	<li> not using the slotting mechanism described here, on per symbol basis,
	     by not having a <i>slot</i> attribute
	<li> not using the device mapping mechanism described here, on per symbol basis,
	     by not having a <i>devmap</i> attribute
	<li> not using the port mapping mechanism described here, on per symbol basis,
	     by not having a <i>portmap</i> attribute and not using a device map
	     that would introduce a <i>portmap</i> attribute
</ul>

<p>{des7:4}
It is possible to use features of this plugin selectively and/or replace
some or all features using another plugin.

<p>{des7:5}
<code>std_devmap</code> registers for:
<ul>
	<li> the symbol_joined_component hook (for updating attribute from the devmap if the devmap attribute is present)
	<li> the <a href="02_data.html#des2:77">terminal_name_to_port_name translation hook</a> (for setting port names according to devmap)
</ul>

<h2> 7.1. Slot prefixing </h2>

<p>{des7:6}
If the "<code>slot</code>" attribute presents in a symbol, and the terminal does
not have the "<code>noslot</code>" attribute, the translation is activated. The slot
name is taken directly from the concrete symbol's "<code>slot</code>" attribute.
All ports are then named  as "<code>slot/terminal-name</code>" and the slot
attribute is then removed from the abstract component to avoid
collisions.

<p>{des7:7}<a id="des7:7">
Typical example: common opamp symbol:

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des7:9} example opamp terminal attributes
<tr><th> name                    <th> noslot                <th> terminal description
<tr><td> <code>in_plus</code>    <td> (not present)         <td> positive input pin
<tr><td> <code>in_minus</code>   <td> (not present)         <td> negative input pin
<tr><td> <code>out</code>        <td> (not present)         <td> output pin
<tr><td> <code>v_plus</code>     <td> <i>"yes"</i>          <td> positive power rail
<tr><td> <code>v_minus</code>    <td> <i>"yes"</i>          <td> negative power rail
</table>

<p>{des7:10}
In the above example "<code>v_plus</code>" and "<code>v_minus</code>" are never prefixed with "<code>slot/</code>" because
of their "<code>noslot</code>" attribute. In case the symbol is used with the "<code>slot</code>" attribute
set, this means:
<ul>
	<li> "<code>in_plus</code>", "<code>in_minus</code>" and "<code>out</code>" will be copied/duplicated for each slot
	     (e.g. as "<code>A/in_plus</code>" for slot "<code>A</code>") for the abstract component
	<li> "<code>v_plus</code>" and "<code>v_minus</code>" are kept common and only one copy of them will
	     exist in the abstract component
</ul>

<p>{des7:11}
Note: when the same symbol is used in a non-slotted use case, the symbol will
not have the "<code>slot</code>" attribute; in this case no terminal name translation
applied and the resulting abstract component will have the same port names
as the terminal names of the symbol.


<h2> 7.2. Port mapping </h2>

<p>{des7:12}
Port mapping takes a map between the symbolic description and the
physical description (port numbers for a footprint or sim model) and changes
port attributes accordingly. There
shall be one such mapping per abstract component. If multiple symbols
contribute a mapping, the <a href="04_attrib.html#des4:9"> attribute
priority mechanism</a> will keep only one of the arrays.


<p>{des7:13}<a id="des7:13">
The input <a href="04_attrib.html#des4:7">array</a> is an unordered list of (terminal name + slot name)
-&gt; (attribute_name + value) pairs, each specified in the following syntax:
<pre>
slot/termname -&gt; attribkey=value
or
termname -&gt; attribkey=value
</pre>

<p>{des7:14} E.g. "<code>A/out -&gt; pcb/pinnum=1</code>" means "terminal with 
<i>name=out</i> in symbol tagged as <i>slot=A</i> shall get attribute
<i>pcb/pinnum</i> set to <i>1</i>". Equivalent non-slotted example is
"<code>out -&gt; pcb/pinnum=1</code>"


<p>{des7:15}
If a slot/terminal pair referenced in the map is not found in the concrete
symbol, no error message is emitted.
A typical example when this happens is when a logic circuit
has 4 identical slots but the schematics uses only 3.
If this is considered an error, a DRC plugin shall check for it.

<p>{des7:16}
If an existing port of the abstract component is not listed in the mapping,
that is the mapping doesn't reference the port in any way, a warning
is generated. (This happens e.g. on the PCB flow when the schematics
symbol uses a terminal that has no physical pin with the given device.).


<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des7:18} port mapping related symbol attributes
<tr><th> attribute              <th> value                <th> description
<tr><td> <code>portmap</code>   <td> array of map entries <td> the actual map, as <a href="#des7:13"> described above </a>
<tr><td> <code>slot</code>      <td> slot name            <td> textual name of the slot the symbol provides
</table>

<h2> 7.3. Device mapping </h2>

<p>{des7:19}
The device mapper takes the "<code>devmap</code>" attribute of a symbol and looks it up
in a database for finding a device map file. If the "<code>devmap</code>" attribute is an
array, each devmap file is looked up and applied, in order of appearance.

<p>{des7:20}
A device map file is a simple list of cschem attributes. The device mapper
takes each attribute from the device map file and apply it on the symbol that
had the "<code>devmap</code>" attribute.

<p>{des7:21}
A common device map file will usually have a "<code>portmap</code>" attribute that will
set at least one attributes on each terminal.

<p><table border=1 cellspacing=0>
<tr><th colspan=3 bgcolor="#aaaaaa"> {des7:22} device mapping related symbol attributes
<tr><th> attribute              <th> value  <th> description
<tr><td> <code>devmap</code>    <td> name   <td> name of the devmap file to use
</table>

