For concrete molde objects there are fields, typically holding
oid-paths, that have a textual description on input but should be
tracked by object pointers after load.

These fields are stored in a pair of struct fields marked xorcache:
either the text version or the object-pointer version is NULL. After
load, the text is valid, the pointer is NULL. Upon the first access,
the reference is resolved and the code uses the pointer. Whenever
this happens, the text field is free'd and set to NULL.

Export code rebuilds the textual reference from the pointer.
