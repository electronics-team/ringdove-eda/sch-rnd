#!/bin/sh

ROOT=..

if test -z "$LIBRND_LIBDIR"
then
	# when not run from the Makefile
	LIBRND_LIBDIR=`cd developer/packaging && make -f librnd_root.mk libdir | grep -v make[[]`
fi

proot=$ROOT/src/plugins
. $LIBRND_LIBDIR/devhelpers/awk_on_formats.sh

### sch-rnd specific categories ###
type_detect_begin='
	types = types " schematics symbol"
'

type_detect_awk='
	if (lname ~ "schematics?")
		type = "schematics"
	if (lname ~ "symbol")
		type = "symbol"
'

### output ###
awk_on_formats  '
($1 == "<!--") && ($2 == "begin") && ($3 == "fmt") { ignore = 1; print $0; next }
($1 == "<!--") && ($2 == "end") && ($3 == "fmt") {
	print FMTS[$4, $5]
	ignore = 0;
	print $0
	next
}

(!ignore) { print $0 }
' < datasheet.html > datasheet2.html && mv datasheet2.html datasheet.html
