ha:cschem-sheet-v1 {
	ha:obj_indirect.1 {
		li:objects {
		}
	}
	ha:obj_direct.2 {
		uuid=xsOiQL0BalAZ8gK7fkMAAAAC;
		li:objects {
			ha:pen.sheet-decor { shape=round; size=125; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-frame { shape=round; size=250; color=#777777; font_height=0; }
			ha:pen.titlebox-fill { shape=round; size=250; color=#bbffbb; font_height=0; }
			ha:pen.titlebox-big { shape=round; size=250; color=#777777; font_height=3000; font_family=sans; }
			ha:pen.titlebox-small { shape=round; size=250; color=#777777; font_height=1500; font_family=sans; }
			ha:pen.wire { shape=round; size=250; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.bus { shape=round; size=1500; color=#2222bb; font_height=3000; font_family=sans; }
			ha:pen.hub { shape=round; size=3000; color=#6666ff; font_height=3000; font_family=sans; }
			ha:pen.sym-decor { shape=round; size=125; color=#119911; font_height=3000; font_family=sans; }
			ha:pen.sym-primary { shape=round; size=125; color=#119911; font_height=12000; font_family=sans; font_style=bold; }
			ha:pen.sym-secondary { shape=round; size=125; color=#33bb33; font_height=3000; font_family=sans; }
			ha:pen.term-decor { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.term-primary { shape=round; size=250; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.term-secondary { shape=round; size=250; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.busterm-decor { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; }
			ha:pen.busterm-primary { shape=round; size=1500; color=#222222; font_height=3000; font_family=sans; font_style=bold; }
			ha:pen.busterm-secondary { shape=round; size=1500; color=#555555; font_height=3000; font_family=sans; }
			ha:pen.junction { shape=round; size=1000; color=#2222bb; font_height=3000; font_family=sans; }
			ha:group.2 {
				uuid=xsOiQL0BalAZ8gK7fkMAAAAP; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAw;
				x=0; y=16000;
				li:objects {
					ha:group.1 {
						uuid=xsOiQL0BalAZ8gK7fkMAAAAQ; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAx;
						x=20000; y=0;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							name=2
							role=terminal
						}
					}
					ha:group.2 {
						uuid=xsOiQL0BalAZ8gK7fkMAAAAR; src_uuid=iNOQfJpO6hT/HFDFGjoAAAAy;
						mirx=1;
						li:objects {
							ha:line.1 { x1=0; y1=0; x2=-4000; y2=0; stroke=term-decor; }
						}
						ha:attrib {
							name=1
							role=terminal
						}
					}
					ha:text.4 { x1=3500; y1=-15000; dyntext=1; stroke=sym-primary; text=%../A.name%; floater=1; }
					ha:arc.5 { cx=7000; cy=0; r=2000; sang=-40.000000; dang=220.000000; stroke=sym-decor; }
					ha:arc.6 { cx=13000; cy=0; r=2000; sang=0.000000; dang=220.000000; stroke=sym-decor; }
					ha:arc.7 { cx=10000; cy=0; r=2000; sang=-40.000000; dang=260.000000; stroke=sym-decor; }
					ha:line.8 { x1=4000; y1=0; x2=5000; y2=0; stroke=sym-decor; }
					ha:line.9 { x1=15000; y1=0; x2=16000; y2=0; stroke=sym-decor; }
				}
				ha:attrib {
					-sym-copyright=(C) 2022 Tibor 'Igor2' Palinkas
					-sym-license-dist=GPLv2+
					-sym-license-use=Public Domain
					-sym-source=sch-rnd default symbol lib
					name=rnd
					role=symbol
					value=<n/a>
				}
			}
			ha:group.3 {
				uuid=xsOiQL0BalAZ8gK7fkMAAAAS;
				li:objects {
					ha:line.1 { x1=0; y1=16000; x2=0; y2=0; stroke=wire; }
					ha:line.4 { x1=0; y1=0; x2=20000; y2=0; stroke=wire; }
					ha:line.5 { x1=20000; y1=0; x2=20000; y2=16000; stroke=wire; }
				}
				ha:attrib {
					ha:role = { value=wire-net; prio=0; }
				}
			}
			ha:connection.11 {
				li:conn {
					/2/3/1
					/2/2/2/1
				}
			}
			ha:connection.12 {
				li:conn {
					/2/3/5
					/2/2/1/1
				}
			}
		}
		ha:attrib {
			maintainer=<maint. attr>
			page=<page attr>
			print_page=A/4
			title=<please set sheet title attribute>
		}
	}
  li:sch-rnd-conf-v1 {
   ha:overwrite {
    ha:editor {
     grids_idx = 2
     grid = 4.0960 mm
    }
   }
  }
}
