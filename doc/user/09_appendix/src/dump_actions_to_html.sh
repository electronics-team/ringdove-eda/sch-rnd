#!/bin/sh

# collates the sch-rnd action table into a html doc page

asrc="../action_src"
lsrc="librnd_acts"

. $LIBRND_LIBDIR/dump_actions_to_html.sh

cd ../../../../src/sch-rnd
sch_rnd_ver=`./sch-rnd --version`
sch_rnd_rev=`svn info ^/ | awk '/Revision:/ {
	print $0
	got_rev=1
	exit
	}
	END {
		if (!got_rev)
		print "Rev unknown"
	}
	'`
cd ../../doc/user/09_appendix/src

print_hdr "sch-rnd"

(
	cd ../../../../src/sch-rnd
	./sch-rnd --dump-actions 2>/dev/null
) | gen
