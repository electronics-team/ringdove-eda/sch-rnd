#!/bin/sh

# convert table description to html

awk -F "[\t]" '

BEGIN {
	q="\""
}

function open_tbl(    n)
{
	if (is_open) return

	print "<table border=1 cellspacing=0>"
	if (anchor != "")
		print " <tr id=" q anchor q ">"
	else
		print " <tr>"
	print "  <th colspan=" q cols q ">" name

	print " <tr>"
	for(n = 0; n < cols; n++)
		print "  <th> " COL[n]

	is_open=1
}

function close_tbl()
{
	if (!is_open) return
	print "</table>"
}

/^[ \t]*#/ { next }

($1 == "@sect") {
	close_tbl();
	$1=""
	print "<h2>" $0 "</h2>"
	next
}

($1 == "@table") {
	close_tbl();
	name = $2
	is_open = 0
	anchor = ""
	cols = 0
	next
}

($1 == "@anchor") { anchor = $2; next }

($1 == "@cols") {
	cols = 0
	for(n = 2; n <= NF; n++) {
		COL[cols] = $n
		cols++
	}
	next
}

(NF > 1) {
	open_tbl()
	print " <tr>"
	for(n = 1; n <= NF; n++)
		print "  <td> " $n
}

END {
	close_tbl()
}
'
