<html>
<body>

<H2 autotoc="yes"> Funcmap </H2>

<H3 autotoc="yes"> Introduction </H3>
<p>
Some integrated circuits like microcontrollers and embedded processors
have more features than physical pins and use internal multiplexers to
configure which physical pin should serve which functionality (peripheral).
<p>
There are different ways to cope with this on the schematics without extra
features:
<ul>
	<li> list all functions in the terminal label
	<li> the above, and let the user draw some marking around the active function's name
	<li> better yet, the user could edit the symbol, change the terminal label, to mark the active function or even remove the inactive ones
</ul>
<p>
However these are time consuming procedures and prone to error as some
functionality are in group, e.g. an SPI typically requires a clock and at
least a MISO or a MOSI (but usually both). With the above manual processes
the software is not doing any verification whether function-terminal assignments
are valid (the given terminal indeed has the given function) or whether a
group of functions, like SPI, have all relevant terminals marked.
<p>
The funcmap plugin provides an automated solution to the majority of the common
cases. It works a bit similar to devmap: (alternate pin) function maps,
or <i>funcmaps</i> for short, are written by the user in a text file on
a per part basis, collected in a funcmap library. Then the file name of
the <i>funcmap</i> is set in an attribute in one of the symbols
(contributing to a component).
<p>
During compilation the component inherits
the attribute and the plugin loads and caches the funcmap file into sheet
local library (so the sheet file is self-contained without the external
funcmap lib shipped with it). Then the plugin looks at the attributes of
each port of the component to compute a funcmap name to display - this is
how the alternate function is applied. Instead of a hardwired string, the
symbol shall print the computed funcmap name attribute. This way, after
a successful compilation, the sheet shows the dynamically calculated,
verified alternate function of each port.
<p>
Furthermore CLI actions and GUI dialog boxes are provided to overview and
edit and verify the consistency of the per terminal function selection.

<H3 autotoc="yes"> Details </H3>
<p>
Further details are sorted into the following sub-sections:
<ul>
	<li> <a href="#format"> Funcmap data model and file format </a>
	<li> <a href="#attr"> Attribute usage summary </a>
	<li> <a href="#lib"> Funcmap library </a>
	<li> <a href="#cli"> CLI: actions </a>
	<li> <a href="#gui"> GUI: dialog boxes and menus </a>
	<li> <a href="#drc"> DRC: design rule checks </a>
	<li> <a href="#boxsym"> boxsym-rnd aspects </a>
	<li> <a href="#example"> Example: using the system in practice </a>
</ul>

<H4 autotoc="yes"> Funcmap data model and file format </H4>
<p id="format">
A funcmap operates on the abstract model. It lists alternate functions
of each port and optionally groups those functions in weak and strong
function groups.
<p>
A typical example is a microcontroller (MCU) that can have the same port
act as a GPIO, as an analog input, a PWM output or part of a sync serial
device. The function is selected runtime by software.
<p>
As an example for such a situation consider 3 ports of the attiny24
(full <a href="#example">example</a> for all pins is also available). Below
is the stripped down funcmap file from the external library:
<pre>
ha:funcmap.v1 {
  ha:comp_attribs {
    li:funcmap/ports {
      { PB6/PB6      -> sigtype=digital; }
      { PB6/PCINT6   -> sigtype=digital; dir=input  }
      { PB6/MOSI     -> sigtype=digital; }
      { PB6/DI       -> sigtype=digital; dir=input  }
      { PB6/SDA      -> sigtype=digital; }
      { PB6/OC1A     -> sigtype=digital; dir=output }
      { PB6/ADC6     -> sigtype=analog;  dir=input  }

      { PA4/PA4      -> sigtype=digital; }
      { PA4/PCINT4   -> sigtype=digital; dir=input  }
      { PA4/SCK      -> sigtype=digital; }
      { PA4/SCL      -> sigtype=digital; }
      { PA4/ADC4     -> sigtype=analog;  dir=input  }

      { PA5/PA5      -> sigtype=digital; }
      { PA5/PCINT5   -> sigtype=digital; dir=input  }
      { PA5/MISO     -> sigtype=digital; }
      { PA5/DO       -> sigtype=digital; dir=output }
      { PA5/OC1B     -> sigtype=digital; dir=output }
      { PA5/ADC5     -> sigtype=analog;  dir=input  }
    }

    li:funcmap/weak_groups {
      { gpio         -> PB6, PA4, PA5 }
      { pcint        -> PCINT4, PCINT5, PCINT6 }
      { PWM          -> OC1A, OC1B }
      { adc          -> ADC4, ADC5, ADC6 }
    }

    li:funcmap/strong_groups {
      { SPI          -> SCK, MISO, MOSI }
      { I2C          -> SDA, SCL }
      { USI          -> SCK, DI, DO }
    }
  }
}
</pre>
<p>
The file is a lihata document with the root node ha:funcmap.v1. It hosts
a single ha:comp_attribs subtree (similar to devmap), to indicate that
the subsequent attributes are component-level (even tho the funcmap plugin
will not actually create these attributes in the components).
<p>
The li:funcmap/ports attribute is a list of port functions. Each line
is a <b>port/function</b> -> <b>port attributes</b> pair. Function names
are arbitrary. Once the given function is activated for a port, the funcmap
plugin will set all port attributes listed on the right side of the arrow.
As a convention the first function shall match the port name; this is going
to be the primary function of the port which is also used as a fallback
when the user does not activate any function for a port. The typical
port attributes to set are <i>sigtype</i> and <i>dir</i> (see
<a href="http://repo.hu/projects/coraleda/std/crl002_attr.html">coraleda
std 002</a>), but any port attribute may be set.
<p>
The li:funcmap/weak_groups attribute is a list of groups for displaying
purposes. Each line is function group in the form of <b>group_name</b> ->
<b>function_list</b>. Group names are arbitrary but must be unique within
a funcmap file. A function_list is a comma separated list of function names.
(Function names must be defined in the li:funcmap/ports section.)
<p>
In a tabular display each group is a column (while ports are
rows):
<pre>
&lt;port&gt; gpio    pcint  PWM      adc  SPI  I2C USI &lt;no-group&gt;
PB6    [[PB6]] PCINT6 OC1A     ADC6 MOSI SDA DI  -
PA4    [[PA4]] PCINT4 -        ADC4 SCK  SCL SCK -
PA5    PA5     PCINT5 [[OC1B]] ADC5 MISO -   DO  -
</pre>
<p>
The function wrapped in [[]] is the one activated for the given port.
<p>
li:funcmap/strong_groups has the same format and display properties as
weak_groups. The difference is that a strong group is also
<a href="#drc">checked in the DRC for consistency.</a>
<p>
In the above table the &lt;port&gt; column holds raw port names; functions
start from the second column. The last column is always called
&lt;no-group&gt; and holds any function that is not referenced by any of the
function groups. Note: the same function may be referenced from multiple
groups (like SCK in this case), but it's generally best practice to
avoid that.

<H4 autotoc="yes"> Attribute usage summary </H4>
<p id="attr">
Component attribute: <b>funcmap</b>. Specifies the name of the
funcmap to use. This is the file name in the external library, without
the .funcmap suffix. The attribute is typically specified in the symbol.
If the component is compiled from multiple symbols, the attribute should
be specified only in one of the symbols. If not specified, the component
is not affected by funcmap.
<p>
Port attribute: <b>funcmap/name</b> (typically specified in a terminal).
Specifies the function the given port should take. Shall be one of the
functions available for the given port. Shall be used only for ports in
components with the funcmap attribute set. If a port in a component with
the funcmap attribute does not specify its funcmap/name, it is computed
by the funcmap plugin (the port name is copied into the funcmap/name attribute).
Thus funcmap/name always contains the valid function name used by the port
(of a funcmap capable symbol) and the symbol should print the
dyntext %../a.funcmap/name% for pin label.


<H4 autotoc="yes"> Funcmap library </H4>
<p id="lib">
Funcmap files are loaded from external libs. External lib paths
are configured in the conf node plugins/funcmap/search_paths, which
has the same syntax as the symbol library.
<p>
Once a funcmap is referenced (from the <i>funcmap</i> attribute of
a component, typically coming from the <i>funcmap</i> attribute of a
symbol), the plugin loads the file from the external lib and places it
in the sheet local library. This keeps sheets self-contained and portable,
not relying on external files.
<p>
Both the sheet local library and the external library can be browsed
using the funcmap library dialog. The sheet local library can be
flushed (removed) from the File/Maintenance menu; upon the next compilation
all funcmap references are imported into the sheet local library from the
external libraries again. This is how external library changes can be
applied to a sheet.


<H4 autotoc="yes"> CLI: actions </H4>
<p id="cli">
The generic way to manipulate funcmaps is simply editing
symbol and terminal attributes, e.g. using the propedit() action. The
funcmap system depends only on funcmap files and attributes, there are
not GUI-only aspects.
<p>
The specialized CLI actions to manage funcmaps (manually or from scripts)
are:
<ul>
	<li> <b><a href="../../09_appendix/action_details.html#funcmapprinttable">
	     FuncmapPrintTable</a></b> that prints the funcmap state table for a
	     component to stdout (same table that the component funcmap dialog
	     presents)
	<li> <b><a href="../../09_appendix/action_details.html#funcmapchange">
	     FuncmapChange</a></b> that can change port function selection in
	     a verified way and can select function for multiple ports by
	     a function group.
</ul>
<p>
These actions work only if the project is compiled.

<H4 autotoc="yes"> GUI: dialog boxes and menus </H4>
<p id="gui">
Quick attribute edit for selecting the funcmap for a symbol is available
in the attribute editor, once the funcmap attribute is selected. It opens
the funcmap library dialog (also accessible from the window menu) which
allows browsing the external and sheet local funcmap libs for picking a
funcmap.
<p>
The per component funcmap summary table dialog can be invoked from the right
click context menu of symbols. This dialog box presents the tabular view
of all ports (rows) and functions (columns). Once a port is selected,
the function of that port can be easily stepped through its available
functions. Alternatively the function can also be set by explicitly
selecting one of the alternate functions (using the change button).
There's a button for activating the whole function group that matches
the currently selected port's function. Note: this dialog box does not
refresh automatically for changes made elsewhere; if there are relevant
changes in the drawing or attributes and the dialog box is still open,
click the refresh button.
<p>
The per port function selector can be invoked using the right click context
menu of a terminal. The dialog popping up allows selecting one of the
possible functions the port may have.
<p>
These dialog boxes work only if the project is compiled.

<H4 autotoc="yes"> DRC: design rule checks </H4>
<p id="drc">
DRC checks are ran automatically as part of the normal sch-rnd DRC process,
unless it is disabled using the conf node <b>plugins/funcmap/drc_disable</b>.
<p>
The funcmap DRC check verifies that each component with an active funcmap
has all strong function groups properly utilized. For each function (of
a function group) there are three possible states:
<ul>
	<li> <i>unused</i>: the function is not assigned by any port
	<li> <i>single</i>: the function is used by only one strong function group and is assigned to a port
	<li> <i>multi</i>: the function is used by multiple strong function groups and is assigned to a port
</ul>
<p>
A function group is valid if and only if:
<ul>
	<li> all of its functions are <i>unused</i> or <i>multi</i>
	<li> all of its functions are <i>single</i> or <i>multi</i>
</ul>
<p>
If a strong function group has some functions <i>unused</i> while other
functions <i>single</i> used, that means the function group is partly
utilized, which is an error that is reported as a DRC violation.
<p>
For example on the <a href="#example">attiny24</a>, there are two
very similar sync serial devices are available with shared
ports: SPI and USI. SPI has SCK, MISO and MOSI while USI has SCK, DI and DO.
Both SPI and USI are strong function groups. Considerations when running the
verification for the SPI function group:
<ul>
	<li> SCK is present in both SPI and USI using the same name; if it is assigned to a port, it's <i>multi</i> state, otherwise it is in <i>unused</i>state
	<li> If MISO is assigned but MOSI is not, that is a violation
	<li> If MOSI is assigned but MISO is not, that is a violation
	<li> If MISO and MISO are assigned but SCK is not, that is a violation
	<li> If SCK is assigned but neither MISO nor MOSI, that is not reported as a violation (because SCK is <i>multi</i> so it is assumed it'd be used for another group, e.g. USI - but it is not verified while checking on SPI)
</ul>
<p>
Thus either all three functions of SPI must be activated, or none of them;
except for SCK, which may be activated (for the function group USI).

<H4 autotoc="yes"> Example: using the system in practice </H4>
<p id="example">
A full example is provided in <a href="../../../examples/funcmap">
doc/examples/funcmap</a>. It consists of a boxsym-rnd generated
monolithic symbol and the corresponding funcmap file. Ports Vcc and GND
are not mentioned in the funcmap file since those ports are single-function.

<H4 autotoc="yes"> boxsym-rnd aspects </H4>
<p id="boxsym">
The symbol for MCUs and CPUs are typically generated using a tool, such
as boxsym-rnd. To play nicely with funcmap, the only requirement is that
the tool generates the affected terminal labels to use dyntext
%../a.funcmap/name%. In boxsym this is done when the funcmap property
(without any argument) is added to a pin block.
<p>
Boxsym-rnd does not generate the funcmap file; the funcmap file shall be
written manually. It is common that an MCU or CPU symbol is a heavy symbol
that works with only one funcmap. In that case the funcmap attribute could
be specified on component level (in the global section of the boxsym
input file).



</body>
</html>
