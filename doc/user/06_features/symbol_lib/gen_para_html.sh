#!/bin/sh

echo '
<html>
<body>
<h1> sch-rnd standard lib parametric symbols </h1>
<ul>
'

for fn in "$@"
do
	tmp=${fn##*/}
	sym=${tmp%%.html}
	echo "<li> <a href=\"$fn\"> $sym() </a> "
done

echo '
	</ul>
	</body>
	</html>
'