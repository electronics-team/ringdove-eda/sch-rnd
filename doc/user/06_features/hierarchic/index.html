<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <title> sch-rnd hierarchic </title>
  <meta http-equiv="Content-Type" content="text/html;charset=us-ascii">
</head>
<body>

<H2 autotoc="yes"> sch-rnd hierarchic design </H2>

<p>
This is an user guide; the formal specification of the feature is
documented in the design doc as 
<a href="../../../design/06_hierarchy.html"> feature design </a> and
<a href="../../../design/06_hierarchy_imp.html"> implementation notes </a>.

<p>
After a brief intro, this document splits use cases in three categories. The
<i>basic</i> category contains the most trivial use cases with an intro on
how to set up a hierarchic design. The intermediate category contains more
advanced approaches, still limiting addressing to two different types. Finally
the expert category discusses usage that exploits the full potential of
sch-rnd but requires deeper understanding.

<p>
Note: all examples are multisheet with an explicit project file so project.lht
needs to be loaded in sch-rnd.

<h3 autotoc="yes"> Non-hierarchic cases </h3>
<p>
A non-hierarchic design has a single sheet or a set of sheets, with or
without a project file. The latter is also called <i>flat multisheet</i>
design.
<p>
In a non-hierarchic case every component and network are in a single
project global namespace. In practice this means:
<ul>
	<li> if two or more concrete symbols have the same name, they are merged into a single abstract component
	<li> if two or more concrete nets have the same name, they are merged into a single abstract net
</ul>

<h3 autotoc="yes"> Hierarchic design intro </h3>
<p>
In a hierarchic design a schematic sheet contains a symbol that represents
a subsheet. This symbol is called the <i>sheet reference symbol</i> or
<i>sheet ref sym</i> for short; the sheet it is referring to is called the
child sheet. The sheet ref sym may have zero or more terminals and the
child sheet must have the same number of terminals placed directly on the sheet.
(In practice these terminals are typically placed from the symbol library; they
are called misc/shterm_* and they are not really symbols but terminals.)
<p>
The abstract model is produced by the compiler by compiling each root sheet
of the current project. Root sheets are all sharing the common namespace at
the <b>hierarchy root</b>. When compiling a sheet, the sheet ref sym is
detected and the compiler recurses into compiling the child sheet into a
<b>new level of hierarchy</b>. The terminals of the sheet ref sym are
connected to the terminals of the child sheet.
<p>
Child sheets can contain sheet ref syms to further child sheets, making up
a multi-level hierarchy. The only restriction is that the hierarchy needs to
be free of loops (circular references).

<h3 autotoc="yes"> Scopes, addressing </h3>

<h4 autotoc="yes"> Basic: using only the project global scope </h4>
<p>
In the simplest setup each child sheet is used only once. There are two
ways to make connections across sheets:
<ul>
	<li> by naming nets and components in any sheet; they all end up in the project global namespace (also called global namespace)
	<li> by connecting the sheet ref sym's terminal on the parent sheet
</ul>
<p>
The same child sheet can not be referred twice because it would create the
same components and networks again.
<p>
This is a typical setup for the so called "CPU board" case, where a top
level (root) sheet acts as a block diagram connecting terminals of sheet ref
syms of a single level of sheets, each realizing a different section of
the design, as shown in this example:
<p>
<a href="../../../examples/hierarchic/10_cpu/">10_cpu example circuit</a>
<p>
A large CPU is not specified in a single symbol but is split up into a set
of different "slot" symbols by functional blocks (this is called heterogeneous
slotting). At the end all these slot symbols are merged into the same final
abstract component referencing the whole CPU, because they all
have the same name (U1). Global nets, such as gnd and Vcc are easy to
address simply by referencing them by name - this happens in the stock
gnd and Vcc rail symbols.
<p>
The sheet reference symbol is manually crafted, one symbol for each child
sheet. Such symbols can be constructed right on the root sheet using the
graphical editor, or from text files using the utility boxsym-rnd. The sheet
ref sym has an attribute called <i>cschem/child/name</i> which holds the
name of the child sheet (with or without the .rs ending). The top sheet
must be added to the project file as a root sheet and all child sheets must
be added to the project file as an aux sheet. The project file needs to be
loaded when running sch-rnd. This ensures all sheets are available and can be
found by name and the project is self-contained (if the directory is packed up
and shared, it will load on anyone else's system as well).
<p>
The extra functionality that this setup provides over the plain flat multisheet
setup is that the same child sheets can be used by a different project in a
slightly different way by doing the wiring between sheet ref syms on the
project-specific top (root) sheet differently.

<h4 autotoc="yes"> Intermediate: sheet-local objects </h4>
<p>
Hierarchy can also be used to produce repetitive parts. In this (oversimplified)
<a href="../../../examples/hierarchic/20_led/">20_led example circuit </a>
the root sheet contains a
microcontroller that is controlling two LEDs using PWM. The "power
electronics" of driving the LED is the same on both channels so it makes
sense to draw it only once and reference it twice from the root sheet by
simply placing the sheet ref symbol twice, using different name for each
instance.
<p>
To avoid the two instances of the child sheet creating the same
components and nets twice, there is a mechanism for creating objects in the
<i>sheet local</i> scope. This is done by prefixing the component or net name
by ./ (a dot and a slash). This prefix is removed by the compiler and the
name is transformed into something unique - by default this is done prepending
the <i>hierarchic path</i>, which is a slash separated list of sheet ref sym
names from root to the given object. This is how Q1 becomes two FETs:
S1/Q1 and S2/Q1. This system works side by side with the basic
mechanism from the previous chapter:
<ul>
	<li> the gnd symbol still addresses the ground as GND, without the
	    ./ prefix, which means it's the same GND as the parent sheet's GND;
	<li> the control signals pwm1a and pwm1b are connected (and named) on the
	     root sheet and are bound through the sheet ref sym's terminal to
	     the child sheet's terminal; same applies tot he V9V power supply net.
	<li> local networks: ./res_led and ./led_fet; these need to be prefixed
	     so that they don't become global nets shared among all channels
</ul>
<p>
So all in all, the intermediate approach uses two different addressing
methods: the ./ name prefix to keep something sheet-local; and
names with no prefix, which are called <i>automatic</i>, which simply end
up being global. (At least in the intermediate use case).

<h4 autotoc="yes"> Intermediate: component addressing </h4>
<p>
In case of the classic "CPU board" example, the intention is that all the U1
symbols are merged together into a single big CPU component. In case of the
LED example, the same Q1 on different child sheet instances should be different
FETs. This problem is the very same problem as with the networks, and the
solution is the same: name prefixing. In fact, components have the same
name prefixes and the same behavior as nets.
<p>
In the actual examples, this was achieved:
<ul>
	<li> in the 10_cpu example by simply using U1 for symbol name, with no prefix
	<li> in the 20_led example by using ./Q1, ./D1 and ./R1 for symbol names (./ is the sheet-local prefix)
</ul>
<p>
This rule of network and symbol name prefixes work the same is true for
the more advanced use cases, for the rest of this document as well, although
the document will use networks to demonstrate different tricks.

<h4 autotoc="yes"> Intermediate: more on final names </h4>
<p>
A slightly modified version of the same example is 
<a href="../../../examples/hierarchic/22_led/"> 22_led</a>.
This removes the net name pwm1a in the top sheet but the control
net is named on the child sheet. In this case the same number of networks
are generated and generally the same abstract model is produced, but the
network that was called pwm1a in the previous example becomes S2/gate,
because that's the second best name sch-rnd could find.
<p>
In case the child sheet did not name the control net, a globally unique
anon net name would have been generated for this net.

<h4 autotoc="yes"> Intermediate: passing down global nets </h4>
<p>
Another modification of the original LED example is 
<a href="../../../examples/hierarchic/24_led/"> 24_led</a>.
This introduces a common enable signal, controlled by the
microcontroller, that can cut off both LEDs at once. To avoid having to
have an extra terminal on the both sheet ref syms and having to draw the
enable signal to both syms, child sheets simply refer to the <i>blink_enable</i>
net without the ./ prefix, which means this is a global scope net and all three
sheet instances will join its references into a single net.
<p>
This is the same mechanism that the GND symbol uses.

<h4 autotoc="yes"> Expert: subtree scope </h4>
<p>
The intermediate setup covers the situation of reusable single-sheet
modules, locking internal networks and components into the sheet local scope
using the ./ name prefix. In some cases the reusable module is large enough
that it needs to be a multi-sheet hierarchy. In that case there should be
a single root sheet of the module that references all other sheet of
the same module using sheet ref syms. The module is then instantiated
by a bigger design by:
<ul>
	<li> listing all the module files as aux sheets in the project file
	<li> having a sheet ref sym referencing to the root sheet of the module
</ul>
<p>
(The bigger design is generally not interested in the internals of the
multi-sheet module, the interface ("API") between the module and the
bigger design should be the terminals of the module's root sheet and a few
well documented global nets, such as GND.)
<p>
When a common signal needs to be distributed among all subsheets within the
module the most trivial solution is to create a new terminal on all the
sheet ref syms and do the wiring on the module's root sheet. However this
leads back to the same problem of having too many terminals and too many wires
on the module's root sheet. It would be easier to use a global net that is
accessible from all the module's sheets. However, using a true global net
for this purpose is a bad idea, as it may unintentionally merge with the same
named global net in the bigger design or with the same named global net of
a second instance of the same module that should have been independent.
<p>
The solution is using subtree local objects (nets and components). This works
by declaring the object (net or component) on the module's root sheet using
a v/ prefix to the name. The v/ prefix means this object is mergable only
downward in the hierarchy so only subsheets of this module, all somewhere
under the module's root sheet will see the object, but it will not
merge with anything above the module's subtree.
<p>
In example <a href="../../../examples/hierarchic/32_subtree/">32_subtree</a>
an extension of the 24_led example: the whole led control section with the
microcontroller and the two LEDs are packed into a subtree. The root of the
subtree is led_ctrl.rs and is almost the same circuit as main.rs in
24_led. The change is that the 2-channel MCU controlled LED subtree is now
commanded through SPI.
<p>
A new root sheet is added on top, called main.rs, which adds U5, the main
microcontroller, then creates two instances of the led control subtree and
hooks up U5's SPI to control both led_ctrl microcontrollers (using different
slave select signals).
<p>
Sheet instance tree will look like:
<pre>
main.rs (SPI master)
 |
 +- S1: led_ctrl.rs (SPI slave 2 channel LED control)
 |  |
 |  +- S1/S1: led.rs (LED and FET)
 |  |
 |  +- S1/S2: led.rs (LED and FET)
 |
 +- S2: led_ctrl.rs (SPI slave 2 channel LED control)
    |
    +- S2/S1: led.rs (LED and FET)
    |
    +- S2/S2: led.rs (LED and FET)
</pre>

<p>
The expert feature, subtree net, can be observed on sheet led_ctrl.rs. It's
the v/blink_enable net. This used to be a global net in example 24_led, so
that it didn't need to have a dedicated terminal on the sheet ref syms. In
this example we need the same, except that blink_enable can not be fully global
because then the two led_ctrl sheets (S1 and S2) would share the same led_ctrl
net. What is needed instead is a <i>subtree local network</i>, which means that
on the level of S1 and S2 these are two separate networks, but in the subtree
under S1 and in the other subtree under S2 the network is accessible without
having to use sheet terminals.
<p>
The practical implementation is:
<ul>
	<li> on the top of the subtree, on led_ctrl.rs, the net is referenced with the
	     v/ prefix, which means it's a subtree net: visible downward but isn't merged
	     upward
	<li> on subsheets under led_ctrl.rs, the net is referenced with the ^/ prefix,
	     which means the net is search upward, preferring a subtree local net and
	     not a project global nets.
	<li> the rest of the nets are the same as before:
	<ul>
		<li> global nets like Vcc or gnd or V9V
		<li> sheet local nets like ./res_led (which will have 4 instances, named like S1/S2/res_led)
		<li> nets passed through sheet ref sym terminals to the child sheet's sheet terminal
	</ul>
</ul>
<p>
Note: on led_ctrl.rs it's possible to name the blink_enable net without any prefix
and the example will still work. This is because of the search preference of
the <i>auto</i> scope that is used when the name is not prefixed: it will
first search sheet local nets to bind to then starts ascending level by level
looking for a subtree local net, bumping into the same v/blink_enable on
led_ctrl.rs. However, if the search fails (e.g. led_ctrl.rs fails to offer
a subtree local net named blink_enable), the <i>auto</i> will silently create
a global net whereas the ^/ prefixed name will throw an error, which is a
safer choice.


<h4 autotoc="yes"> Expert: scope summary and explicit global scoping </h4>
<p>
A full list of hierarchic name prefixes (and their scopes) can be
found in the <a href="../../../design/06_hierarchy.html#des6:8">design document.</a>
<p>
The system is designed so that:
</ul>
	<li> the <i>auto</i> scope, when no prefix is prepended to the net name,
	     usually does what's expected, searching from local to global, then if
	     no existing net of the same name is found, creating a new global net.
	     This is the only addressing used in the flat (non-hierarchic) case and
	     the most commonly used addressing for non-local objects in the
	     hierarchic case
	<li> it offers an easy to use ./ prefix, which is the most commonly used
	     local scope prefix in the hierarchic case, locking the given object
	     into the current sheet
	<li> offers a less often used subtree local mechanism for real complicated case,
	     prefixed with v/ for "search downward" or ^/ for "search upward"
	<li> offers the exotic / prefix for explicitly addressing the global scope.
<p>
The explicit global scope prefix can be used in the real exotic case when
a (near) bottom child sheet needs to address a project global net not being sure
there was no same named sheet local or subtree local net in the hierarchy.
The <i>auto</i> scope (no prefix) or the ./ or ^/ prefixes would pick up those
intermediate nets, may they exist, not reaching the global scope. The / prefix
turns off the search mechanism and searches right in the project global namespace;
if the object is not found, it is created in the project global namespace.

<h4 autotoc="yes" id="hlibrary"> Expert: hlibrary </h4>
<p>
The simpler usage discussed above, is also the self-contained usage: when
all sheets files of the hierarchy are placed in a single project directory
and all sheets are listed in the project file (as root and aux sheets).
A variation for large projects is placing all sheet files in a directory
tree and have a project file in the top directory, listing all sheets.
<p>
This setup is called self-contained because packing up the project starting
from its root directory results in a portable file that can be distributed.
After unpacking on a different system, all files are available and the project
can be compiled. This is because all symbols and devmaps are automatically
embedded in the sheet files, all project-specific configuration embedded in
the project file.
<p>
Another use case of hierarchic pages is maintaining a library of child sheets;
for example in IC design it is common to have symbols for blocks like a XOR gate
and then have the "few FET implementation" of that XOR gate as a child sheet
in a library. The advantage is that different projects can share and reuse
the same child sheets. The drawback is that these child sheets are not copied
into the project directory and the <b>project is not self-contained</b>: the
library of child sheets need to be distributed alongside the project.
<p>
This option is supported by sch-rnd via path based child sheet references.
Reusable child sheets are placed in the so called hlibrary. This is very
similar to the symbol library, with the search paths configured in the
config node rc/hlibrary_search_paths. Each hlibrary path is a tree of
sheet files and directories on the file system. Child sheets from
sheet ref syms are addressed using the same <i>cschem/child/name</i>
attribute. During project compilation when the given sheet is not
found by name in the project file, sch-rnd announces this in the
message log and automatically searches the hlibrary path and picks the first
file with a matching name. The name is either in the form of "foo.rs" or
just "foo" (sch-rnd automatically matches the .rs or .lht endings).
<p>
Example project:
<a href="../../../examples/hierarchic/50_hlibrary/">50_hlibrary</a> is
a modified copy if the 20_led example; led.rs is the same child sheet,
referenced as <i>cschem/child/name=led</i> from main.rs
S1 and S2. The file lives in ./hlibrary/, which is a preconfigured standard
hlibrary search path. The file is searched and picked up upon the first
compilation.
<p>
Sheets loaded this way are <b>not added</b> to the project file and are
listed as [E] (for external) sheets in the sheet selector. This is a clear
indication that the sheet is not listed in the project file and is automatically
loaded from the hlibrary for a hierarchic design.
<p>
The project properties dialog lists external sheets in a separate group
and the toggle button converts an external sheet into an aux sheet, adding
it to the project file sheet listing with its relative path to the project
file. This is useful only if the external sheet is coming from a project-local
hlibrary.

<h4 autotoc="yes"> Expert: addressing sheets by path </h4>
<p>
Another way of addressing external child sheets that are not listed in the
project file is path based addressing. This is done by using the sheet
ref sym attribute <i>cschem/child/path</i> instead of cschem/child/name.
The path must be relative to the parent sheet's path on the file system.
The sheet is automatically loaded as an external sheet during compilation.
<p>
Example project:
<a href="../../../examples/hierarchic/52_path/">52_path</a> is
a modified copy if the 20_led example; led.rs is the same child sheet,
referenced as <i>cschem/child/path=my_lib/led.rs</i> from main.rs
S1 and S2. The file lives in ./my_lib/, which is <b>not</b> on
hlibrary search path, thus the file is not searched. It is picked up
by a direct file system lookup, using path ./my_lib/led.rs
relative to main.rs. Note: since there is no search, there is also
no file name matching, thus in the path addressing method the exact
file name must be specified, the .rs ending can not be left off.
<p>
This method can not be used to address central hlibrary child sheets because of
the relative addressing in the file system. It is useful for large projects
with a local system of child sheets with redundant names, sorted into
subdirectories. In that case the designer may wish to explicitly refer to
the child sheet by relative path instead of by short name depending on 
a library search algorithm that may pick the wrong file.


<h4 autotoc="yes"> Expert: addressing sheets by uuid </h4>
<p>
Not yet supported. (TODO)

<h4 autotoc="yes"> Expert: non-graphical sheets </h4>
<p>
Non-graphical sheets define object directly in the abstract model,
they do not represent concrete model sheets. Hierarchic mechanisms (like
connecting child sheet terminals to sheet ref sym terminals) depend on
working with concrete model sheets. Thus non-graphical sheets can not
participate in the hierarchy at the moment but can be used as root
sheets in global scope.


<h4 autotoc="yes"> Expert: spice unslotting </h4>
<p>
A special use case for hierarchic designs is "unslotting" a symbol
that was drawn as monolithic symbol. E.g. a dual opamp drawn
as a single symbol can be split up into two separate opamps
This use case is described in
<a href="../../07_io/export_spice/pinout.html#unslotting"> the spice
export plugins documentation</a>.


</body>
</html>
