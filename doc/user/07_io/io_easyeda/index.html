<html>
<body>

<H2 autotoc="yes"> io_easyeda </H2>
<p>
Type: schematics, symbols.

<H3 autotoc="yes"> EasyEDA versions and compatibility </H3>
<p>
EasyEDA is an EDA tool for the schematics-pcb-simulation workflow. It is
proprietary but offers essentially unlimited free access both as an online
web service and an offline desktop software. As of 2024 neither the service
nor the software do too much to lock users in:
<ul>
	<li> design files are easy to download or upload
	<li> the native file formats are plain text
	<li> there's even some minimal documentation on some of the native file formats
	<li> the software has alien format support
</ul>
<p>
EasyEDA alien format support may be of interest to Ringdove users because
EasyEDA is connected to:
<ul>
	<li> oshwlab: a community sharing open source hardware designs in EasyEDA
	     format; being able to load those schematics makes it easier to fork
	     them with free tools
	<li> lcsc: a generic electronics part vendor with a huge variety of parts;
	     besides datasheets, they also make symbols and footprints freely
	     downloadable for most of their parts - in EasyEDA format
	<li> jlcpcb: board fab and assembly shop; there seems to be some three-way
	     affiliation among EasyEDA, lcsc and jlcpcb, which suggests symbols/footprints
	     downloaded from lcsc would work well with the processes of jlc
</ul>
<p>
EasyEDA offers two variants, called <b>std</b> and <b>pro</b>. The two
variants are essentially two distinct software projects with
differing file formats and menus. It seems like symbols are always in
the pro file format, even when loaded by the std variant. Symbols present
in the part file downloaded from lcsc contains files, including the symbol,
in the pro format.
<p> 
The file saved from the std variant is really a project file that may
contain multiple sheets (pages). A schematic sheet in the std format
contains all symbols as embedded (in the std format), but there seems
to be no way to export or save symbols separately in the std format.

<p>
sch-rnd provides support for loading:
<ul>
	<li> std schematic project (potentially multiple sheets)
	<li> std symbol (both from library and directly in sym editor mode)
	<li> pro schematic project (potentially multiple sheets)
	<li> pro symbol (both from library and directly  in sym editor mode)
</ul>


<H3 autotoc="yes"> How to export from EasyEDA </H3>
<p>
Before trying any of these, make sure you use the right variant (std or pro),
the user interface and file formats really differ a lot.
<p>
<ul>
	<li> <b>std schematics</b> from the online editor: File menu, File source...,
	     download button; saves a json that contains all sheets of the
	     current project
	<li> <b>std symbol</b> from the online editor <b>from sheet</b>: copy the symbol to
	     the paste buffer; File menu, new symbol; paste in the new empty
	     symbol; File menu, File source, download; saves a json of Doctype=2
	<li> <b>std symbol</b> from the online editor <b>from the library</b>: double click the
	     symbol and the editor opens it in a new symbol editor sheet;
	     File menu, File source, download; saves a json of Doctype=2
	<li> <b>pro schematics</b> from the online editor: File menu, Project save as (local);
	     saves a zip file named *.epro.
	<li> <b>pro symbol from lcsc</b>: navigate to the product detail page of the part,
	     there should be an EasyEDA model link under the datasheet link;
	     clicking that opens a preview; click "free trial" there and the
	     file opens in the online editor; select the symbol tab; File menu,
	     File source..., Download button; saves a text file starting with
	     <i>["DOCTYPE","SYMBOL",...</i>; copy this file into the library,
	     start sch-rnd, open library window: the symbol should show up in
	     the list
</ul>

<H3 autotoc="yes"> Alien format limitations </H3>
<p>
There are some details that are not imported; the data model and the logics
of the software differ, so there are some manual editing steps needed after
loading alien format data. Below is a list of known cases for EasyEDA.

<H4 autotoc="yes"> Color </H4>
<p>
In EasyEDA each drawing object has its own stroke and fill color and
text objects have font size and font properties. In sch-rnd
objects have a pen that determines all these. Sch-rnd will throw away
color information and will use standard pens on import.

<H4 autotoc="yes"> Text alignment and size </H4>
<p>
Font differs a lot between EasyEDA and sch-rnd. For typical longer text,
like titleblock fields or symbol part number suffixed with lcsc identifier
sch-rnd's render tend to be half the width of EasyEDA's. In some cases
EasyEDA decides to store multiple text objects in a column anchored at
their right side ends. These text objects happen to line up in EasyEDA render
because the right-side coordinates are computed with EasyEDA font in mind.
While sch-rnd will place these text objects accurately, also setting alignment
properly, they won't line up since sch-rnd font is much narrower.
<p>
Whenever in doubt, enable the text meta layer. If an unaligned text object
has its origin in the bottom right corner, it's an indication of the above
problem.

<H4 autotoc="yes"> Multi-sheet bundles </H4>
<p>
An EasyEDA std schematics file is always a multi-page file even if it contains
only a single sheet. Sch-rnd won't behave differently with a single-page
file and will always import the file as if it was a project file. In the sheet
selector the project name is deduced from the file name and the sheet names
are copied from sheet titles.

<H4 autotoc="yes"> Terminal connection sensitivity </H4>
<p>
In EasyEDA there's a single point, typically the outer endpoint, of the terminal
that is sensitive to wire and terminal connections. In sch-rnd the whole
terminal is sensitive. EasyEDA wires crossing the insensitive middle
section of a terminal are connected to the terminal when importing the
file in sch-rnd and upon compilation a multi-connection warning is generated
unless it is disabled.

<H4 autotoc="yes"> Multiple terminal connections </H4>
<p>
It is also common in EasyEDA to connect two or more terminals and a wire in the
same endpoint. While this works in sch-rnd, it is generating the multi-connection
warning on compilation. Best practice is to move the terminals apart and make
sure it's always exactly two objects participating in a connection (green circle).
<p>
The multi-connection warning can be disabled via a config node called
<b>multiport_net_merge</b>.

<H4 autotoc="yes"> Redundant terminal names </H4>
<p>
In sch-rnd each terminal of a symbol must have a name that is unique within
the symbol. If a part has 4 GND pins, the symbol is either drawn as a single
terminal and is assigned 4 physical pin numbers or if the symbol is drawn
with 4 GND terminals they have 4 different name name attributes (e.g. GND1..GND4).
<p>
In EasyEDA there's no such requirement on uniquely named terminals so many
EasyEDA symbols will have multiple GND and VCC pins. Such a symbol will
break in sch-rnd and generate errors during compilation. The only way to fix
this is manually renaming the redundant terminals to unique names.

<H4 autotoc="yes"> Inline images </H4>
<p>
EasyEDA sheets may contain images (svg, png, etc.). These are embedded in
base64 encoding into the std sheet file. These objects are not imported in
sch-rnd. The bounding box of the image object is drawn instead.

<H4 autotoc="yes"> Slots </H4>
<p>
Slotting works very differently.
<p> EasyEDA pro stores a separate complete symbol for each slot within the
same symbol file. In sch-rnd when loading slotted EasyEDA symbols from the
library, only the first slot is loaded. When open directly in the "symbol
editor mode", all slots are rendered in a horizontal arrangement, as a
single symbol. When loading a pro sheet the loader saves each slot as a
separate symbol in a temporary cache and places (embeds) the right one.
<p>
EasyEDA std doesn't have slotting in its symbol format. Instead slots
are simply stored as separate symbols, one symbol per symbol file, which
sch-rnd loads properly, without any slotting information.
When loading an std sheet, this doesn't matter because the right slot
symbols are embedded in the drawing at each component in the sheet file.

<H4 autotoc="yes"> Loading pro sheet files without zip </H4>
<p>
sch-rnd can not load sheet files manually extracte from a zip file, but can
read only the zip file. It is because how symbols are stored: in the pro
variant symbols are not embedded in the sheet file but are stored as separate
symbol files within the same zip. The sheet file makes sense only together with
the symbols, thus the whole (unpacked) zip is required.

<H4 autotoc="yes"> How unzipping works </H4>
<p>
I decided not to introduce a libzip dependency, rather just call unzip as
an external utility. Any zip implementation can be used as long as it
provides two functionality:
<ul>
	<li> given a full path to a zip file, prints a file list to stdout (in whatever text format)
	<li> given a full path to a zip file, extracts all files into the current working directory
</ul>
<p>
For example on Debian-like systems the packgae <b>unzip</b> can do these.
<p>
The actual command lines for these two functions are configured, see
config nodes <b>plugins/easyeda/zip_extract_cmd</b> and
<b>plugins/easyeda/zip_list_cmd</b>; %s is substituted with the full path
of the zip file and CWD is set before calling the extract command.
<p>
This is a weak dependency: any of these is required only if pro files are to
be open.
