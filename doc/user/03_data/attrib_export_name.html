<html>
<body>
<h1> sch-rnd - attribute export naming </h1>
<p>
<h2> Attribute export names </h2>
<p>
Each attribute in the abstract model has an optional "export name" field.
If the export file format supports arbitrary key/value pairs exported
(e.g. with nets, components or even ports), those attributes that have an
export name set are exported, using the export name instead of the original
attribute name. This allows plugins to control which attributes to export
for a given workflow, under what output attribute name.
<p>
Usually the target plugin sets export name on some of the attributes,
typically using the standard attribute export naming mechanism.

<h2> Standard attribute export naming mechanism </h2>
<p>
The target plugin has the following configuration nodes:

<table border=1 cellspacing=0>
	<tr><th>end of node path <th> description
	<tr><td>flow_prefix      <td> workflow prefix (domain prefix)
	<tr><td>sw_prefix        <td> consumer software prefix
	<tr><td>whitelist        <td> list of attribute names to export with an option to use a different name in export; this is typically a list type config node of the target plugin, e.g. for target_pcb it is <i>plugins/target_pcb/attr_export/whitelist</i>
	<tr><td>blacklist        <td> list of attribute names that shall not be exported; this is typically a list type config node of the target plugin, e.g. for target_pcb it is <i>plugins/target_pcb/attr_export/blacklist</i>
</table>

<p>
The mechanism is specified in <a href="http://repo.hu/projects/coraleda/std/crl001_attr.html">
CoralEDA crl001</a>, with the following sch-rnd specific implementation details:
<ul>
	<li> flow_prefix is the same as "domain scope prefix" in crl001; it's typically the same as the target plugin's basename, e.g. "pcb" for plugin target_pcb, so symbol attribute "pcb:foo" is exported as component attribute "foo" when using target_pcb
	<li> whitelist items are either plain attribute names, or "attr_name->export_name" pairs for a rename; e.g. "display/dnp->dnp" means the display/dnp attribute is exported under the name dnp
	<li> blacklist is a list of attribute names; a blacklisted attribute's export name is removed even if it was also whitelisted or matched any of the prefixes or another plugin has set it earlier during the compilation process
</ul>
<p>
The order of precedence (later items override earlier items on the list):
<ol>
	<li> whitelist
	<li> sw_prefix
	<li> flow_prefix
	<li> blacklist
</ol>
<p>
Since sch-rnd does not know the exact software the netlist will be used with,
the sw_prefix is usually manually configured, e.g. when using target_pcb the
config node to set is <i>plugins/target_pcb/attr_export/sw_prefix</i>.

<h2> Practical example (pcb workflow) </h2>
<p>
When exporting a netlist using the <i>pcb</i> view, which in turn uses
the target_pcb plugin to mark attributes for export, the following methods
can be used to get an attribute exported.
<p>
In this example the list type config node
<i>plugins/target_pcb/attr_export/whitelist</i> contains the following items:
<ul>
	<li> tolerance
	<li> color
	<li> display/dnp->dnp
</ul>
<p>
A typical symbol placed on the sheet will have a <i>name</i>, a <i>footprint</i>
a <i>value</i> and sometimes a <i>device</i> attribute. The <i>name</i>
attribute has a meaning to the sch-rnd compiler as it will determine the
unique identifier of the resulting (abstract) component. The <i>footprint</i>,
<i>value</i> and <i>device</i> attributes are hardwired in the tEDAx netlist
code because these are predefined fields in the file format with fixed name.
<p>
Then comes the flexible part: any component attribute key that's on the
whitelist is selected for export. For the <i>display/dnp->dnp</i> entry
of the above whitelist, the component symbol <i>display/dnp</i> is
renamed to dnp on the export. (The input attribute on the symbol is
normally <i>dnp</i>, but it gets renamed to <i>display/dnp</i> during
compilation while merging attributes of multiple symbols of the same
<i>name</i> into a single component).
<p>
Then any component attribute whose key is starting with <i>pcb:</i> is
marked for export, with the pcb: prefix removed. This is done automatically
but the target_pcb plugin (used when the <i>pcb</i> view is active).
This means a custom attribute like <i>pcb:height=15mm</i> is marked for export
as <i>height=15mm</i> without any extra configuration.
<p>
Then comes the sw_prefix mechanism. For example set the config node 
<i>plugins/target_pcb/attr_export/sw_prefix</i> to "pcb-rnd", either in the
config tree or e.g. using the -c command line argument:
<pre>
pcb-rnd -x tedax -c plugins/target_pcb/attr_export/sw_prefix=pcb-rnd board.rs
</pre>
and any attribute whose key is prefixed with "pcb-rnd:" will be marked for
export, with the prefix removed.
<p>
Finally if the blacklist at <i>plugins/target_pcb/attr_export/blacklist</i>
is not empty, all attributes are checked against the blacklist. On any match
the attribute's export name (set by the above mechanisms) is deleted,
so that the attribute is not exported.
<p>
Note: the blacklist goes by the original
attribute name, including prefixes. So to remove the final <i>height=15mm</i>
comptag from the netlist, <i>pcb:height</i> needs to be blacklisted, and
to remove the dnp comptag, <i>display/dnp</i> needs to be blacklisted.
<p>
Component attribute (original) names can be observed in the abstract model
window. When the attribute is selected, the <i>exported as:</i> under the
list shows the final export name used in the netlist output. When this is
empty, the attribute is not exported.
